package gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.border.CompoundBorder;

public class CommandPanel extends JPanel{
    private static final List<Character> POSSIBLE_HOTKEYS = Arrays.asList(
                    'q', 'w', 'e', 'r', 't', 'y',
                    'a', 's', 'd', 'f' , 'g', 'h',
                    'z', 'x', 'c', 'v', 'b', 'n');
    private static final List<Character> CRAMPED_HOTKEYS = Arrays.asList(
            'q', 'w', 'e', 'r',
            'a', 's', 'd', 'f' ,
            'z', 'x', 'c', 'v'); 
    private static final Set<String> DEFAULT_CHOICES = new HashSet<String>(Arrays.asList("Wait", "Nothing", "Next", "Leave", "Back"));

//    private JPanel panel;
    private int index;
    private int page;
    private int perPage;
    private int rowLimit = 6;
    private Map<Character, KeyableButton> hotkeyMapping;
    private List<KeyableButton> buttons;
    private JPanel rows[];
    public CommandPanel(int width, int height) {
//        panel = new JPanel();
    	super();
        this.setBackground(new Color(245,245,245));
        if(height > 768){
        this.setPreferredSize(new Dimension(width, 150));
        this.setMinimumSize(new Dimension(width, 150));
        }else{
        	this.setPreferredSize(new Dimension(width, 100));
            this.setMinimumSize(new Dimension(width, 100));
        }
        this.setBorder(new CompoundBorder());
        hotkeyMapping = new HashMap<Character, KeyableButton>();
        if (width <= 1024){
        	rowLimit = 4;
        }
        if(height > 768){
        	perPage = 3 * rowLimit;
        }else{
        	perPage = 2 * rowLimit;
        }
        rows = new JPanel[3];
        rows[0] = new JPanel();
        rows[1] = new JPanel();
        rows[2] = new JPanel();
        for (JPanel row : rows) {
            //FlowLayout layout;
            //layout = new FlowLayout();
            BoxLayout layout = new BoxLayout(row, BoxLayout.LINE_AXIS);
            row.setLayout(layout);
            row.setOpaque(false);
            row.setBorder(BorderFactory.createEmptyBorder());
            this.add(row);
        }
        BoxLayout layout = new BoxLayout(this, BoxLayout.Y_AXIS);
        this.setLayout(layout);
        this.setAlignmentY(Component.TOP_ALIGNMENT);
        this.add(Box.createVerticalGlue());
        this.add(Box.createVerticalStrut(15));
        buttons = new ArrayList<KeyableButton>();
        index = 0;
    }

    public JPanel getPanel() {
        return this;
    }

    public void reset() {
        buttons.clear();
        hotkeyMapping.clear();
        clear();
        refresh();
    }

    private void clear() {
        for (JPanel row : rows) {
            row.removeAll();
        }
        if(rowLimit==4){
        	for(Character mapping : CRAMPED_HOTKEYS){
            	hotkeyMapping.remove(mapping);
            }
        }else{
	        for(Character mapping : POSSIBLE_HOTKEYS){
	        	hotkeyMapping.remove(mapping);
	        }
        }
        index = 0;  
    }

    public void refresh() {
        repaint();
        revalidate();
    }

    public void add(KeyableButton button) {
        page = 0;
        buttons.add(button);
        use(button);
    }

    private void use(KeyableButton button) {
        int effectiveIndex = index - page * perPage;
        final int currentPage = page;
        if (effectiveIndex >= 0 && effectiveIndex < perPage) {
            int rowIndex = Math.min(rows.length - 1, effectiveIndex / rowLimit);
            JPanel row = rows[rowIndex];
            row.add(button);
            row.add(Box.createHorizontalStrut(8));
            Character hotkey;
            if(rowLimit==4){
            	hotkey = CRAMPED_HOTKEYS.get(effectiveIndex);
            }else{
            	hotkey = POSSIBLE_HOTKEYS.get(effectiveIndex);
            }
            register(hotkey, button);
            if (DEFAULT_CHOICES.contains(button.getText()) && !hotkeyMapping.containsKey(' ')) {
                hotkeyMapping.put(' ', button); 
            }
        } else if (effectiveIndex == -1) {
            KeyableButton leftPage = new RunnableButton("<<<", new Runnable() {
				@Override
				public void run() {
					setPage(currentPage - 1);
				}
			});
            rows[0].add(leftPage, 0);
            register('~', leftPage);
        } else if (effectiveIndex == perPage){
            KeyableButton rightPage = new RunnableButton(">>>", new Runnable() {
				@Override
				public void run() {
					setPage(currentPage + 1);
				}
			});
            rows[0].add(rightPage);
            register('`', rightPage);
        }
        index += 1;
    }

    public void setPage(int page) {
        this.page = page;
        clear();
        for(KeyableButton b: buttons){
        	use(b);
        }
        refresh();
    }

    public Optional<KeyableButton> getButtonForHotkey(char keyChar) {
        return Optional.ofNullable(hotkeyMapping.get(keyChar));
    }

    public void register(Character hotkey, KeyableButton button) {
        button.setHotkeyTextTo(hotkey.toString().toUpperCase());
        hotkeyMapping.put(hotkey, button);
    }

/*	public void setBackground(Color frameColor) {
		setBackground(frameColor);
	}*/
}