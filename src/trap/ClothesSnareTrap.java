package trap;

import characters.Character;
import combat.Encounter;

public class ClothesSnareTrap implements Trap {
    @Override
    public void trigger(Character target) {

    }

    @Override
    public boolean decoy() {
        return false;
    }

    @Override
    public boolean recipe(Character owner) {
        return false;
    }

    @Override
    public boolean requirements(Character owner) {
        return false;
    }

    @Override
    public String setup(Character owner) {
        return null;
    }

    @Override
    public String description() {
        return null;
    }

    @Override
    public void resolve(Character active) {

    }

    @Override
    public Character owner() {
        return null;
    }

    @Override
    public int priority() {
        return 0;
    }

    @Override
    public String toString() {
        return null;
    }

    @Override
    public void capitalize(Character attacker, Character victim, Encounter enc) {

    }
}
