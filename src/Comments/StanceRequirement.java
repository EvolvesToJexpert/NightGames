package Comments;
import characters.Character;
import combat.Combat;
import stance.Stance;

public class StanceRequirement implements CustomRequirement {
	Stance stance;

	public StanceRequirement(Stance stance) {
		this.stance = stance;
	}

	@Override
	public boolean meets(Combat c, Character self, Character other) {
		if (c == null) {
			return false;
		}
		return c.stance.en == stance;
	}
}
