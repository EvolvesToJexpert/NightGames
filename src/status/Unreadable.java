package status;

import java.util.HashSet;

import combat.Combat;

import characters.Attribute;
import characters.Character;
import characters.Trait;


public class Unreadable extends Status {

	public Unreadable(Character affected) {
		super("Unreadable",affected);
		this.duration=3;
		flag(Stsflag.unreadable);
		tooltip = "Hides arousal and stamina from opponent";
		if(affected!=null && affected.has(Trait.PersonalInertia)){
			this.duration=5;
		}
		this.affected = affected;
	}

	@Override
	public String describe() {
		return "";
	}

	@Override
	public Status copy(Character target) {
		return new Unreadable(target);
	}
}
