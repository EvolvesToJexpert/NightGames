package status;

import combat.Combat;

import characters.Attribute;
import characters.Character;
import characters.Emotion;
import characters.Trait;

public class Distorted extends Status {

	public Distorted(Character affected, int magnitude) {
		super("Distorted",affected);
		this.magnitude = magnitude;
		flag(Stsflag.distorted);
		tooltip = "Evasion bonus";
		duration = 6;
		if(affected.has(Trait.PersonalInertia)){
			duration = 9;
		}
		this.affected = affected;
	}

	@Override
	public String describe() {
		if(affected.human()){
			return "Your image is distorted, making you hard to hit.";
		}
		else{
			return "Multiple "+affected.name()+"s appear in front of you. When you focus, you can tell which one is real, but it's still screwing up your accuracy.";
		}
	}

	@Override
	public int evade() {
		return magnitude;
	}

	@Override
	public Status copy(Character target) {
		return new Distorted(target,magnitude);
	}

}
