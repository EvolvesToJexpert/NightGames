package status;

import java.util.HashSet;

import combat.Combat;

import characters.Attribute;
import characters.Character;
import characters.Emotion;
import characters.Trait;

public class Charmed extends Status {

	public Charmed(Character affected,int duration) {
		super("Charmed",affected);
		flag(Stsflag.charmed);
		tooltip = "Unable to act and suseptible to suggestion";
		this.duration = duration;
		if(affected!=null && affected.has(Trait.PersonalInertia)) {
			this.duration = Math.round(this.duration*1.5f);
		}
		this.affected = affected;
	}

	@Override
	public String describe() {
		if(affected.human()){
			return "You know that you should be fighting back, but it's so much easier to just surrender.";
		}
		else{
			return affected.name()+" is flush with desire and doesn't seem interested in fighting back.";
		}
	}
	@Override
	public int tempted(int x) {
		return 3;
	}

	@Override
	public int escape() {
		return -10;
	}

	public int counter() {
		return -10;
	}

	@Override
	public Status copy(Character target) {
		return new Charmed(target,duration);
	}
	@Override
	public void turn(Combat c) {
		affected.emote(Emotion.horny,15);
		decay(c);
	}
}
