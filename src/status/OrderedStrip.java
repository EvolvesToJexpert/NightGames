package status;

import characters.Character;
import combat.Combat;

public class OrderedStrip extends Status{
    public OrderedStrip(Character affected) {
        super("Ordered to Strip", affected);
        flag(Stsflag.orderedstrip);
        duration = 3;
    }

    @Override
    public String describe() {
        if(affected.human()){
            return "You've been ordered to undress";
        }else{
            return "You've ordered "+affected.name()+" to strip";
        }
    }

    @Override
    public void turn(Combat c) {
        decay();
        if(affected.nude()){
            affected.removeStatus(this,c);
        }
        if(duration<=0){
            affected.removeStatus(this,c);
            if(!affected.nude()){
                affected.add(new Disobedient(affected),c);
            }
        }
    }

    @Override
    public Status copy(Character target) {
        return new OrderedStrip(target);
    }
    public void decay(){
        duration--;
    }
}
