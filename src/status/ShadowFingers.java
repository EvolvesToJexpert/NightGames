package status;

import characters.Anatomy;
import characters.Character;
import characters.Trait;
import combat.Combat;

public class ShadowFingers extends Status {

	public ShadowFingers(Character affected) {
		super("Shadow Fingers", affected);
		if(affected!=null && affected.has(Trait.PersonalInertia)){
			duration = 6;
		}else{
			this.duration = 4;
		}
		this.flags.add(Stsflag.shadowfingers);
		tooltip = "+50 finger proficiency";
	}

	@Override
	public String describe() {
		if(affected.human()){
			return "The fingers of your right hand have fused with shadow tendrils, giving you supernatural dexterity.";
		}
		else{
			return "The dark tentacles extending from "+affected.name()+"'s wriggle ominously.";
		}
	}
	
	public float proficiency(Anatomy using){
		if(using==Anatomy.fingers){
			return 1.5f;
		}
		return 1.0f;
	}

	@Override
	public Status copy(Character target) {
		return new ShadowFingers(affected);
	}

}
