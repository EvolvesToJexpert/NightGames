package status;

import characters.Anatomy;
import characters.Character;
import characters.Trait;
import combat.Combat;
import global.Global;

public class Composed extends Status{
    private int durability;

    public Composed(Character affected, int durability, int magnitude) {
        super("Composed", affected);
        this.magnitude = magnitude;
        this.durability = durability;
        this.flags.add(Stsflag.composed);
        tooltip = "Resist Pleasure until composure breaks. Endurance: "+durability+", Pleasure resistance: "+magnitude+"%";
        if(!affected.human()){
            this.traits.add(Trait.sportsmanship);
        }
        lingering = true;
        if(affected.has(Trait.highcomposure)){
            durability = 2*durability;
        }
        if(affected.has(Trait.potentcomposure)){
            magnitude = Math.min(90,2*magnitude);
        }
        duration = 999;
        decaying=false;
    }

    @Override
    public String describe() {
        return "";
    }

    @Override
    public void turn(Combat c) {
        if(affected.is(Stsflag.shamed)){
            durability -= affected.getStatus(Stsflag.shamed).mag();
        }
        if(affected.is(Stsflag.masochism)){
            durability -= 1;
        }
        if(affected.is(Stsflag.bondage)){
            durability -= 1;
        }
        if(affected.is(Stsflag.buzzed)){
            durability -= 1;
        }
        if(durability<=0){
            if(affected.human()){
                c.write(affected,"Something snaps in your mind. Despite your best efforts, you can't keep your emotions in check any longer, and you let out a curse before you can stop yourself. Fuck it then. You've still got a match to win right now.");
            }else{
                c.write(affected,affected.name()+"'s eyebrow twitches. Then twitches again. Her face scrunches up and she closes her eyes, finally letting out a fierce scream. <i>\"Fuck!\"</i> It seems she can't hold her emotions in check any longer.");
            }
            affected.removeStatus(this,c);
            affected.add(new Broken(affected,magnitude),c);
        }
    }

    public int damage(int x, Anatomy area){
        durability -= 1;
        if(area == Anatomy.genitals){
            durability -= 1;
        }
        return 0;
    }

    public int pleasure(int x, Anatomy area){
        if(area == Anatomy.ass){
            durability -= 2;
        }
        return -x*magnitude/100;
    }

    @Override
    public Status copy(Character target) {
        return new Composed(target,durability,magnitude);
    }
}
