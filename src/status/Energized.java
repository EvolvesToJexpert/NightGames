package status;

import combat.Combat;

import characters.Attribute;
import characters.Character;
import characters.Emotion;
import characters.Trait;

public class Energized extends Status {

	public Energized(Character affected,int duration) {
		super("Energized",affected);
		this.duration=duration;
		lingering = true;
		flag(Stsflag.energized);
		tooltip = "Gain 10% mojo each turn";
		if(affected!=null && affected.has(Trait.PersonalInertia)){
			this.duration=3*duration/2;
		}
		this.affected = affected;
	}

	@Override
	public String describe() {
		if(affected.human()){
			return "You're brimming with energy.";
		}else{
			return affected.name()+"'s eyes seem to be faintly glowing.";
		}
	}

	@Override
	public Status copy(Character target) {
		return new Energized(target,duration);
	}
	@Override
	public void turn(Combat c) {
		affected.buildMojo(10);
		affected.emote(Emotion.confident,5);
		affected.emote(Emotion.dominant,10);
		decay(c);
	}
}
