package status;

import characters.Anatomy;
import characters.Character;
import characters.Trait;
import combat.Combat;

public class Disobedient extends Status {
    public Disobedient(Character affected) {
        super("Disobedient", affected);
        flag(Stsflag.disobedient);
        if(affected.has(Trait.PersonalInertia)){
            this.duration=9;
        }else {
            this.duration=6;
        }
    }

    @Override
    public String describe() {
        if(affected.human()){
            return "You disobeyed your master's command and await punishment";
        }else{
            return affected.name()+" has disobeyed your order and deserves punishment";
        }
    }

    @Override
    public int damage(int x, Anatomy area) {
        return x;
    }

    @Override
    public int weakened(int x) {
        return x;
    }

    @Override
    public int evade() {
        return -99;
    }

    @Override
    public int gainmojo(int x) {
        return -x;
    }

    @Override
    public Status copy(Character target) {
        return new Disobedient(affected);
    }

}
