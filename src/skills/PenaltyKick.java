package skills;

import characters.Anatomy;
import characters.Attribute;
import characters.Character;
import combat.Combat;
import combat.Result;
import global.Global;
import stance.Neutral;
import stance.Stance;
import status.Stsflag;

public class PenaltyKick extends Skill {
    public PenaltyKick(Character self) {
        super("Penalty Shot", self);
    }

    @Override
    public boolean requirements(Character user) {
        return user.getPure(Attribute.Footballer) >= 12;
    }

    @Override
    public boolean usable(Combat c, Character target) {
        return c.stance.en== Stance.behind && c.stance.sub(self) && self.canAct()
                &&self.canSpend(10);
    }

    @Override
    public String describe() {
        return "Distract your opponent by grinding on them and leave them open to a knee strike. More effective against Horny targets: 10 Mojo";
    }

    @Override
    public void resolve(Combat c, Character target) {
        self.spendMojo(10);
        target.pleasure(3+self.get(Attribute.Seduction)/2+target.get(Attribute.Perception), Anatomy.genitals,Result.finisher,c);
        int m = Global.random(6)+self.get(Attribute.Footballer);
        if(target.is(Stsflag.horny)) {
            if (self.human()) {
                c.write(self, deal(0, Result.strong, target));
            } else if (target.human()) {
                c.write(self, receive(0, Result.strong, target));
                if (Global.random(5) >= 3) {
                    c.write(self, self.bbLiner());
                }
            }
            m *= 1.2;
            target.pain(m,Anatomy.genitals,c);
            c.stance = new Neutral(self,target);
        }else if(c.attackRoll(this,self,target)){
            if (self.human()) {
                c.write(self, deal(0, Result.normal, target));
            } else if (target.human()) {
                c.write(self, receive(0, Result.normal, target));
                if (Global.random(5) >= 3) {
                    c.write(self, self.bbLiner());
                }
            }
            target.pain(m,Anatomy.genitals,c);
            c.stance = new Neutral(self,target);
        }else{
            if (self.human()) {
                c.write(self, deal(0, Result.miss, target));
            } else if (target.human()) {
                c.write(self, receive(0, Result.miss, target));
            }
        }


    }

    @Override
    public int speed(){
        return 2;
    }

    @Override
    public int accuracy() {
        return 3;
    }

    @Override
    public Skill copy(Character user) {
        return new PenaltyKick(user);
    }

    @Override
    public Tactics type() {
        return Tactics.damage;
    }

    @Override
    public String deal(int damage, Result modifier, Character target) {
        if(modifier == Result.strong){
            return "You grind your ass against "+target.name()+"'s crotch to distract her. She's so horny that she enthusiastically grinds back. " +
                    "When she's distracted you kick backwards, catching her completely off guard. You turn to face her while she's busy holding her groin.";
        }else if(modifier == Result.miss){
            return "ou grind your ass against "+target.name()+"'s crotch to distract her. Unfortunately she only tightens her grip on you.";
        }else{
            return "You grind your ass against "+target.name()+"'s crotch to distract her. You feel her grip weaken as she loses focus. " +
                    "When she's distracted you kick backwards, catching her completely off guard. You turn to face her while she's busy holding her groin.";
        }
    }

    @Override
    public String receive(int damage, Result modifier, Character target) {
        if(modifier == Result.strong){
            return self.name()+" squirms in your grip, grinding her ass against your groin. In your lust-fogged state, you can't resist grinding back and " +
                    "rubbing your dick between her firm cheeks. She suddenly slams her heel into your unprotected balls. The pain is devastating, " +
                    "and you release your hold on her to hold your sore bits.";
        }else if(modifier == Result.miss){
            return self.name()+" squirms in your grip, grinding her ass against your groin. It feels good, but you suspect this is just a diversion. When she suddenly " +
                    "tries to kick at you, you're ready to block it with your leg.";
        }else{
            return self.name()+" squirms in your grip, grinding her ass against your groin. Despite your better judgement, you let yourself indulge in the feeling " +
                    "of her generous ass rubbing your cock for a long moment. She suddenly slams her heel into your unprotected balls. The pain is devastating, " +
                    "and you release your hold on her to hold your sore bits.";
        }
    }
}
