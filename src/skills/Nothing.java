package skills;

import global.Global;
import status.Unreadable;
import characters.Attribute;
import characters.Character;
import characters.Trait;

import combat.Combat;
import combat.Result;

public class Nothing extends Skill {

	public Nothing(Character self) {
		super("Wait", self);
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct();
	}

	@Override
	public void resolve(Combat c, Character target) {
		if(bluff()){
			int m = Global.random(25);
			if(self.human()){
				c.write(self,deal(m,Result.special,target));
			}
			else{
				c.write(self,receive(m,Result.special,target));
			}
			
			self.spendMojo(20);
			self.add(new Unreadable(self),c);
			self.heal(m,c);
			self.calm(25-m,c);
		}
		else if(focused()&&!c.stance.sub(self)){
			if(self.human()){
				c.write(self,deal(0,Result.strong,target));
			}
			else if(target.human()){
				c.write(self,receive(0,Result.strong,target));
			}
			self.heal(Global.random(4),c);
			self.calm(Global.random(8),c);
			self.buildMojo(20);
		}
		else{
			if(self.human()){
				c.write(self,deal(0,Result.normal,target));
			}
			else if(target.human()){
				c.write(self,receive(0,Result.normal,target));
			}
			self.buildMojo(10);
			self.heal(Global.random(4),c);
		}
	}

	@Override
	public boolean requirements(Character user) {
		return true;
	}

	@Override
	public Skill copy(Character user) {
		return new Nothing(user);
	}
	public int speed(){
		return 0;
	}

	@Override
	public Tactics type() {
		if(bluff()||focused()){
			return Tactics.calming;
		}
		else{
			return Tactics.misc;
		}
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if(modifier==Result.special){
			return "You force yourself to look less tired and horny than you actually are. You even start to believe it yourself.";
		}
		else if(modifier==Result.strong){
			return "You take a moment to clear your thoughts, focusing your mind and calming your body.";
		}
		else{
			return "You bide your time, waiting to see what "+target.name()+" will do.";
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if(modifier==Result.special){
			return "Despite your best efforts, "+self.name()+" is still looking as calm and composed as ever. Either you aren't getting to her at all, or she's good at hiding it.";
		}
		else if(modifier==Result.strong){
			return self.name()+" closes her eyes and takes a deep breath. When she opens her eyes, she seems more composed.";
		}
		else{
			return self.name()+" hesitates, watching you closely.";
		}
	}
	public String toString(){
		if(bluff()){
			return "Bluff";
		}
		else if(focused()){
			return "Focus";
		}
		else{
			return name;
		}
	}
	@Override
	public String describe() {
		if(bluff()){
			return "Regain some stamina and lower arousal. Hides current status from opponent.";
		}
		else if(focused()){
			return "Calm yourself and gain some mojo";
		}
		else{
			return "Do nothing";
		}
	}
	private boolean focused(){
		return self.getPure(Attribute.Cunning)>=15 && !self.has(Trait.undisciplined);
	}
	private boolean bluff(){
		return self.has(Trait.pokerface)&&self.getPure(Attribute.Cunning)>=9&&self.canSpend(20);
	}
}
