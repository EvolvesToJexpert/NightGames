package skills;

import characters.*;
import characters.Character;
import combat.Combat;
import combat.Result;

public class KittyKick extends Skill {

	public KittyKick(Character self) {
		super("Kitty Kick", self);
	}

	@Override
	public boolean requirements(Character user) {
		return user.id()== ID.KAT && user.getRank()>=2;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canSpend(30)&&c.stance.reachTop(self)&&self.canAct()&&c.stance.sub(self);
	}

	@Override
	public String describe() {
		return "A specialized low attack from a disadvantageous position: 30 Mojo";
	}

	@Override
	public void resolve(Combat c, Character target) {
		self.spendMojo(30);
		int m = self.getLevel()+self.get(Attribute.Power);
		if(target.human()){
			c.write(self,receive(m,Result.normal,target));
		}
		target.pain(m,Anatomy.genitals,c);
		if(self.has(Trait.wrassler)){
			target.calm(m/4,c);
		}
		else{
			target.calm(m/2,c);
		}			
		target.emote(Emotion.angry,50);

	}

	@Override
	public Skill copy(Character user) {
		return new KittyKick(user);
	}

	@Override
	public Tactics type() {
		return Tactics.damage;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return "Kat firmly grabs both of your wrists and then rolls backwards, pulling you over her.  She begins rapidly bicycle-kicking her feet out, aiming at your lower stomach and crotch.  The kicks don't hurt very much individually, but they are so fast that she manages to get several quick hits in before you can free your hands and block the attacks.";
	}

}
