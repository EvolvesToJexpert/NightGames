package skills;

import global.Scheduler;
import items.Attachment;
import items.Flask;
import items.Toy;
import status.Stsflag;
import global.Global;
import global.Modifier;
import combat.Combat;
import combat.Result;

import characters.Attribute;
import characters.Character;
import characters.Anatomy;

public class UseExcalibur extends Skill{

	public UseExcalibur(Character self) {
		super(Toy.Excalibur.getName(), self);
	}

	@Override
	public boolean requirements(Character user) {
		return true;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.has(Toy.Excalibur)&&self.canAct()&&target.hasPussy()&&c.stance.reachBottom(self)&&
				(target.pantsless()||(self.has(Attachment.ExcaliburScience)&&self.has(Flask.DisSol)))
				&&!c.stance.penetration(self)
				&&(!self.human()|| Scheduler.getMatch().condition!=Modifier.notoys);
	}

	@Override
	public void resolve(Combat c, Character target) {
		int m = 0;
		if(c.attackRoll(this, self, target) || self.has(Attachment.ExcaliburNinjutsu)){
			m = 5 + (target.get(Attribute.Perception)/2);
			int level = 1;
			if(self.has(Attachment.Excalibur2)){
				level ++;
				m += 1 + Global.random(6);

			}
			if(self.has(Attachment.Excalibur3)){
				level ++;
				m *= 2;

			}
			if(self.has(Attachment.Excalibur4)){
				level ++;
				m *= 1.5;

			}
			if(self.has(Attachment.Excalibur5)){
				level ++;
				m *= 2;

			}
			m += (self.get(Attribute.Science)/2);
			if(self.has(Attachment.ExcaliburNinjutsu)){
				if(self.human()){
					c.write(self,deal(0,Result.upgrade,target));
				}
				else if(target.human()){
					c.write(self,deal(0,Result.upgrade,target));
				}
			}
			if(self.has(Attachment.ExcaliburScience)&&!target.pantsless()&&self.has(Flask.DisSol)){
				self.consume(Flask.DisSol, 1);
				
				if(self.human()){
					c.write(self,deal(1,Result.upgrade,target));
				}
				else if(target.human()){
					c.write(self,deal(1,Result.upgrade,target));
				}
				target.shred(Character.OUTFITBOTTOM);
			}
			if(self.human()){
				c.write(self,deal(level,Result.normal,target));
			}
			else if(target.human()){
				c.write(self,deal(level,Result.normal,target));
			}
			if(self.has(Attachment.ExcaliburFetish)){
				m *= 1 + self.getArousal().percent()/100;
				if(self.human()){
					c.write(self,deal(2,Result.upgrade,target));
				}
				else if(target.human()){
					c.write(self,deal(2,Result.upgrade,target));
				}
			}
			if(self.has(Attachment.ExcaliburDark) && target.is(Stsflag.horny)){
				m *= 1 + (target.getStatusMagnitude("Horny")/10);
				if(self.human()){
					c.write(self,deal(3,Result.upgrade,target));
				}
				else if(target.human()){
					c.write(self,deal(3,Result.upgrade,target));
				}
			}
			if(self.has(Attachment.ExcaliburAnimism)&&self.is(Stsflag.feral)){
				m *= 1.5;
				if(self.is(Stsflag.beastform)){
					m*=1.5;
				}
				if(self.human()){
					c.write(self,deal(4,Result.upgrade,target));
				}
				else if(target.human()){
					c.write(self,deal(4,Result.upgrade,target));
				}
			}
			if(self.has(Attachment.ExcaliburArcane)){
				if(self.human()){
					c.write(self,deal(5,Result.upgrade,target));
				}
				else if(target.human()){
					c.write(self,deal(5,Result.upgrade,target));
				}
				target.spendMojo(10);
				self.buildMojo(30);
			}
			if(self.has(Attachment.ExcaliburKi)){
				if(self.human()){
					c.write(self,deal(6,Result.upgrade,target));
				}
				else if(target.human()){
					c.write(self,deal(6,Result.upgrade,target));
				}
				self.heal(10,c);
			}
			m = self.bonusProficiency(Anatomy.toy, m);
			target.pleasure(m,Anatomy.genitals,c);
			
		}
		else{
			if(self.human()){
				c.write(self,deal(0,Result.miss,target));
			}
			else if(target.human()){
				c.write(self,receive(0,Result.miss,target));
			}
		}
	}

	@Override
	public Skill copy(Character user) {
		return new UseExcalibur(user);
	}

	@Override
	public Tactics type() {
		return Tactics.pleasure;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if(modifier == Result.miss){
			return "You thrust Sexcalibur at "+target.name()+"'s groin, but she blocks it.";
		}
		else if(modifier == Result.upgrade){
			switch(damage){
			case 0:
				//Concealed Holster
				return "In one quick motion, you draw Sexcalibur from your concealed holster and attack, catching "+target.name()+" by surprise";
			case 1:
				//Dissolver
				return String.format("On contact with her %s, the head sprays enough dissolving solution to quickly melt through the garment.", 
						target.bottom.peek().getName());
			case 2:
				//Tentacles
				return "Small tentacles uncoil from the shaft and attack her sensitive skin.";
			case 3:
				//Lust Curse
				return "You feel the cursed sex toy response to her horny thoughts and grow more powerful.";
			case 4:
				//Furry
				return "The soft fur on the vibrating head grows into a bushy tail that caresses her privates, "
						+ "guided by the animal spirit possessing it.";
			case 5:
				//Enchantment
				return "The enchanted rod glows slightly as it pulls "+target.name()+"'s mana into you.";
			case 6:
				//Ki
				return "Simply wielding the two improves your Ki flow and restores your stamina.";
			default:
				return "";
			}
		}
		else{
			switch(damage){
			case 2:
				//Grand
				return "You touch your Grand Sexcalibur to "+target.name()+"'s bare slit, and she whimpers with pleasure at the intense vibration.";
			case 3:
				//Masterwork
				return "You attack "+target.name()+"'s pussy with your Masterwork Sexcalibur, dealing intense pleasure with the finely crafted sex toy. "
						+ "She lets out an audible moan at the sensation.";
			case 4:
				//Legendary
				return "You thrust the Legendary Sexcaibur between "+target.name()+"'s nethers, dealing a critical hit of pleasure to her "
						+ "pussy and clit.";
			case 5:
				//Perfect
				return "You touch the vibrating head of your Perfect Sexcalibur to "+target.name()+"'s vulva. She gasps at the unfathomable pleasure "
						+ "and her knees buckle.";				
			default:
				return "You press Sexcalibur's vibrating head against "+target.name()+"'s clit, causing her to flinch and let out a yelp of "
						+ "pleasure.";
			}
		}
				
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return null;
	}

	@Override
	public String describe() {
		return String.format("Pleasure opponent with your %s",Toy.Excalibur.getFullName(self));
	}

}
