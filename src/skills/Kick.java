package skills;

import global.Flag;
import global.Global;
import characters.Attribute;
import characters.Character;
import characters.Emotion;
import characters.Anatomy;
import characters.Trait;

import combat.Combat;
import combat.Result;

public class Kick extends Skill {

	public Kick(Character self) {
		super("Kick", self);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Power)>=17;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return c.stance.feet(self)&&self.canAct()&&!self.has(Trait.sportsmanship)&&(!c.stance.prone(self)||self.has(Trait.dirtyfighter))&&!c.stance.penetration(self);
	}

	@Override
	public void resolve(Combat c, Character target) {
		if(c.attackRoll(this, self, target)){
			int m = Global.random(12)+self.get(Attribute.Power);
			Result r = Result.normal;
			if(c.stance.prone(self)){
				r = Result.strong;
				m *= 1.5;
			}
			if(self.getPure(Attribute.Footballer)>=15 && target.distracted()){
			    r = Result.critical;
			    m *= 1.5;
            }
			if(!target.bottom.isEmpty()&&self.getPure(Attribute.Ki)>=21){
				r = Result.special;
				target.shred(1);
			}
			if(self.human()){
				c.write(self,deal(m,r,target));
				if(!Global.checkFlag(Flag.exactimages)||target.name().startsWith("Yui")){
					c.offerImage("Yui Kicked.png", "Art by AimlessArt");
				}
			}
			else if(target.human()){
				c.write(self,receive(m,r,target));
				if(!Global.checkFlag(Flag.exactimages)||self.name().startsWith("Jewel")){
					c.offerImage("Kick.jpg", "Art by AimlessArt");
				}
			}
			self.buildMojo(10);
			m = self.bonusProficiency(Anatomy.feet, m);
			target.pain(m,Anatomy.genitals,c);
			if(self.has(Trait.wrassler)){
				target.calm(m/4,c);
			}
			else{
				target.calm(m/2,c);
			}			
			target.emote(Emotion.angry,80);
		}
		else{
			if(self.human()){
				c.write(self,deal(0,Result.miss,target));
			}
			else if(target.human()){
				c.write(self,receive(0,Result.miss,target));
			}
		}
	}

	@Override
	public Skill copy(Character user) {
		return new Kick(user);
	}
	public int speed(){
		return 8;
	}
	public Tactics type() {
		return Tactics.damage;
	}
	public String toString(){
		if(self.getPure(Attribute.Ki)>=21){
			return "Shatter Kick";
		}
		else{
			return "Kick";
		}
	}
	
	@Override
	public String deal(int damage, Result modifier, Character target) {
		if(modifier==Result.miss){
			return "Your kick hits nothing but air.";
		}
		if(modifier==Result.special){
			return String.format("You focus your ki into a single kick, targeting not %s's body, but %s %s. The garment is completely destroyed, and your foot continues into %s exposed groin.",
					target.name(),target.possessive(false),target.bottom.peek().name(),target.possessive(false));
		}
        if(modifier==Result.critical){
            if(target.hasBalls()){
                return String.format("You capitalize on %s's momentary inability to defend %sself – you quickly sweep the inside of %s foot with yours, kicking %s legs apart. " +
                                "%s arms flail out to her sides as %s struggles to maintain her footing, and you take a short step back, lining up your attack.  " +
                                "You wind up and launch a merciless kick right between %s wide-open legs, your instep colliding solidly with %s tender balls."
                        ,target.name(),target.pronounSubject(false),target.possessive(false),target.possessive(false),target.possessive(true),target.pronounSubject(false),target.possessive(false),target.possessive(false));            }else{
                return String.format("You capitalize on %s's momentary inability to defend %sself – you quickly sweep the inside of %s foot with yours, kicking %s legs apart. " +
                                "%s arms flail out to her sides as %s struggles to maintain her footing, and you take a short step back, lining up your attack.  " +
                                "You wind up and launch a merciless kick right between %s wide-open legs, your instep colliding solidly with %s tender little pussy."
                        ,target.name(),target.pronounSubject(false),target.possessive(false),target.possessive(false),target.possessive(true),target.pronounSubject(false),target.possessive(false),target.possessive(false));
            }
        }
		if(modifier==Result.strong){
			if(target.hasBalls()){
				return String.format("Lying on the floor, you feign exhaustion, hoping %s will lower her guard. As %s approaches unwarily, you suddenly kick up between " +
						"%s legs, delivering a painful hit to %s family jewels.",target.name(),target.pronounSubject(false),target.possessive(false),target.possessive(false));
			}else{
				return "Lying on the floor, you feign exhaustion, hoping "+target.name()+" will lower her guard. As she approaches unwarily, you suddenly kick up between " +
						"her legs, delivering a painful hit to her sensitive vulva.";
			}
		}
		else{
			if(target.hasBalls()){
				return String.format("You deliver a swift kick between %s's legs, hitting %s squarely on the balls.",target.name(),target.pronounTarget(false));
			}
			else{
				return "You deliver a swift kick between "+target.name()+"'s legs, hitting her squarely on the baby maker.";
			}
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if(modifier==Result.miss){
			return self.name()+"'s kick hits nothing but air.";
		}
		if(modifier==Result.special){
			return self.name()+" launches a powerful kick straight at your groin, battering your poor testicles. Your "+target.bottom.peek()+" crumble off your body, adding nudity to injury.";
		}
        if(modifier==Result.critical){
            return self.name()+" capitalizes on your momentary inability to defend yourself; with a mischievous smirk, she sweeps the inside of your foot with hers, kicking your legs apart.  " +
                    "Your arms flail out to your sides as you struggle to maintain your footing, but in the split-second it takes you to regain your balance, "+self.name()+" has already taken a short step back, " +
                    "and is lining you up for the coup-de-grace.  Smirking slightly, "+self.name()+" winds up and launches a merciless kick squarely into your dangling and unprotected testicles.";
        }
		if(modifier==Result.strong){
			return "With "+self.name()+" flat on her back, you quickly move in to press your advantage. Faster than you can react, her foot shoots up between " +
					"your legs, dealing a critical hit on your unprotected balls.";
		}
		else{
			switch(Global.random(2)){
			case 1:
				return self.name()+" delivers a swift and confident kick right into your sensitive nutsack.  "
						+ "You can almost taste your balls in the back of your throat.";
			default:
				return self.name()+"'s foot lashes out into your delicate testicles with devastating force. ";
			}
		}
	}

	@Override
	public String describe() {
		if(self.getPure(Attribute.Ki)>=21){
			return "A precise, Ki-powered kick that can destroy clothing";
		}
		else{
			return "Kick your opponent in the groin";
		}
	}
}
