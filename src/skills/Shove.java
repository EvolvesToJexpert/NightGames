package skills;

import items.ClothingType;
import stance.Mount;
import stance.Neutral;
import stance.ReverseMount;
import stance.StandingOver;
import status.Braced;
import status.Stsflag;
import global.Global;
import characters.Attribute;
import characters.Character;
import characters.Emotion;
import characters.Anatomy;
import characters.Trait;

import combat.Combat;
import combat.Result;

public class Shove extends Skill {

	public Shove(Character self) {
		super("Shove", self);
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return !c.stance.dom(self)&&!c.stance.prone(target)&&c.stance.reachTop(self)&&self.canAct()&&!c.stance.penetration(self);
	}

	@Override
	public void resolve(Combat c, Character target) {
		int m = Global.random(4)+self.get(Attribute.Power)/3;
		Result r = Result.normal;
		if(c.stance.getClass()==Mount.class||c.stance.getClass()==ReverseMount.class){
			if(self.check(Attribute.Power,target.knockdownDC()+5)){
				r = Result.powerful;
				if(!self.is(Stsflag.braced)){
					self.add(new Braced(self));
				}
				c.stance= new Neutral(self,target);
			}
			else{
				r = Result.weak;
			}
		}
		else{ 
			if(self.check(Attribute.Power,target.knockdownDC())){
				r = Result.strong;
				c.stance= new StandingOver(self,target);
			}
		}
		if(self.getPure(Attribute.Ki)>=1 && !target.topless()){
		    target.shred(Character.OUTFITTOP);
        }
		if(self.human()){
		    c.write(self,deal(m,r,target));
        }else if(target.human()){
            c.write(self,receive(m,r,target));
        }
        self.buildMojo(5);
        target.emote(Emotion.angry, 15);
        target.pain(m,Anatomy.chest,c);
	}

	@Override
	public boolean requirements(Character user) {
		return !user.has(Trait.cursed);	
	}

	@Override
	public Skill copy(Character user) {
		return new Shove(user);
	}
	public int speed(){
		return 7;
	}
	public Tactics type() {
		return Tactics.damage;
	}
	public String toString(){
		if(self.get(Attribute.Ki)>=1){
			return "Shredding Palm";
		}
		else{
			return name;
		}			
	}
	@Override
	public String deal(int damage, Result modifier, Character target) {
	    switch(modifier){
            case powerful:
                return "You shove "+target.name()+" off of you and get to your feet before she can retaliate.";
            case weak:
                return "You push "+target.name()+", but you're unable to dislodge her.";
            case special:
                return "You channel your ki into your hands and strike "+target.name()+" in the chest, destroying her "+target.top.peek();
            case strong:
                return "You shove "+target.name()+" hard enough to knock her flat on her back.";
            case miss:

            default:
                return "You shove "+target.name()+" back a step, but she keeps her footing.";
        }

	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
        switch(modifier){
            case powerful:
                return self.name()+" shoves you hard enough to free herself and jump up.";
            case weak:
                return self.name()+" shoves you weakly.";
            case special:
                return self.name()+" strikes you in the chest with her palm, staggering you a step. Suddenly your "+target.top.peek()+" tears and falls off you in pieces";
            case strong:
                return self.name()+" knocks you off balance and you fall at her feet.";
            case miss:

            default:
                return self.name()+" pushes you back, but you're able to maintain your balance.";
        }

	}

	@Override
	public String describe() {
		return "Slightly damage opponent and try to knock her down";
	}
}
