package skills;

import characters.Anatomy;
import characters.Attribute;
import characters.Character;
import characters.Emotion;
import combat.Combat;
import combat.Result;
import global.Global;
import global.Modifier;
import global.Scheduler;
import items.Attachment;
import items.Toy;
import status.Stsflag;

public class BerserkerBarrage extends Skill {

	public BerserkerBarrage(Character self) {
		super("Berserker Barrage", self);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Discipline)>=18;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return (self.has(Toy.Crop)||self.has(Toy.Crop2)||self.has(Toy.Crop3))&&self.canAct()&&c.stance.mobile(self)&&
				self.is(Stsflag.broken)&&self.canSpend(20)&&
				(c.stance.reachTop(self)||c.stance.reachBottom(self))
				&&(!self.human()|| Scheduler.getMatch().condition!=Modifier.notoys);
	}

	@Override
	public void resolve(Combat c, Character target) {
		int m;
		self.spendMojo(20);

        m = 10+Global.random(14)+target.get(Attribute.Perception)+(self.get(Attribute.Science)/2) + (self.get(Attribute.Discipline));
        m = self.bonusProficiency(Anatomy.toy, m);
        if(self.has(Toy.Crop3)){
            m *= 2.5;
        }
        else if(self.has(Attachment.CropShocker)) {
            m *= 2;
        }
        if (self.human()) {
            c.write(self, deal(0, Result.normal, target));
        } else if (target.human()) {
            c.write(self, receive(0, Result.normal, target));
        }
        target.pain(m, Anatomy.ass, c);
        target.emote(Emotion.nervous, 30);

	}

	@Override
	public Skill copy(Character user) {
		return new BerserkerBarrage(user);
	}

	@Override
	public Tactics type() {
		return Tactics.damage;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
        return "Something within you snaps and anger overwhelms you so much you lose all sense of what you're doing for a few moments. " +
                "By the time you realize what you're doing again, the only evidence to what happened in the lost time is a set of marks on "+target.name()+"'s skin, " +
                "evidence to a series of vicious strikes with your riding crop. You almost feel sorry for her. Almost.";
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return "<i>\"Grrrahhhh!\"</i> The way cry barely serves as warning before "+self.name()+" lets loose with a flurry of strikes with her riding crop. " +
                "You try in vain to shield yourself from the strikes, but there's nothing you can do to keep from being hit again and again by her crop, " +
                "each strike inflicting more pain than the last. When it's all over, you catch sight of "+self.name()+" " +
                "looking at you in mild confusion, as if she doesn't even know quite what she's just done herself. Not that that'll make you forgive her for this.";
	}

	@Override
	public String describe() {
		return "Wildly strike your opponent with repeated crop strikes";
	}

}
