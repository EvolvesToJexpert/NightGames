package skills;

import characters.Attribute;
import characters.Character;
import combat.Combat;
import combat.Result;
import stance.Neutral;
import status.Stsflag;

public class SecondWind extends Skill {

    public SecondWind(Character user){
        super("2nd Wind", user);
    }

    @Override
    public boolean requirements(Character user) {
        return user.getPure(Attribute.Contender)>=3;
    }

    @Override
    public boolean usable(Combat c, Character target) {
        return (self.stunned()) && self.canSpend(10);
    }

    @Override
    public String describe() {
        return "Quickly recover from being Winded";
    }

    @Override
    public void resolve(Combat c, Character target) {
        self.spendMojo(10);
        self.removeStatus(Stsflag.stunned);
        if(self.human()){
            c.write(self,deal(0,Result.normal,target));
        }
        else if(target.human()){
            c.write(self,receive(0,Result.normal,target));
        }
        self.heal(Math.round(self.getStamina().max()*.3f),c);
        if(c.stance.mobile(self)){
            c.stance = new Neutral(self, target);
        }
    }

    @Override
    public Skill copy(Character user) {
        return new SecondWind(user);
    }

    @Override
    public Tactics type() {
        return Tactics.recovery;
    }

    public int speed(){
        return 0;
    }

    @Override
    public String deal(int damage, Result modifier, Character target) {
        return "Your body screams at you to stay down and catch your breath, but you shake off your exhaustion and continue fighting.";
    }

    @Override
    public String receive(int damage, Result modifier, Character target) {
        return "You thought "+self.name()+" was too winded to keep fighting, but she recovers almost immediately.";
    }
}
