package skills;

import stance.Mount;
import global.Global;
import characters.Attribute;
import characters.Character;
import characters.Emotion;
import characters.Anatomy;
import characters.Trait;

import combat.Combat;
import combat.Result;

public class Tackle extends Skill {

	public Tackle(Character self) {
		super("Tackle", self);
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return c.stance.mobile(self)&&c.stance.mobile(target)&&!c.stance.prone(self)&&self.canAct()&&!self.has(Trait.petite);
	}

	@Override
	public void resolve(Combat c, Character target) {
		if(c.attackRoll(this, self, target)&&self.check(Attribute.Power,target.knockdownDC()-self.get(Attribute.Animism))){
			if(self.get(Attribute.Animism)>=1){
				if(self.human()){
					c.write(self,deal(0,Result.special,target));
				}
				else if(target.human()){
					c.write(self,receive(0,Result.special,target));
				}
				target.pain(4+Global.random(6),Anatomy.chest,c);
				c.stance=new Mount(self,target);
			}
			else if(target.has(Trait.reflexes)){
				if(self.human()){
					c.write(self,deal(0,Result.unique,target));
				}
				else if(target.human()){
					c.write(self,receive(0,Result.unique,target));
				}
				self.pain(target.getLevel(), Anatomy.chest);
				self.emote(Emotion.nervous, 10);
				target.emote(Emotion.dominant,15);
			}
			else{
				if(self.human()){
					c.write(self,deal(0,Result.normal,target));
				}
				else if(target.human()){
					c.write(self,receive(0,Result.normal,target));
				}
				target.pain(3+Global.random(4),Anatomy.chest,c);
				c.stance=new Mount(self,target);
			}
			
		}
		else{
			if(self.human()){
				c.write(self,deal(0,Result.miss,target));
			}
			else if(target.human()){
				c.write(self,receive(0,Result.miss,target));
			}
		}
	}

	@Override
	public boolean requirements(Character user) {
		return (user.getPure(Attribute.Power)>=26|| user.getPure(Attribute.Animism)>=1) && !user.has(Trait.petite);
	}

	@Override
	public Skill copy(Character user) {
		return new Tackle(user);
	}
	public int speed(){
		if(self.get(Attribute.Animism)>=1){
			return 3;
		}
		else{
			return 1;
		}
	}
	public int accuracy(){
		if(self.get(Attribute.Animism)>=1){
			return 3;
		}
		else{
			return 1;
		}
	}
	public Tactics type() {
		return Tactics.positioning;
	}

	public String toString(){
		if(self.get(Attribute.Animism)>=1){
			return "Pounce";
		}
		else{
			return name;
		}
	}
	@Override
	public String deal(int damage, Result modifier, Character target) {
		if(modifier==Result.unique){
			return "You lunge at "+target.name()+", but she rolls backwards, using your momentum to throw you behind her. "
					+ "You land hard on the floor, taking the wind out of you.";
		}
		if(modifier==Result.special){
			return "You let your instincts take over and you pounce on "+target.name()+" like a predator catching your prey.";
		}
		else if(modifier==Result.normal){
			return "You tackle "+target.name()+" to the ground and straddle her.";
		}
		else{
			return "You lunge at "+target.name()+", but she dodges out of the way.";
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if(modifier==Result.special){
			return self.name()+" wiggles her butt cutely before leaping at you and pinning you to the floor.";
		}
		if(modifier==Result.miss){
			return self.name()+" tries to tackle you, but you sidestep out of the way.";
		}
		else{
			return self.name()+" bowls you over and sits triumphantly on your chest.";
		}
	}

	@Override
	public String describe() {
		return "Knock opponent to ground and get on top of her";
	}
}
