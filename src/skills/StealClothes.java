package skills;

import characters.Attribute;
import characters.Character;
import combat.Combat;
import combat.Result;

public class StealClothes extends Skill {

	public StealClothes(Character self) {
		super("Steal", self);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Ninjutsu)>=15;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()&&self.canSpend(10)&&
			((c.stance.reachTop(self)&&!target.topless()&&self.canWear(target.top.peek()))||
			(c.stance.reachBottom(self)&&!target.pantsless()&&self.canWear(target.bottom.peek())));
	}

	@Override
	public String describe() {
		return "Steal and put on an article of clothing: 10 Mojo.";
	}

	@Override
	public void resolve(Combat c, Character target) {
		self.spendMojo(10);
		if(c.stance.reachTop(self)&&!target.topless()&&self.canWear(target.top.peek())){
			if(self.human()){
				c.write(self,deal(0,Result.top,target));
				
			}
			else if(target.human()){
				c.write(self,receive(0,Result.top,target));
			}
			self.wear(target.strip(0, c));
			
			
		}else if(c.stance.reachBottom(self)&&!target.pantsless()&&self.canWear(target.bottom.peek())){
			if(self.human()){
				c.write(self,deal(0,Result.bottom,target));
				
			}
			else if(target.human()){
				c.write(self,receive(0,Result.bottom,target));
			}
			self.wear(target.strip(1, c));
		}
	}

	@Override
	public Skill copy(Character user) {
		return new StealClothes(user);
	}

	@Override
	public Tactics type() {
		return Tactics.stripping;
	}
	public int speed(){
		return 6;
	}
	@Override
	public String deal(int damage, Result modifier, Character target) {
		if(modifier==Result.top){
			return String.format("Using your speed and trickery, you swipe %s's %s and put on your ill-gotten new duds.",
					target.name(),target.top.peek().getName());
		}else{
			return String.format("Using your speed and trickery, you swipe %s's %s and put on your ill-gotten spoils.",
					target.name(),target.bottom.peek().getName());
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if(modifier==Result.top){
			return String.format("%s dodges pash you, fast enough that you lose sight of %s. You notice the absence of your %s at almost the same time as you "
					+ "spot %s wearing it.",
					self.name(),self.pronounTarget(false),target.top.peek().getName(),self.pronounTarget(false));
		}else{
			return String.format("%s dodges pash you, fast enough that you lose sight of %s. You notice the absence of your %s at almost the same time as you "
					+ "spot %s wearing them.",
					self.name(),self.pronounTarget(false),target.bottom.peek().getName(),self.pronounTarget(false));
		}
	}

}
