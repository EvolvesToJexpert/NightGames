package skills;

import characters.Anatomy;
import characters.Attribute;
import characters.Character;
import characters.ID;
import combat.Combat;
import combat.Result;
import global.Flag;
import global.Global;

public class BunshinService extends Skill {

	public BunshinService(Character self) {
		super("Bunshin Service", self);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Ninjutsu)>=12;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canActNormally(c)&&!c.stance.behind(target)&&!c.stance.penetration(target)&&self.canSpend(6)&&target.nude();
	}

	@Override
	public String describe() {
		return "Pleasure your opponent with shadow clones: 4 mojo per attack (min 2))";
	}

	@Override
	public void resolve(Combat c, Character target) {
		int clones = Math.min(Math.min(self.getMojo().get()/4, self.get(Attribute.Ninjutsu)/3),5);
		int m;
		self.spendMojo(clones*4);
		Result r;
		if(self.human()){
			c.write(String.format("You form %d shadow clones and rush forward.",clones));
		}
		else if(target.human()){
			c.write(String.format("%s moves in a blur and suddenly you see %d of %s approaching you.",self.name(),clones,self.pronounTarget(false)));
			if(!Global.checkFlag(Flag.exactimages)||self.id()== ID.YUI){
				c.offerImage("Yui bunshin service.jpg", "Art by AimlessArt");
			}
		}
		for(int i=0;i<clones;i++){
			if(c.attackRoll(this,self,target)){
				switch(Global.random(4)){
				case 0:
					r=Result.weak;
					if(self.human()){
						c.write(self,deal(0,r,target));
					}
					else if(target.human()){
						c.write(self,receive(0,r,target));
					}
					target.tempt(Global.random(3)+self.get(Attribute.Seduction)/4,Result.foreplay,c);
					break;
				case 1:
					r=Result.normal;
					if(self.human()){
						c.write(self,deal(0,r,target));
					}
					else if(target.human()){
						c.write(self,receive(0,r,target));
					}
					target.pleasure(Global.random(3+self.get(Attribute.Seduction)/2)+target.get(Attribute.Perception)/2, Anatomy.chest,c);
					break;
				case 2:
					r=Result.strong;
					if(self.human()){
						c.write(self,deal(0,r,target));
					}
					else if(target.human()){
						c.write(self,receive(0,r,target));
					}
					m = Global.random(4+self.get(Attribute.Seduction))+target.get(Attribute.Perception)/2;
					m = self.bonusProficiency(Anatomy.fingers, m);
					target.pleasure(m, Anatomy.genitals,c);
					break;
				default:
					r=Result.critical;
					if(self.human()){
						c.write(self,deal(0,r,target));
					}
					else if(target.human()){
						c.write(self,receive(0,r,target));
					}
					m = Global.random(6)+self.get(Attribute.Seduction)/2+target.get(Attribute.Perception);
					m = self.bonusProficiency(Anatomy.mouth, m);
					target.pleasure(m, Anatomy.genitals,c);
					break;
				}
			}else{
				if(self.human()){
					c.write(self,deal(0,Result.miss,target));
				}
				else if(target.human()){
					c.write(self,receive(0,Result.miss,target));
				}
			}
		}
	}

	@Override
	public Skill copy(Character user) {
		return new BunshinService(user);
	}
	public int speed(){
		return 4;
	}
	@Override
	public Tactics type() {
		return Tactics.pleasure;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if(modifier==Result.miss){
			return String.format("%s dodges your clone's groping hands.",target.name());
		}else if(modifier==Result.weak){
			return String.format("Your clone darts close to %s and kisses %s on the lips.",target.name(),target.pronounTarget(false));
		}else if(modifier==Result.strong){
			if(target.hasDick()){
				return String.format("Your shadow clone grabs %s's dick and strokes it.",target.name());
			}else{
				return String.format("Your shadow clone fingers and caresses %s's pussy lips.",target.name());
			}
		}else if(modifier==Result.critical){
			if(target.hasDick()){
				return String.format("Your clone attacks %s's sensitive penis, rubbing and stroking %s glans.",target.name(),target.possessive(false));
			}else{
				return String.format("Your clone slips between %s's legs to lick and suck %s swollen clit.",target.name(),target.possessive(false));
			}
		}else{
			return String.format("A clone pinches and teases %s's nipples.",target.name());
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if(modifier==Result.miss){
			return String.format("You manage to avoid one of the shadow clones.");
		}else if(modifier==Result.weak){
			return String.format("One of the %ss grabs you and kisses you enthusiastically.",self.name());
		}else if(modifier==Result.strong){
			if(target.hasBalls()){
				return String.format("A clone gently grasps and massages your sensitive balls.");
			}else{
				return String.format("A clone teases and tickles your inner thighs and labia.");
			}
		}else if(modifier==Result.critical){
			if(target.hasDick()){
				return String.format("One of the %s clones kneels between your legs to lick and suck your cock.",self.name());
			}else{
				return String.format("One of the %s clones kneels between your legs to lick your nether lips.",self.name());
			}
		}else{
			if(self.hasBreasts()){
				return String.format("A %s clone presses her boobs against you and teases your nipples.",self.name());
			}else{
				return String.format("A %s clone caresses your chest and teases your nipples.",self.name());
			}
			
		}
	}

}
