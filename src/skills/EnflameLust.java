package skills;

import status.Horny;
import status.Shamed;
import status.Status;
import status.Stsflag;
import characters.Attribute;
import characters.Character;
import characters.Emotion;
import combat.Combat;
import combat.Result;

public class EnflameLust extends Skill {

	public EnflameLust(Character self) {
		super("Enflame Lust", self);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Hypnosis)>=2;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()&&c.stance.mobile(self)&&!c.stance.behind(self)&&
		!c.stance.behind(target)&&!c.stance.sub(self)&&(target.is(Stsflag.charmed)||target.is(Stsflag.enthralled));
	}

	@Override
	public String describe() {
		return "Use hypnotic suggestion to fuel your opponent's dirtiest fantasies.";
	}

	@Override
	public void resolve(Combat c, Character target) {
		if(target.is(Stsflag.horny)){
			if(self.human()){
				c.write(self,deal(0,Result.strong,target));
			}else{
				c.write(self,receive(0,Result.strong,target));
			}
			Status sts = target.getStatus(Stsflag.horny);
			if(sts!=null){
				target.add(new Horny(target,target.getStatusMagnitude("Horny")*2,6),c);
			}
			else{
				target.add(new Horny(target,4,6),c);
			}
		}else{
			if(self.human()){
				c.write(self,deal(0,Result.normal,target));
			}else{
				c.write(self,receive(0,Result.normal,target));
			}
			target.add(new Horny(target,4,6),c);
		}
		target.emote(Emotion.horny, 30);
	}

	@Override
	public Skill copy(Character user) {
		return new EnflameLust(user);
	}

	@Override
	public Tactics type() {
		return Tactics.pleasure;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if(modifier==Result.strong){
			return String.format("Your hypnosis builds on %s's existing fantasies to shape %s perception of this fight into %s "
					+ "ideal sexual scenario.",
					target.name(),target.possessive(false),target.possessive(false));
		}else{
			return String.format("You use a suggestion to plant a dirty thought in %s's head. The vague fantasy will be "
					+ "shaped by %s secret desires and make %s hornier than usual.",
					target.name(),target.possessive(false),target.pronounTarget(false));
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if(modifier==Result.strong){
			return String.format("%s elaborates in glorious, erotic detail on the fantasy that was running through your head. You have "
					+ "no idea how %s knew what you were thinking about, but %s story is so intensely sexual that you have trouble thinking straight.",
					self.name(),self.pronounSubject(false),self.possessive(false));
		}else{
			return String.format("In a brief lull in the fight, %s shares the story of %s favorite sexual experience with you. Thinking "
					+ "back, you have trouble remembering all the details, but you do know it was hot enough that you can't help "
					+ "fantasizing about it.",self.name(),self.possessive(false));
		}
	}

}
