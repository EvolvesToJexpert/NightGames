package skills;

import characters.*;
import characters.Character;
import global.Flag;
import global.Scheduler;
import items.Attachment;
import items.Toy;
import global.Global;
import global.Modifier;

import combat.Combat;
import combat.Result;
import status.Stsflag;

public class UseCrop extends Skill {

	public UseCrop(Character self) {
		super(Toy.Crop.getName(), self);
	}

	@Override
	public boolean requirements(Character user) {
		return true;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return (self.has(Toy.Crop)||self.has(Toy.Crop2)||self.has(Toy.Crop3))&&self.canAct()&&c.stance.mobile(self)&&(c.stance.reachTop(self)||c.stance.reachBottom(self))
				&&(!self.human()|| Scheduler.getMatch().condition!=Modifier.notoys);
	}

	@Override
	public void resolve(Combat c, Character target) {
		int m;
		if(c.attackRoll(this, self, target)){
			if(target.pantsless()&&c.stance.reachBottom(self)){
				if((self.has(Toy.Crop2)&&Global.random(10)>7) || (self.has(Attachment.CropKeen)&&Global.random(10)>7)){
					if(self.human()){
						c.write(self,deal(0,Result.critical,target));
					}
					else if(target.human()){
						c.write(self,receive(0,Result.critical,target));
					}
					target.emote(Emotion.angry,10);
					m = 8+Global.random(14)+target.get(Attribute.Perception)+(self.get(Attribute.Science)/2)+(self.get(Attribute.Discipline)/2);
					m = self.bonusProficiency(Anatomy.toy, m);
                    if(self.has(Toy.Crop3)){
                        m *= 2.5;
                    }
                    else if(self.has(Attachment.CropShocker)) {
                        m *= 2;
                    }
					if(self.has(Trait.cropexpert)){
						m *= 1.5f;
					}
					target.emote(Emotion.angry,15);
					target.pain(m,Anatomy.genitals,c);					
				}else if(self.getPure(Attribute.Discipline)>=15 && self.is(Stsflag.composed)&&target.pantsless() && target.is(Stsflag.masochism)){
                    m = 5+Global.random(12)+target.get(Attribute.Perception)/2+(self.get(Attribute.Science)/2);
                    m = self.bonusProficiency(Anatomy.toy, m);
                    if(self.has(Toy.Crop3)){
                        m *= 2.5;
                    }
                    else if(self.has(Attachment.CropShocker)) {
                        m *= 2;
                    }
                    if(self.has(Trait.cropexpert)){
                        m *= 1.5f;
                    }
                    if(self.human()){
                        c.write(self,deal(0,Result.special,target));
                    }
                    else if(target.human()){
                        c.write(self,receive(0,Result.special,target));
                    }
                    target.pleasure(m,Anatomy.genitals,c);
                    if(self.human()){
                        if(target.pantsless()){
                            if(!Global.checkFlag(Flag.exactimages)||target.id()== ID.YUI){
                                c.offerImage("Crop Nude.jpg", "Art by AimlessArt");
                            }
                        }else{
                            if(!Global.checkFlag(Flag.exactimages)||target.id()== ID.YUI){
                                c.offerImage("Crop.png", "Art by AimlessArt");
                            }
                        }
                    }
                    target.pain(m,Anatomy.ass,c);
                }
				else{
					m = 5+Global.random(12)+target.get(Attribute.Perception)/2+(self.get(Attribute.Science)/2)+(self.get(Attribute.Discipline)/2);
					m = self.bonusProficiency(Anatomy.toy, m);
                    if(self.has(Trait.cropexpert)){
                        m *= 1.5f;
                    }
					if(self.has(Toy.Crop3)){
                        m *= 2.5;
                        if(self.human()){
                            c.write(self,deal(0,Result.unique,target));
                        }
                        else if(target.human()){
                            c.write(self,receive(0,Result.unique,target));
                        }
                    }
					else if(self.has(Attachment.CropShocker)){
						m *= 2;
						if(self.human()){
							c.write(self,deal(0,Result.upgrade,target));
						}
						else if(target.human()){
							c.write(self,receive(0,Result.upgrade,target));
						}
					}else{
						if(self.human()){
							c.write(self,deal(0,Result.normal,target));
						}
						else if(target.human()){
							c.write(self,receive(0,Result.normal,target));
						}
					}
					if(self.has(Trait.cropexpert)){
						m *= 1.5f;
					}
					if(self.human()){
						if(target.pantsless()){
							if(!Global.checkFlag(Flag.exactimages)||target.id()== ID.YUI){
								c.offerImage("Crop Nude.jpg", "Art by AimlessArt");
							}
						}else{
							if(!Global.checkFlag(Flag.exactimages)||target.id()== ID.YUI){
								c.offerImage("Crop.jpg", "Art by AimlessArt");
							}
						}
					}
					target.pain(m,Anatomy.ass,c);
				}
			}
			else{
				if(self.human()){
					c.write(self,deal(0,Result.weak,target));
				}
				else if(target.human()){
					c.write(self,receive(0,Result.weak,target));
				}
				m = 5+Global.random(12)+(self.get(Attribute.Science)/2);
                if(self.has(Trait.cropexpert)){
                    m *= 1.5f;
                }
				m = self.bonusProficiency(Anatomy.toy, m);
				target.pain(m,Anatomy.chest,c);
			}
			target.emote(Emotion.angry,45);
		}
		else{
			if(self.human()){
				c.write(self,deal(0,Result.miss,target));
			}
			else if(target.human()){
				c.write(self,receive(0,Result.miss,target));
			}
		}
	}

	@Override
	public Skill copy(Character user) {
		return new UseCrop(user);
	}

	@Override
	public Tactics type() {
		return Tactics.damage;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if(modifier==Result.miss){
			if(!target.has(Toy.Crop)){
				return "You lash out with your riding crop, but it fails to connect.";
			}
			else{
				return "You try to hit "+target.name()+" with your riding crop, but she deflects it with her own.";
			}
		}
        else if(modifier==Result.unique){
            return "You strike "+target.name()+"'s soft, bare skin with your riding crop, leaving a visible red mark.";
        }
        else if(modifier==Result.special){
            return target.name()+" seems to be in the mood to be hurt right now, and you have no objection to that. Letting her know just how good a girl she's being right now, you give her just the lash with your riding crop that she's asking for.";
        }
		else if(modifier==Result.critical){
			if(target.hasBalls()){
				return "You strike "+target.name()+"'s bare ass with your crop and the 'Treasure Hunter' attachment slips between her legs, hitting one of her hanging testicles " +
						"squarely. She lets out a shriek and clutches her sore nut";
			}
			else{
				return "You strike "+target.name()+"'s bare ass with your crop and the 'Treasure Hunter' attachment slips between her legs, impacting on her sensitive pearl. She " +
					"lets out a high pitched yelp and clutches her injured anatomy.";
			}
		}
		else if(modifier==Result.weak){
			return "You hit "+target.name()+" with your riding crop.";
		}
		else if(modifier==Result.upgrade){
			return "You hit "+target.name()+"'s bare ass with your shock crop, delivering a painful zap on contact.";
		}
		else{
			return "You strike "+target.name()+"'s soft, bare skin with your riding crop, leaving a visible red mark.";
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if(modifier==Result.miss){
			if(!target.has(Toy.Crop)){
				return "You duck out of the way, as "+self.name()+" swings her riding crop at you.";
			}
			else{
				return self.name()+" swings her riding crop, but you draw your own crop and parry it.";
			}
		}
        else if(modifier==Result.unique){
            return self.name()+" hits you with her exceptional riding crop. You discover the hard way that the impressive craftsmanship isn't just for show. The skin where it hits feels like it's on fire.";
        }
        else if(modifier==Result.special){
            return self.name()+" gives you a knowing glance. She seems to know how much you want to be hurt right now, and she's in just the mood to oblige. Almost before you know it, her riding crop strikes across your skin, delighting it with both pain and pleasure simultaneously.";
        }
		else if(modifier==Result.critical){
			return self.name()+" hits you on the ass with her riding crop. The attachment on the end delivers a painful sting to your jewels. You groan in pain and fight the urge to " +
					"curl up in the fetal position.";
		}
		else if(modifier==Result.weak){
			return self.name()+" strikes you with a riding crop.";
		}
		else if(modifier==Result.upgrade){
			return self.name()+" smacks your ass with her shock crop, and you feel a painful electrical discharge where it hits.";
		}
		else{
			return self.name()+" hits your bare ass with a riding crop hard enough to leave a painful welt.";
		}
	}

	@Override
	public String describe() {
		return "Strike your opponent with riding crop. More effective if she's naked";
	}

}
