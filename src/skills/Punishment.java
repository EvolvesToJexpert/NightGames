package skills;

import characters.Attribute;
import characters.Character;
import characters.Emotion;
import combat.Combat;
import combat.Result;
import status.Erebia;
import status.Punisher;
import status.Stsflag;

public class Punishment extends Skill {

	public Punishment(Character self) {
		super("Punishment", self);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Discipline)>=30;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canActNormally(c)&&
				self.is(Stsflag.composed)&&
				self.canSpend(80)&&!self.is(Stsflag.punisher);
	}

	@Override
	public String describe() {
		return "Ready yourself to counter all your opponents attacks: 80 Mojo";
	}

	@Override
	public void resolve(Combat c, Character target) {
		self.spendMojo(80);
		if(self.human()){
			c.write(self,deal(0,Result.normal,target));
		}else if(target.human()){
			c.write(self,receive(0,Result.normal,target));
		}
		self.add(new Punisher(self,target),c);
		self.emote(Emotion.confident,20);
		self.emote(Emotion.dominant, 20);
	}

	@Override
	public Skill copy(Character user) {
		return new Punishment(user);
	}

	@Override
	public Tactics type() {
		return Tactics.preparation;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return "You raise your riding crop into a readied position, making it abundantly clear to "+target.name()+" that she’d better not try anything against you, or else punishment will quickly be coming her way.";
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return String.format("%s raises her riding crop into a readied position, and she gives you a stern smile. That smile makes it abundantly clear to you exactly what’s coming if you dare try to attack her right now.",
				self.name());
	}

}
