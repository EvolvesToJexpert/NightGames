package skills;

import stance.Neutral;
import status.Braced;
import status.Stsflag;
import global.Global;
import characters.Character;
import characters.Trait;
import combat.Combat;
import combat.Result;

public class Recover extends Skill {

	public Recover(Character self) {
		super("Recover", self);
	}

	@Override
	public boolean usable(Combat c, Character target) {		
		return c.stance.prone(self)&&c.stance.mobile(self)&&self.canAct();
	}

	@Override
	public void resolve(Combat c, Character target) {
		if(self.human()){
			c.write(self,deal(0,Result.normal,target));
		}
		else if(target.human()){
			c.write(self,receive(0,Result.normal,target));
		}
		if(c.stance.prone(self)&&!self.is(Stsflag.braced)){
			self.add(new Braced(self));
		}
		c.stance=new Neutral(self,target);
		if(self.has(Trait.determined)){
			self.calm(self.getArousal().max()/4,c);
			self.heal(self.getStamina().max()/4,c);
		}
		else{
			self.heal(Global.random(3),c);
		}
	}

	@Override
	public boolean requirements(Character user) {
		return true;
	}

	@Override
	public Skill copy(Character user) {
		return new Recover(user);
	}
	public int speed(){
		return 0;
	}
	public Tactics type() {
		return Tactics.positioning;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return "You pull yourself up, taking a deep breath to restore your focus.";
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return self.name()+" scrambles back to her feet.";
	}

	@Override
	public String describe() {
		return "Stand up";
	}
}
