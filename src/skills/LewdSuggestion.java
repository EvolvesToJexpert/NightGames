package skills;

import characters.Attribute;
import characters.Character;
import characters.Emotion;
import combat.Combat;
import combat.Result;
import status.Horny;
import status.Stsflag;

public class LewdSuggestion extends Skill {

	public LewdSuggestion(Character self) {
		super("Lewd Suggestion", self);
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Hypnosis)>=3;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()&&c.stance.mobile(self)&&!c.stance.behind(self)&&
				!c.stance.behind(target)&&!c.stance.sub(self)&&target.is(Stsflag.charmed);
	}

	@Override
	public String describe() {
		return "Plant an erotic suggestion in your hypnotized target.";
	}

	@Override
	public void resolve(Combat c, Character target) {
		int x = 1;
		if(target.is(Stsflag.horny)){
			if(self.human()){
				c.write(self,deal(0,Result.strong,target));
			}else{
				c.write(self,receive(0,Result.strong,target));
			}
			x += target.getStatus(Stsflag.horny).mag();
		}else{
			if(self.human()){
				c.write(self,deal(0,Result.normal,target));
			}else{
				c.write(self,receive(0,Result.normal,target));
			}
		}
		target.add(new Horny(target,x,4),c);
		target.emote(Emotion.horny, 30);
	}

	@Override
	public Skill copy(Character user) {
		return new LewdSuggestion(user);
	}

	@Override
	public Tactics type() {
		return Tactics.pleasure;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if(modifier==Result.strong){
			return String.format("You take advantage of the erotic fantasies already swirling through %s's head, whispering ideas that fan the flame of %s lust.",
					target.name(),target.possessive(false));
		}else{
			return String.format("You plant an erotic suggestion in %s's mind, distracting %s with lewd fantasies.",
					target.name(),target.pronounTarget(false));
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if(modifier==Result.strong){
			return String.format("%s whispers a lewd suggestion to you, intensifying the fantasies you were trying to ignore and enflaming your arousal.",
					self.name());
		}else{
			return String.format("%s gives you a hypnotic suggestion and your head is immediately filled with erotic possibilities.",
					self.name());
		}
	}

}
