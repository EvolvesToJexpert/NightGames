package global;

import characters.*;
import gui.KeyableButton;
import gui.SaveButton;
import gui.SceneButton;
import items.Clothing;
import items.ClothingType;
import items.Item;
import items.Toy;

import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.JButton;

import status.Hypersensitive;
import daytime.Scene;

public class Prematch implements Scene{
	private Modifier type;
	private Player player;
	private Dummy lilly;
	public Prematch(Player player){
		this.player=player;
		Global.current=this;
		lilly = new Dummy("Lilly");
		lilly.dress();
		PreEvent scene = checkFlags();
		intro(scene);
	}
	@Override
	public boolean play(String response) {
		return true;
	}
	private void intro(PreEvent scene){
		ArrayList<KeyableButton> choice = new ArrayList<KeyableButton>();		
		String message = "";
		if(Scheduler.getMatchNumber()==0){
		    message+="This is the first match of the season. There are "+Constants.SEASONLENGTH+" matches in this season.<p>";
        }else if(Scheduler.getMatchNumber()==Constants.SEASONLENGTH){
            message+="This is the final match of the season. At the end of the night, the player or players with the highest total score will receive a special prize.<p>" +
                    "The current rankings are:<br>"+Scheduler.displayScores()+"<p>";
        }else if(Scheduler.getMatchNumber()>Constants.SEASONLENGTH){
            message+="This is an exhibition match, not counted for season ranking. The new season will start next week.<p>";
        }else{
            message+="This is match "+(Scheduler.getMatchNumber()+1)+" of the season. There are "+(Constants.SEASONLENGTH-Scheduler.getMatchNumber())+" matches left in this season.<p>" +
                    "The current rankings are:<br>"+Scheduler.displayScores()+"<p>";
        }
		switch(scene){
			case EARLYGAME:
				message+= "You arrive at the Student Union a few minutes before the start of the match. " +
					"You have enough time to check in and make idle chat with your opponents before " +
					"you head to your assigned starting point and wait. At exactly 10:00, the match is on.";
				type=Modifier.normal;
				choice.add(new SceneButton("Start The Match"));
				break;
			case LILLYINTRO:
				Global.gui().loadPortrait(player, lilly);
				message+= "You get to the Student Union a little earlier than usual. Cassie and Jewel are there already and you spend a few minutes talking with them while " +
						"you wait for the other girls to show up. A few people are still rushing around making preparations, but it's not clear exactly what they're doing. " +
						"Other than making sure there are enough spare clothes to change into, there shouldn't be too much setup required. Maybe they're responsible for " +
						"making sure the game area is clear of normal students, but you can't imagine how a couple students could pull that off.<p>"
						+ "A girl who appears to be " +
						"in charge calls you over. She has straight, red hair, split into two braided pigtails. Her features are unremarkable except for the freckles lightly " +
						"spotting her face, but you could reasonably describe her as cute. She mentioned her name once. It was a kind of flower... Lilly? Yeah, " +
						"that sounds right, Lilly Quinn.<p>"
						+ "Lilly gives you a thin smile that's somewhere between polite and friendly. <i>\"You seem to be doing alright for a beginner. Not " +
						"everyone takes to this sort of competition so naturally. In fact, if you think you can handle a bit of a handicap, I've been authorized to offer " +
						"a small bonus.\"</i> Well, you're not going to complain about some extra spending money. It's worth at least hearing her out.<p>"
						+ "<i>\"Sometimes our Benefactor " +
						"offers some extra prize money, but I can't just give it away for free.\"</i> You think you see a touch of malice enter her smile. <i>\"I came up with some " +
						"additional rules to make the Game a little more interesting. If you accept my rule, then the extra money will be added as a bonus to each point you " +
						"score tonight.\"</i><p>"
						+ "It's an interesting offer, but it begs the question of why she's extending it to you specifically. Lilly smirks and brushes one of " +
						"her pigtails over her shoulder. <i>\"Don't worry, I'm not giving you preferential treatment. You're very much not my type. On the other hand, I do like " +
						"watching your opponents win, and by giving you a handicap I make that more likely to happen. I don't intend to unfairly pick on you though. Fortunately, " +
						"you'll make more money for every fight you do win, " +
						"so it's better for everyone.\"</i><p>"
						+ "That's.... You're not entirely sure how to respond to that.<p>"
						+ "<i>\"For the first rule, I'll start with something simple: for " +
						"tonight's match, you're only allowed to wear your boxers. Even when you come back here for a change of clothes, you'll only get your underwear. If you " +
						"agree to this, I'll throw in an extra "+Modifier.pantsman.percentage()+"% on top of your normal prize. Interested?\"</i>";
				Global.flag(Flag.metLilly);
				type=Modifier.pantsman;
				choice.add(new SceneButton("Do it"));
				choice.add(new SceneButton("Not interested"));
				break;
			case CHALLENGEMODE:
				message+="You get to the student union a little earlier than usual. Cassie and Jewel are there already and you spend a few minutes talking with them while " +
						"you wait for the other girls to show up. A few people are still rushing around making preparations, but it's not clear exactly what they're doing. " +
						"Other than making sure there are enough spare clothes to change into, there shouldn't be too much setup required. Maybe they're responsible for " +
						"making sure the game area is clear of normal students, but you can't imagine how a couple students could pull that off.<p>"
						+ "A girl who appears to be " +
						"in charge calls you over. She has straight, red hair, split into two simple pigtails. Her features are unremarkable except for the freckles lightly " +
						"spotting her face, but you could reasonably describe her as cute. She mentioned her name once. It was a kind of flower... Lilly? Yeah, " +
						"that sounds right, Lilly Quinn.<p>"
						+ "Lilly gives you a thin smile that's somewhere between polite and friendly. <i>\"You seem to be doing alright for a beginner. Not " +
						"everyone takes to this sort of competition so naturally. In fact, the Benefactor who finances the Games has taken a particular interest in you. You have "
						+ "my sympathies.\"</i><p>"
						+ "OK, you'll take the bait this time. Why is this bad for you? Does this Benefactor have a history of sexually harassing his favorites?<p>"
						+ "<i>\"No, nothing like that. Our Benefactor is a believer in tough love. He's asked me to be a little harder on you than the other participants. "
						+ "If he's right about your potential, you'll rise to the challenge and end up stronger.\"</i><p>"
						+ "She grins and brushes one of her pigtails off her shoulder. She doesn't look particularly sympathetic.<br>"
						+ "<i>\"So here's the deal. Before each match, I'm going to assign you a handicap. It may be a restriction on what you're allowed to do, or something "
						+ "that will make you more vulnerable to your opponents. Ideally it should force you to adapt your strategies each night. As a bonus, you're more "
						+ "likely to end up at the mercy of a bunch of cute girls, which makes me smile. Sound good?\"</i><p>"
						+ "It doesn't sound like you have much of a choice, unless you plan to quit. Of course, if you were the sort of person who ran away from a challenge, "
						+ "you wouldn't be here now. You'll just have to deal with each handicap as they come.<p>"
						+ "<i>\"Cool. For the first rule, I'll start with something simple: for " +
						"tonight's match, you're only allowed to wear your underwear. Even when you come back here for a change of clothes, that's all you'll get.\"</i>";
				Global.flag(Flag.metLilly);
				type=Modifier.pantsman;
				choice.add(new SceneButton("Do it"));
				break;
			case MAYA:
				message+="When you arrive at the student union, you notice the girls have all gathered around Lilly. As you get closer, you notice Maya, the recruiter, standing next "
						+ "to her. She isn't usually present during a match, or at least you haven't seen her, so this must be a special occasion. Lilly gives you a nod of "
						+ "acknowledgement as you approach.<p>"
						+ "<i>\"Is everyone here? Good. We have a rare treat tonight.\"</i> She motions toward the visitor. <i>\"Maya, who you have all met, is going to join "
						+ "in this match as a special guest. She is a veteran of the Games and has probably forgotten more about sexfighting than any of you have ever learned. "
						+ "You wouldn't normally have an opportunity to face someone of her caliber, but she has graciously come out here to test you rookies.\"</i> "
						+ "She brushes one of her pigtails aside for dramatic effect.<p>"
						+ "Maya smiles politely and gives a small curtsy. <i>\"I rarely find an opportunity to compete anymore, but I like to indulge every once in awhile.\"</i> Her "
						+ "eyes meet yours and something in her piercing gaze makes you feel like a small prey animal. Despite feeling intimidated, you feel your cock stir in your pants "
						+ "against your will. <i>\"I may be a bit rusty, but I'll try to set a good example for you.\"</i><p>"
						+ "Lilly takes the lead again. <i>\"If any of you actually manage to make Maya cum, I'll give you multiple points for it. Otherwise you can just consider "
						+ "this a learning opportunity and a chance to experience an orgasm at the hands of a master.\"</i><p>";
				type = Modifier.maya;
				choice.add(new SceneButton("Start The Match"));
				break;
			case MARASICK:
				message+="When you arrive at the Student Union to prepare for your match, Mara is already there, being looked over by Lilly.<p>" +
						"<i>\"Are you sure you're feeling alright?\"</i> Lilly asks her. <i>\"You look a little... 'off' tonight.\"</i><p>" +
						"Mara shrugs, nonchalantly. <i>\"I had a long day in the lab, but I'll feel a lot better after blowing off some steam and getting covered with cum.\"</i><p>" +
						"Lilly looks like she wants to say something else, but Mara notices you and scampers over like a puppy. Before you have a chance to greet her, she pulls you into an intense kiss.<p>" +
						"She breaks the kiss for air after a little while, but keeps her arms around you. <i>\"I'm counting on you to provide the cum, so try not to lose to anyone before I find you.\"</i><p>" +
						"Mara seems like her normal, perky self. Maybe Lilly was worried about nothing.<p>";
				Global.flag(Flag.MaraDayOff);
				choice.add(new SceneButton("Start The Match"));
				break;
			case LILLYPHONE:
				message+="You run out of ways to kill time before the match, and decide to head over there early.<p>"
						+ "When you arrive at the starting point, you're the first competitor here. Lilly sits cross-legged on top of a desk in the corner, intent upon her laptop, and doesn't look up.<p>"
						+ "The second time you say hello and get no response, you decide to walk over and see what she's so focused on. Her laptop's running a freeform construction game that you're vaguely familiar with. As you watch, she chops down a tree, then methodically reduces the trunk to a stack of lumber and carries it away.<p>"
						+ "Lilly's avatar looks about as much like she does as the game can support, and wears a red-and-black plaid shirt, jeans, and pale brown workman's boots. She walks it over to a small base camp she's established and drops the wood on top of a formidably tall pile. The pile is next to similarly stacked piles of stone, ore, and what might be tanned leather. Lilly takes a notebook out of her vest pocket and checks off a tally mark.<p>"
						+ "You watch for a few minutes, since you've got nothing better to do, until an alarm on her watch beeps. It's exactly ten minutes until the official start of the Night Games. Lilly glances at it, then moves her character into a tent and logs off.<p>"
						+ "When she looks up, she does a double-take at you like she's forgotten who you are, then shuts her laptop. <i>\"Oh. Hi.\"</i><p>"								
						+ "You did try to get her attention, you point out.<p>"
						+ "<i>\"That's... entirely believable.\"</i><p>"
						+ "What are you making?<p>"
						+ "<i>\"Oh, my sister and I sometimes get together and collaborate on something,\"</i> Lilly says, and slides off the top of the desk. <i>\"She got this idea about trying to make a rollercoaster. I don't know if this game can actually support the physics involved, but...\"</i> She pauses, brushes some invisible dust off her clothes, and straightens up. <i>\"Some people build ships in bottles,\"</i> Lilly says, a bit defensively.<p>"
						+ "Even if that was weird, your personal weirdness threshold has been going up by leaps and bounds lately. You've played the same game, although you're a few major content patches behind, and you say as much.<p>"
						+ "Lilly brightens, and spends the next five minutes telling you all about what's changed since the last time you logged on. As it turns out, damn near everything.<p>"
						+ "She gets so enthused about it that she almost doesn't start the Games on time, but her watch goes off again: two-minute warning. Lilly glances at you, then flips to a clean page of her notebook and writes down her phone number.<p>"
						+ "<i>\"We should hang out,\"</i> she says. <i>\"Maybe the next time we both have a night off?\"</i><p>"
						+ "It's not a bad idea--Lilly's kind of cool, despite the built-in awkwardness of how she occasionally wants to bind your scrotum or something--but you're not sure how to interpret that.<p>"
						+ "She notices your indecision, then puts up one hand, palm out. <i>\"No, wait. I mean that. Just hang out. I'm gay.\"</i><p>"
						+ "Oh. Right.<p>"
						+ "<i>\"Plus, I see you naked six nights a week,\"</i> Lilly says. <i>\"Even if I wasn't gay, I don't think I'd want to sign up for more of that once I'm off the clock.\"</i><p>"
						+ "That's... entirely fair.<p>"
						+ "<i>\"But... yeah.\"</i> Another couple of competitors walk in behind you. <i>\"Give me a call when you get the chance.\"</i> Lilly grins at you, then walks to meet them.<p>"
						+ "Well, why not. You put her phone number in with the rest of your belongings, then get ready for the match.";
				Global.flag(Flag.Lillyphone);
				Roster.gainAffection(ID.PLAYER,ID.LILLY,2);
				Global.gui().message("<b>You've gained Affection with Lilly</b>");
				type=Modifier.normal;
				choice.add(new SceneButton("Start The Match"));
				break;
			default:
				Global.gui().loadPortrait(player, lilly);
				message+="You arrive at the student union with about 10 minutes to spare before the start of the match. You greet each of the girls and make some idle chatter with " +
						"them before you check in with Lilly to see if she has any custom rules for you.<p>";
				type=offer(player);
				if(type==Modifier.normal){
					message+="<i>\"Sorry "+player.name()+", there's no bonus money available tonight. Our Benefactor doesn't always give us the extra budget.\"<i/> She shrugs casually and " +
						"brushes her pigtail over her shoulder. <i>\"There's nothing wrong with having a normal match. You don't want to get so caught up in gimmicks that you forget " +
						"your fundamentals.\"</i> You give her a fairly neutral shrug and spend a few more minutes chatting with her before the match starts. She's surprisingly easy to " +
						"talk to. You eventually head to your start point and arrive just before 10:00.";
					choice.add(new SceneButton("Start The Match"));
				}
				else{
					switch(type){
					case pantsman:
						message+="<i>\"So, "+player.name()+", what would you say to another match in your underwear? For some reason, that just amuses the hell out of me. " +
								"You get to run around the campus looking like a fool, and the girls still get the enjoyment of pulling down the most fun bit of clothing during a fight.\"</i> ";
						break;
					case nudist:
						message+="<i>\"Funny thing "+player.name()+", me and the other girls were just talking about you.\"</i> There's no way that's good. <i>\"I asked them all what their least "+
									"favorite thing about you is.\"</i> Nope. Definitely not good. <i>\"After some discussion they all agreed that your worst quality is your insistence on " +
									"so frequently wearing clothing. So, I think you should spend the match naked and see how well you do. Based on a quick, informal poll 100% of the girls think "
									+ "this is a good idea. What do you say?\"</i>";
						break;
					case norecovery:
						message+="Lilly waits until you approach and holds up a small metal... something. <i>\"This unique accessory just fell into my lap, and it made me think of a new " +
								"handicap for you. It's a peculiar little toy that's designed to inhibit a man's ejaculation, but it's not so effective that it would prevent an opponent " +
								"from getting you off. In theory, it should keep you from orgasming from masturbation or when you win. You'll have to fight much more defensively or get " +
								"good at forcing a draw.\"</i> She shrugs. <i>\"That's the theory at least. You'll be my guinea pig for this.\"</i>";
						break;
					case vibration:
						message+="<i>\"Do you like toys, "+player.name()+"? I thought of a way to make your matches harder that you'll still enjoy.\"</i> She holds up a small plastic ring. " +
								"<i>\"Vibrating cock-ring,\"</i> she explains. <i>\"This little fellow will keep you horny and ready to burst. I can call it a little lady instead if it helps.\"</i>";
						break;
					case vulnerable:
						message+="<i>\"I've got a simple handicap for you tonight. You've probably come across some sensitization potions that temporarily enhance your sense of touch, right? " +
								"There's a cream that has basically the same effect, but it'll last for several hours. The deal is that I'll rub the cream into your penis, making you much " +
								"more vulnerable during the match, but hey, it'll probably feel pretty good. Interested?\"</i>";
						break;
					case pacifist:
						message+="Lilly gives you a long, appraising look. <i>\"I'm trying to decide what sort of man you are. You strike me as a good guy, probably not the type " +
								"to hit a girl outside a match. I propose you try being a perfect gentleman by refusing to hit anyone during tonight's match too. So no slapping, " +
								"kicking, anything intended to purely cause pain. What do you say?\"</i>";
						break;
					case notoys:
						message+="<i>\"I've only got a small bonus available tonight, so I have a simple little handicap for you. Leave your sex toys under the bed tonight. You're better off " +
								"getting some practice with your fingers, tongue, or whatever other body parts you like sticking into girls. Liquids, traps, any consumables are fine, only " +
								"toys are off limits. I don't expect this to give you much trouble.\"</i>";
						break;
					case noitems:
						message+="<i>\"Tell me "+player.name()+", are you the sort of player who spends all his winnings on disposable toys and traps to give yourself the edge? You'd " +
								"probably be better off saving the money and relying more on your own abilities. See if you can go the entire night without using any consumable items.\"</i>";
						break;
					case hairtie:
						message+="<i>\"I've got another handicap for you. It's simple, affordable, and painful.\"</i> She holds up a small elastic band. It looks like a hairtie.<p>"
								+ "<i>\"Yup. It's a tactic I liked to use when I was a participant. I wrap this around your scrotum to hold your balls in place, then it hurts "
								+ "much more if you get kicked there. Simple.\"</i><p>"
								+ "That sounds... unpleasant. Lilly idly toys with one of her pigtails, and you notice the other one has come undone. That explains where she got the tie. "
								+ "Maybe she forgot to prepare a handicap in advance.<p>"
								+ "<i>\"Don't worry, I promise there's no risk of serious damage. Just focus on not getting hit in the groin and the extra pain won't even be an issue.\"</i>";
						break;
					case ticklish:
						message+="Lilly shows you a small vial of clear liquid. It reminds you a bit of the hypersensitivity flasks you've seen during matches.<p>"
								+ "<i>\"Close, but this version is less intense and more localized. It should be better suited for this handicap. I'm planning to "
								+ "apply it to sensitive, but not traditionally erogenous areas. Ideally it should make you more ticklish, without making you "
								+ "too vulnerable to pleasure. I'm trying to go easy on you with this one. Sound good?\"</i>";
						break;
					case lameglasses:
						message+="<i>\"OK, your handicap tonight is to wear these.\"</i> She holds up a pair of thick, ugly glasses. They look like something an old woman would wear.<p>"
								+ "You try them on to see how much they'll affect your vision, but to your surprise, you discover the lenses are purely cosmetic. Other than looking lame, "
								+ "how is this suppose to be a handicap?<p>"
								+ "<i>\"Oh those aren't just any lame glasses. Those glasses are specially designed to be as lame as possible. They will actively suppress your Mojo, preventing "
								+ "you from usually any of the cool moves you're fond of.\"</i>";
						break;
					case mittens:
						message+="<i>\"Hey, I thought of a new handicap for you. Wear these.\"</i> She holds up a pair of white mittens, which seem more than a little out of place in "
								+ "a sexfight. <i>\"Have you ever tried to finger a girl with mittens on? It's hard.\"</i><p>"
								+ "Your thoughts must be showing on your face, because Lilly starts to get flustered and defensive.<br>"
								+ "<i>\"OK, so maybe not all of my handicap ideas are solid gold! It will make the match harder for you, that's all that matters.\"</i>";
						break;
					case slippery:
						message += "<i>\"Hey, I picked up some oil-based lube for your next handicap.\"</i> She holds up a bottle of lotion that you're pretty sure you've seen before. <i>\"This stuff is non-water soluble and should keep you slippery all night, even if you decide to take a quick shower.\"</i><p>" +
								"<i>\"On the one hand, it's going to make you harder to hold on to, which sounds pretty fun to watch.\"</i> She gives you a casual smile. <i>\"On the other hand, you're going to be a lot easier to pleasure when every part of your body is wet and slippery. What do you think?\"</i><p>";
						break;
					case furry:
						message += "<i>\"I've got a new and exciting handicap for tonight.\"</i> She does look and sound more enthusiastic than usual. <i>\"This one's a little different. Instead of impairing you, I'm planning to give all your opponents an exotic edge.\"</i> <p>" +
								"<i>\"We've gotten our hands on this serum that'll temporarily bring out the beast in people. They'll get enhanced strength, reflexes, and some cute animal ears. I'm looking forward to seeing how they'll look.\"</i><p>";
					}
					if(!Global.checkFlag(Flag.challengemode)){
						message+="<p><i>\"I'll give you a "+type.percentage()+"% bonus for this match if you accept.\"</i>";
					}
					choice.add(new SceneButton("Do it"));
					if(!Global.checkFlag(Flag.challengemode)){
						choice.add(new SceneButton("Not interested"));
					}
				}
		}
		choice.add(new SaveButton());
		Global.gui().prompt(message, choice);
	}
	private PreEvent checkFlags(){
		Global.unflag(Flag.victory);
		if(player.getLevel()>=5&&!Global.checkFlag(Flag.metLilly)){			
			return PreEvent.LILLYINTRO;
		}
		if(Global.checkFlag(Flag.challengemode)&&!Global.checkFlag(Flag.metLilly)){
			return PreEvent.CHALLENGEMODE;
		}
		if(player.getRank()>0 && Scheduler.getMatchNumber()==Constants.SEASONLENGTH+1){
			return PreEvent.MAYA;
		}
		if(player.getRank()>=4 && Roster.get(ID.MARA).getPure(Attribute.Temporal)>=1&&!Global.checkFlag(Flag.MaraDayOff)&&Global.checkFlag(Flag.metJett)&&Roster.getAffection(ID.PLAYER,ID.MARA)>=40){
			return PreEvent.MARASICK;
		}
		if(player.getLevel()<5){
			return PreEvent.EARLYGAME;
		}
		if(player.getRank()>=2 && !Global.checkFlag(Flag.Lillyphone)){
			return PreEvent.LILLYPHONE;
		}
		return PreEvent.NONE;
	}
	private Modifier offer(Player player){
		ArrayList<Modifier> available = new ArrayList<Modifier>();
		if(Global.random(10)>=4 && !Global.checkFlag(Flag.challengemode)){
			return Modifier.normal;
		}
		else{
			if(player.isWearing(ClothingType.UNDERWEAR)){
				available.add(Modifier.pantsman);
			}
			available.add(Modifier.nudist);
			if(!player.has(Trait.insatiable)){
				available.add(Modifier.norecovery);
			}
			if(!player.has(Trait.hairtrigger)){
				available.add(Modifier.vulnerable);
			}
			if(!player.has(Trait.ticklish)){
				available.add(Modifier.ticklish);
			}
			if(!player.has(Trait.achilles)){
				available.add(Modifier.hairtie);
			}
			if(Global.checkFlag(Flag.Kat)){
				available.add(Modifier.furry);
			}
			available.add(Modifier.slippery);
			available.add(Modifier.pacifist);
			available.add(Modifier.lameglasses);
			available.add(Modifier.mittens);
			if(player.has(Toy.Dildo)||player.has(Toy.Dildo2)||player.has(Toy.Tickler)||player.has(Toy.Tickler2)){
				available.add(Modifier.notoys);
			}
			available.add(Modifier.noitems);
		}
		return available.get(Global.random(available.size()));
	}
	@Override
	public void respond(String response) {
		String message = "";
		ArrayList<KeyableButton> choice = new ArrayList<KeyableButton>();		
		if(response.startsWith("Start")){
			Global.gui().clearText();
			Scheduler.createMatch(type);
		}
		else if(response.startsWith("Not")){
			type=Modifier.normal;
			Global.gui().clearText();
			Scheduler.createMatch(type);
		}
		else if(response.startsWith("Do")){
			switch(type){
			case pantsman:
				player.top.clear();
				player.bottom.clear();
				for(Clothing article: player.outfit[Player.OUTFITBOTTOM]){
					if(article.getType()==ClothingType.UNDERWEAR){
						player.wear(article);
					}
				}
				for(Clothing article: player.outfit[Player.OUTFITTOP]){
					if(article.getType()==ClothingType.TOPUNDER){
						player.wear(article);
					}
				}
				message+="Lilly smiles with her hands on her hips. <i>\"Glad to hear it. We'll hang on to the rest of your clothes until the match is over. Boxers-only starts " +
						"now.\"</i> She wants you to undress here before the match even starts? You hesitate as you realize your opponents are all watching you curiously. Some " +
						"of Lilly's assistants are still around too.<p>"
						+ "She laughs when she notices your reluctance. <i>\"Are you seriously getting embarrassed about this? As if " +
						"anyone in this room hasn't seen you naked on a regular basis.\"</i><p>"
						+ "She does have a point. You quickly strip off your shirt and pants and prepare for " +
						"the match.";
				break;
			case nudist:
				player.nudify();
				message+="You agree to Lilly's rule and start to strip off your clothes. You try to appear nonchalant about it, but you can't help reddening a bit when your " +
						"opponents start cheering you on.<p>"
						+ "You're at least kinda used to the other participants seeing you naked, but you're usually doing your best to undress them at the same time. It's a much "
						+ "different experience being the only naked person in a room full of clothed girls. Lilly's assistants, most of whom are girls, seem to be getting distracted "
						+ "from their tasks and are enjoying the show. Worse still, your dick is starting to harden in response to all the attention, something that has not gone unnoticed "
						+ "by your audience.<p>"
						+ "Lilly stifles a laugh as you hand over your clothes. <i>\"You see? You're way more popular already.\"</i>";
				break;
			case norecovery:
				lilly.setBlush(1);
				player.setModifier(Trait.nosatisfaction);
				message+="<i>\"Come on,\"</i> Lilly says as she leads you into a nearby room. <i>\"I need to handle your naughty bits and I figured you would prefer some privacy.\"</i> It's " +
						"true, but you're a little surprised by her consideration. She generally seems to enjoy making you uncomfortable.<p>"
						+ "<i>\"Ok, take off your pants and underwear. " +
						"Don't be shy, you have nothing I haven't seen before, and nothing I'm interested in.\"</i> You bare your lower half and Lilly fixes the metal ring onto the " +
						"base of your penis. It's snug, but not uncomfortable, though it presses against the base of your scrotum in a way that feels weird.<p>"
						+ "<i>\"Now, this may be " +
						"a little awkward for both of us, but I need you to try to masturbate to completion so we can verify that it works as intended.\"</i> This explains the privacy. " +
						"Is she just doing this to screw with you?<p>"
						+ "She shakes her head with a serious expression. <i>\"If it turns out that accessory actually gives you an unfair advantage, " +
						"I can't let you wear it during the match. I do take my job seriously.\"</i><p>"
						+ "You feel a little bad for doubting her, so you start jerking off without complaining.<p>"
						+ "It " +
						"takes you some time to get hard under Lilly's scrutinizing stare, but you eventually make yourself fully erect and masturbate in earnest. At full mast, the " +
						"metal ring creates an oppressive tightness. No matter how much you try, you find yourself completely unable to cum.<p>"
						+ "Eventually you notice Lilly trying to hide " +
						"her expression, her shoulders shaking with mirth.<p>"
						+ "<i>\"I'm sorry,\"</i> she says between giggles. <i>\"I know it's really rude to laugh, but your expression is just too " +
						"funny. I'm really sorry.\"</i><p>"
						+ "She manages to calm down enough to gesture for you to stop. <i>\"Ok, I'm convinced the accessory effectively prevents masturbation. The " +
						"more important test is whether you can still be made to orgasm.\"</i><p>"
						+ "She grasps your dick and begins to stroke you skillfully. You immediately feel the pleasure start " +
						"to build in your frustrated cock and in seconds she brings you to a spurting climax. <i>\"That worked much better than I expected,\"</i> Lilly comments as she " +
						"pulls a tissue out of her pocket to clean up. <i>\"Hopefully you're not too worn out. The match hasn't even started yet.\"</i>";
				break;
			case vibration:
				message+="You agree to Lilly's rule and reach out to take the cock-ring, but she shakes her head. <i>\"I need to put it on you to make sure it's positioned correctly. Don't worry, " +
						"you don't need to undress.\"</i> She steps close to you and slips her hand down the front of your pants and underwear. Her fingers dexterously manipulate your dick as she " +
						"maneuvers the ring onto it. From her expression, it looks like she's concentrating on her task rather than trying to entice you, but her closeness and her touch still " +
						"have an effect on you. <i>\"Don't feel embarrassed,\"</i> she says with a reassuring smile. <i>\"It's actually easier to put this on when you're a little hard.\"</i> She fits the " +
						"cock-ring in place and removes her hand.<p>"
						+ "<i>\"Time to test.\"</i> She holds up a small remote and switches it on. Your hips jerk at the intense sensation on your cock. You " +
						"have to endure this for three hours? <i>\"The intensity will automatically modulate to keep you from going numb, but after a few minutes, you'll partially adapt to it. " +
						"I'll hang onto the remote during the match.\"</i> She hits the button again and the vibration stops. <i>\"If this ends up making you cum, I won't be offended if you think " +
						"about me.\"</i>";
				break;
			case vulnerable:
				lilly.setBlush(1);
				player.setModifier(Trait.hairtrigger);
				message+="Lilly leads you into the men's bathroom to apply the hypersensitivity cream. She removes your pants and boxers and starts to rub the cream onto your dick. The stuff works " +
						"fast and you can't help letting out a quiet moan at her soft touch. She treats the process very clinically and seems almost bored to be handling your manhood. <i>\"I hope " +
						"you don't take my lack of interest personally,\"</i> she says, as if reading your mind. <i>\"You seem nice, and I guess you're reasonably good looking. You just happen to be " +
						"the wrong gender.\"</i> Ah, that explains a bit. That must make this more awkward for her than it would otherwise be.<p>"
						+ "She shrugs. <i>\"I don't mind. I'm used to competing against " +
						"men, and I have some pride in my technique.\"</i> As she says this, her hand motions turn into smooth, pleasurable strokes. <i>\"Besides, with the typical gender ratio in these " +
						"Games, I'm better off than the straight girls. Are you feeling any effect yet?\"</i> <p>"
						+ "You certainly are. Between the cream and her skilled handjob, you can barely stay standing. " +
						"She continues stroking you until you shoot your load into her hands. <i>\"That was quick. I'm going to assume the cream was effective rather than you having a fetish for girls " +
						"who aren't attracted to you.\"</i>";
				break;
			case pacifist:
				message+="Lilly flashes you a broad grin and slaps you on the back uncomfortably hard. <i>\"Just so everyone's aware,\"</i> she calls out to your opponents, <i>\""+player.name()+
						" has sworn that he won't hurt any girls tonight. So no matter how much anyone taunts him, whips him, or kicks him in the balls, he can't retaliate in " +
						"any way.\"</i><p>"
						+ "As you try to ignore a growing sense of dread, she leans close to your ear and whispers, <i>\"Good luck.\"</i>";
				break;
			case notoys:
				message+="You agree to Lilly's terms and hand over all the sex toys you have on you. She carefully looks over each of the devices, which makes you feel awkward for reasons " +
						"you can't quite explain.<p>"
						+ "<i>\"Is this really the best you have? You're going to need to up your game soon if you want to be competitive. These don't even match up " +
						"to the toys I have for personal use.\"</i>";
				break;
			case noitems:
				message+="Lilly nods, satisfied with your answer. <i>\"Excellent. Traps and items are so impersonal. Besides, who do you think has to clean them up at the end of the night? " +
						"You're better off without them.\"</i><p>"
						+ "It sounds more like she came up with this handicap to save herself some cleanup work, than as a legitimate challenge.";
				break;
			case hairtie:
				player.setModifier(Trait.achilles);
				message+="Lilly nods and leads you into the men's bathroom. She undresses you and applies the hairtie. You're more than a little nervous having her handle your "
						+ "delicates, but she's surprisingly careful. She gently wraps the band around the base on your scrotum, trapping your testicles away from your body.<p>"
						+ "<i>\"How is that? There shouldn't be any pain unless you actually get hit. Don't worry, we don't need to do a test kick or anything like that.\"</i><p>"
						+ "You take a few experimental steps. It's a little uncomfortable, but it won't slow you down at all. You weren't expecting Lilly to be so gentle. "
						+ "Given her clear distaste for you, you had prepared for her to be a little rough with your junk. She actually looks a little hurt when you tell her so.<p>"
						+ "<i>\"Hey, just because I'm not attracted to you doesn't mean I don't like you. This handicap is just part of my job. I have no reason to want to hurt you. "
						+ "I have a thing for girls on top, not guys in pain.\"</i><p>"
						+ "You quickly apologize for misjudging her. You're convinced her motives are not malicious.<p>"
						+ "Lilly suddenly unleashes a snap-kick that stops less than an inch from your jewels.<p>"
						+ "<i>\"There, see? If I did have something against you, you'd be a curled up on the floor right now. Quod erat demonstrandum.\"</i><p>"
						+ "OK, you're slightly less convinced now. Still, she's probably not malicious.";
				break;
			case ticklish:
				lilly.setBlush(1);
				player.setModifier(Trait.ticklish);
				message+="Lilly gestures to a couple of the girls who are helping with preparations, then leads you to the bathroom. <i>\"This will take some work to "
						+ "apply, and I don't feel like doing it all myself.\"</i> She leans close to you and lowers her voice. <i>\"These two both have a thing for "
						+ "ticklish boys, so I'm doing them a bit of a service too.\"</i><p>"
						+ "The two assistants undress you and each take a paintbrush, coated in the sensitivity liquid. At Lilly's direction, they start painting your "
						+ "body with the clear substance.<p>"
						+ "<i>\"Focus on ticklish areas. Be careful not to get any on his genitals or nipples. We want to make him more ticklish, not make him a quick shot.\"</i><p>"
						+ "This feels a bit demeaning. You just stand there naked, holding your arms up to give the girls easy access. You don't even know the names of the girls who "
						+ "are painting your naked body. They do their jobs quietly, but based on their expressions they're enjoying this quite a bit, especially when the "
						+ "solution starts taking effect.<p>"
						+ "Pretty soon, the ticklish sensation of their paintbrushes starts to become more pronounced. You struggle to keep still as the brushes focus on "
						+ "your armpits and thighs. You can tell the girls are starting to focus more on trying to make you squirm than applying an even coat. Lilly notices "
						+ "the change too.<p>"
						+ "<i>\"That should be enough sensitivity solution. Make sure you test the areas you coated to see how much more ticklish they are.\"</i><p>"
						+ "With their supervisor's blessing, the two girls get much bolder, attacking you with both hands. Forget trying to stay still, you're "
						+ "effectively incapable of defending yourself against their relentless tickling. You struggle and writhe while Lilly looks on with amusement. "
						+ "These girls are all sadists!<p>"
						+ "One of the girls suddenly grabs your dick, which is the first time you notice you're rock hard. You're far too overwhelmed to try to resist as "
						+ "she jerks you off. She brings you to orgasm much faster than her relatively unskilled handjob should be able to. You end up spraying cum all over "
						+ "the bathroom floor.<p>"
						+ "Lilly claps twice to get her assistants' attention. <i>\"OK you two, that's enough. He still needs to compete tonight and you two need to clean "
						+ "up this mess.\"</i>";
				break;
			case lameglasses:
				player.setModifier(Trait.lameglasses);
				message+="You agree to wear the glasses, though you're still skeptical of their effectiveness. Surely some dumb-looking glasses can't have "
						+ "any significant effect on your combat capability, right?<p>"
						+ "Of course you remember Aesop telling you these Games are as mental as they are physical. Right now your Mojo is feeling pretty off.";
				break;
			case mittens:
				player.setModifier(Trait.mittens);
				lilly.setMood(Emotion.angry);
				message+="You take the mittens and put them on with quite a bit of skepticism. You hold up your hands to show Lilly how silly you look.<p>"
						+ "Lilly's face flushes a bit as she snaps at you. <i>\"I don't care how it looks, it's a legitimate handicap! I'll show you!\"</i><p>"
						+ "She grabs your hand and shoves it down the front of her pants. Your fingers are right on her vulva, but you can't feel much through "
						+ "the thick material of the mittens and you don't have any dexterity to speak of. Her cheeks turn slightly redder as you continue touching "
						+ "her, but her expression stays mostly stoic.<p>"
						+ "<i>\"See? I can barely feel anything. Now you'll need to come up with more creative ways to get girls off.\"</i>";
				break;
			case slippery:
				lilly.setBlush(1);
				message += "Lilly brings you into the bathroom along with a few female helpers. She tosses the bottle to one of them. <i>\"Undress him and oil up his torso. Play close attention to the sensitive bits. You can skip the limbs and head.\"</i><p>" +
						"The assistants quickly strip you down and start applying the lubricant. They're quite efficient, but this seems like overkill. This is really a one person job. You could even rub it on yourself without much trouble.<p>" +
						"Lilly chuckles. <i>\"It's good for morale.\"</i> One of the girls oiling you up winks at you while rubbing the head of your penis. You feel another girl stick two fingers into your butt to spread the oil there. <i>\"Combatants get all the fun. I have to throw a bone to the support team sometimes.\"</i><p>" +
						"The feminine hands playing with your body quickly work you into a full erection. <i>\"Oh dear, someone's getting excited.\"</i> Lilly comments with insincere concern. <i>\"You don't want to go into the match ready to burst. Better let these girls handle that for you.\"</i><p>" +
                        "You give a nod of resignation and the assistants start groping you more aggressively. One of the girls focuses exclusively on your penis, stroking the shaft with one hand while teasing the glans with the other. Another girl massages your balls with both hands. A third " +
                        "pinches and teases your nipples. None of them are in the same class as your opponents, but it doesn't much matter when they're ganging up on you.<p>" +
                        "Pretty soon, they bring you to orgasm and you shoot your load into one of the assistants' hands. They each give you a peck on the cheek when Lilly dismisses them.";
				break;
			case furry:
				message += "Lilly gives you a thumbs up. <i>\"Great! The girls have already agreed with the idea. Have fun being everyone's prey.\"</i>";
			}
			Global.gui().loadPortrait(player, lilly);
			Global.modCounter(Flag.HandicapMatches, 1);
			choice.add(new SceneButton("Start The Match"));
			Global.gui().prompt(message, choice);
		}
	}
	private enum PreEvent{
		NONE,
		EARLYGAME,
		LILLYINTRO,
		CHALLENGEMODE,
		MAYA,
		MARASICK,
		LILLYPHONE
	}
	@Override
	public String morning() {
		// TODO Auto-generated method stub
		return "";
	}
	@Override
	public String mandatory() {
		// TODO Auto-generated method stub
		return "";
	}
	@Override
	public void addAvailable(HashMap<String, Integer> available) {
		return;
	}
}
