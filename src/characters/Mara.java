package characters;

import daytime.Daytime;
import global.*;

import items.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import Comments.CommentGroup;
import Comments.CommentSituation;
import skills.*;
import stance.Stance;
import status.Hypersensitive;
import status.Oiled;

import combat.Combat;
import combat.Result;

import actions.Action;
import actions.Move;
import actions.Movement;
import actions.Resupply;
import areas.Area;
import status.Stsflag;

public class Mara implements Personality {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3812726803607189573L;
	public NPC character;
	public Mara(){
		character = new NPC("Mara",ID.MARA,1,this);
		character.outfit[0].add(Clothing.bra);
		character.outfit[0].add(Clothing.Tshirt);
		character.outfit[1].add(Clothing.underwear);
		character.outfit[1].add(Clothing.shorts);
		character.closet.add(Clothing.bra);
		character.closet.add(Clothing.Tshirt);
		character.closet.add(Clothing.underwear);
		character.closet.add(Clothing.shorts);
		character.change(Modifier.normal);
		character.mod(Attribute.Cunning, 2);
		character.mod(Attribute.Perception, 2);
		character.gain(Consumable.ZipTie,10);
		character.gain(Flask.Lubricant,5);
		Global.gainSkills(character);
		character.add(Trait.female);
		character.add(Trait.petite);
		character.add(Trait.dexterous);
		character.add(Trait.ticklish);
		character.setUnderwear(Trophy.MaraTrophy);
		character.plan = Emotion.sneaking;
		character.mood = Emotion.confident;
		character.strategy.put(Emotion.hunting, 2);
		character.strategy.put(Emotion.sneaking, 5);
		character.preferredSkills.add(Tie.class);
		character.preferredSkills.add(UseOnahole.class);
		character.preferredSkills.add(UseFlask.class);
		character.preferredSkills.add(WindUp.class);
		character.preferredSkills.add(MatterConverter.class);
		character.preferredSkills.add(Stomp.class);
		character.preferredSkills.add(SpawnSlime.class);
		character.preferredSkills.add(Footjob.class);
		if(Global.checkFlag(Flag.PlayerButtslut)){
			character.preferredSkills.add(AssFuck.class);
			character.preferredSkills.add(Turnover.class);
			character.preferredSkills.add(Strapon.class);
			character.preferredSkills.add(FingerAss.class);
			if(Global.random(2) == 0){
				character.gain(Toy.Strapon);
			}
		}

	}
	@Override
	public Skill act(HashSet<Skill> available,Combat c){
		HashSet<Skill> mandatory = new HashSet<Skill>();
		HashSet<Skill> tactic = new HashSet<Skill>();	
		Skill chosen;
        for(Skill a:available){
            if(a.toString()=="Command"||a.toString().equalsIgnoreCase("Ass Fuck")){
                mandatory.add(a);
            }
            if(character.is(Stsflag.orderedstrip)){
                if(a.toString()=="Undress"||a.toString()=="Strip Tease"){
                    mandatory.add(a);
                }
            }
			if(character.has(Trait.strapped)){
				if(a.toString()=="Turn Over"||a.toString()=="Mount"){
					mandatory.add(a);
				}
			}
		}
		if(!mandatory.isEmpty()){
			Skill[] actions = mandatory.toArray(new Skill[tactic.size()]);
			return actions[Global.random(actions.length)];
		}
		ArrayList<HashSet<Skill>> priority = character.parseSkills(available, c);
		if(Global.checkFlag(Flag.hardmode)&&Scheduler.getMatch().condition!=Modifier.quiet){
			chosen = character.prioritizeNew(priority,c);
		}
		else{
			chosen = character.prioritize(priority);
		}
		if(chosen==null){
			tactic=available;
			Skill[] actions = tactic.toArray(new Skill[tactic.size()]);
			return actions[Global.random(actions.length)];
		}
		else{
			return chosen;
		}
	}

	@Override
	public Action move(HashSet<Action> available, HashSet<Movement> radar, Match match){
		Action proposed = character.parseMoves(available, radar, match);
		return proposed;
	}

	@Override
	public void rest(int time, Daytime day){
		if(character.rank>=1){
			if(character.money>0){
				day.visit("Workshop", character, Global.random(character.money));
			}
		}
		if(!(character.has(Toy.Strapon)||character.has(Toy.Strapon2)) && Global.checkFlag(Flag.PlayerButtslut)
				&& character.money>=300 && character.getPure(Attribute.Seduction)>=5){
			character.gain(Toy.Strapon);
			character.money-=300;
		}
		if(!(character.has(Toy.Onahole)||character.has(Toy.Onahole2))&&character.money>=300){
			character.gain(Toy.Onahole);
			character.money-=300;
		}
		if(!(character.has(Toy.Tickler)||character.has(Toy.Tickler2))&&character.money>=300){
			character.gain(Toy.Tickler);
			character.money-=300;
		}
		if(!(character.has(Toy.Dildo)||character.has(Toy.Dildo2))&&character.money>=250){
			character.gain(Toy.Dildo);
			character.money-=250;
		}
		if(!(character.has(Toy.Crop)||character.has(Toy.Crop2))&&character.money>=200){
			character.gain(Toy.Crop);
			character.money-=200;
		}
		if(!(character.has(Toy.Strapon)||character.has(Toy.Strapon2))&&character.money>=600&&character.getPure(Attribute.Seduction)>=20){
			character.gain(Toy.Strapon);
			character.money-=600;
		}
		if(!(character.has(Toy.Strapon)||character.has(Toy.Strapon2)) && Global.checkFlag(Flag.PlayerButtslut)
				&& character.money>=300 && character.getPure(Attribute.Seduction)>=5){
			character.gain(Toy.Strapon);
			character.money-=300;
		}
		character.visit(2);
		String loc;
		ArrayList<String> available = new ArrayList<String>();
		available.add("Hardware Store");
		available.add("Black Market");
		available.add("XXX Store");
		available.add("Bookstore");
		if(character.rank>0){
			available.add("Workshop");
			character.gain(Potion.SuperEnergyDrink);
		}
		available.add("Play Video Games");
		for(int i=0;i<time-2;i++){
			loc = available.get(Global.random(available.size()));
			day.visit(loc, character, Global.random(character.money));
		}
		if(character.getAffection(Global.getPlayer())>0){
			Global.modCounter(Flag.MaraDWV, 1);
		}
	}

	@Override
	public String bbLiner(){
		if(character.getAffection(Global.getPlayer())>=25){
			return "Mara gives you a sympathetic look with just a hint of a grin. <i>\"Sorry, "+Global.getPlayer().name()+", but you know the drill by now. I've got to be "
					+ "cruel to be kind. I promise I'll make it up to you soon.\"</i>";
		}
		switch(Global.random(3)){
		case 1:
			return "<i>\"Bingo!  I think I got both of them that time!\"</i> Mara exclaims, looking proud of herself.";
		case 2:
			return "<i>\"You should probably put some ice on those balls tonight to keep them from swelling up.\"</i> "
					+ "Mara says, cracking a smug smile.";
		default:
			return "Mara gives you a look of not quite genuine concern. <i>\"That must have really hurt. Sorry for scrambling your eggs. I feel really bad about that. Also for " +
			"lying just now. I'm not actually that sorry.\"</i>";
		}
		
	}

	@Override
	public String nakedLiner(){
		return "Mara gives an exaggerated squeal and covers herself, but can't quite conceal her excited grin. <i>\"You fiend! How dare you strip a helpless, innocent girl like this?! "
				+ "Are you planning to do brutish and naughty things to my naked body?\"</i>";
	}

	@Override
	public String stunLiner(){
		return "Mara lets out a slightly pained whimper. <i>\"Go easy on me. I'm not really the masochistic type.\"</i>";
	}

	public String winningLiner(){
		// TODO Auto-generated method stub
		return "";
	}

	@Override
	public String taunt(){
		return "<i>\"If you want me to get you off so badly,\"</i> Mara teases coyly. <i>\"You should have just said so from the start. You don't need to put up this token resistance.\"</i>";
	}

	@Override
	public String victory(Combat c,Result flag){
		Character opponent=c.getOther(character);
		character.clearGrudge(opponent);
		character.arousal.empty();
		if(flag==Result.anal){
			if(Global.getValue(Flag.PlayerAssLosses)==0){
				Global.flag(Flag.tookAssMara);
				Global.modCounter(Flag.PlayerAssLosses, 1);
				opponent.arousal.set(opponent.arousal.max()/3);
				return "You realise you're too far gone to resist, and it being your first time, you're thankful that Mara's strapon doesn't seem to be overly thick. "
						+ "However, it does seem to be unnaturally good at finding your prostate with every stroke, and sliding over it at just the right angle to "
						+ "make you gasp at the stimulation. Mara appears to be enjoying each gasp immensely, an amused grin on her face as she listens to your "
						+ "flustered reactions to your previously untouched asshole being forcibly played with. <i>\"I've heard the prostate is like the male "
						+ "equivalent of a g-spot. How would you like to cum from prostate "
						+ "stimulation alone?\"</i> Mara asks, although you get the idea you aren't going to be given a choice in the matter. She pants as she "
						+ "continues to piston in and out of you, the strapon still angled to rub up against your lovebutton incessantly. You let out a low moan of "
						+ "pleasure, nearing your limit - Mara redoubles her efforts, seemingly encouraged by your arousal. <i>\"I'll take that as a yes,\"</i> she giggles, "
						+ "<i>\"or maybe a 'yes mistress'? This position definitely suits you!\"</i> You're barely even listening to her teasing, as one last crushing blow to "
						+ "your prostate makes you blow your load uncontrollably, your legs buckling as Mara holds your waist up and doesn't stop humping you. <p>"
						+ "<i>\"Oh, I forgot to tell you I wanted to try something.\"</i> she whispers in your ear. <i>\"I wanted to see if the prostate was so "
						+ "much like a g-spot that I could use it to make you cum twice.\"</i> You let out a little moan as you realise she's serious, and feel a "
						+ "delicate hand curl itself around your softening cock. Suddenly being given attention for the first time in a while, it rapidly stiffens "
						+ "again, Mara jerking it in short strokes while continuing to peg you deeply. You lower your head to the ground and groan as the intense "
						+ "feelings overstimulate and overwhelm you, and before long Mara's teasing has you rock hard and aching to cum again.<p>"
						+ "Unexpectedly, she pulls out of you at the last minute and stands up with a smile. <i>\"You're almost there? Looks like males can cum twice that way. Good to know. Well, "
						+ "I hope you had as much fun as I did - good luck out there, I wouldn't be surprised if Jewel or Angel were hanging around hoping for "
						+ "second prize. They'll probably be mad I took your ass first.\"</i> Mara turns and walks away with your clothes, leaving you dumbfounded.<p>"
						+ "And still very horny.";
			}
			Global.modCounter(Flag.PlayerAssLosses, 1);
			return "The sensations coming from your prostate are too much as your arms give out below you. Mara doesn't let up either, grinding the head of the strap on over your " +
					"prostate. <i>\"I've read that the prostate is the male equivalent of a g-spot,\"</i> she pants as she continues her assault on your ass. <i>\"I'd like to see if I can " +
					"make you come without stimulating your penis.\"</i> she continues. You don't really listen as your brain is about to short circuit and your dick is about to give " +
					"up the ghost. One final thrust from Mara sends you over the edge and you shudder as you come hard. <i>\"Looks like men really can come from prostate stimulation alone.\"</i> " +
					"Mara states, apparently more interested in the biology of the act than anything else. Mara turns you over revealing that your dick is still hard despite the " +
					"mind numbing orgasm you just went through. <i>\"Once again, you rise to the occasion.\"</i> she smiles down to you as she slowly lowers herself onto you. <i>\"Hope you " +
					"know how to give as well as take.\"</i> She jokes as she begins to work up a steady rhythm. Not to be outdone you quickly shift yourself and begin to thrust into her " +
					"earnestly, intent on showing just how well you can 'give'. Soon Mara is collapsed on top of you breathing hard and her eyes unfocused. You stand and take your leave. You may have lost the battle but the war is far from over.";
		}
		if(character.pet != null){
			return "Mara's slime has taken over this battle, to the point where Mara has actually stepped back and "
					+ "is now a spectator. The slime has managed to wrap itself around your legs up to your knees, keeping you rooted to one spot. You struggle, hoping to pull yourself free "
					+ "of the gelatinous mess. However, no matter which way you go or how much you try to pull, your legs are not moving. "
					+ "You look at Mara, curious as to why she isn't pressing her advantage, but she just smiles as she watches you struggle "
					+ "helplessly. You see a bit of desire in her eyes as she watches your naked cock swing back and forth as you flail about, "
					+ "but there is no hint to what her plan is. You look away from her and begin trying to see if there is something you can "
					+ "use to help pull yourself free or maybe push the slime away from you enough to weaken its hold.<p>"
					+ "Suddenly you feel a warm slickness engulfing your cock. You look down and realize there is now a semi-transparent head "
					+ "connected to the blob around your legs right at crotch level. It is slowly moving up your shaft, taking you into its mouth. "
					+ "You can actually partially see yourself being taken in through the top of the slime's head, adding a surreal feeling to the "
					+ "scene playing out in front of you. As you watch, a new appendage with three 'fingers' on the end sprouts from just under the "
					+ "head's chin. These go straight to your balls and begin to fondle them as the mouth finishes taking you inside. You let out a "
					+ "groan at the dual sensations as you begin to come to grips with the fact that you are likely not going to get out of this "
					+ "battle victorious.<p>"
					+ "The head moves back and forth, expertly sucking on your cock while the hand plays with your"
					+ "balls. Your breathing increases as you feel your excitement rising. You begin to struggle harder, "
					+ "trying to pull away and get your cock out of the sticky slime, but succeed in only helping the slime "
					+ "as your movements cause the pleasure to build up faster. You stop and try to focus on something else, "
					+ "hoping to slow down how quickly you are approaching your edge and buy yourself time to come up with a "
					+ "desperate plan to get free. However, the slime has other ideas as it expands itself upwards until it "
					+ "has wrapped around your ass. It begins to pull you forward in time with the head's back and forth motion, "
					+ "forcing you to fuck its mouth whether you like it or not.<p>"
					+ "Just as you are reaching the point of no return you hear a sharp whistle. You open your eyes "
					+ "and realize that Mara made the sound and is now walking towards you. The slime shrinks back "
					+ "downwards, releasing your throbbing, and now slimy, member just as Mara closes the distance between the "
					+ "two of you. As soon as she reaches you, she immediately grabs on to your erection and starts stroking.<p>"
					+ "<i>\"It wouldn't be right for my slime to claim this win for me,\"</i> she says as she picks up the pace. <i>\"I "
					+ "want it to be clear that I am the victor here.\"</i> She pumps faster and faster as she pushes you past the point of no return. With a gasp you release, spraying your cum out onto the ground in front of you. She holds you tight through your whole orgasm, until the last drops of cum leave the tip of your dick. Then she lets go and steps back from you, watching you go flaccid.<p>"
					+ "<i>\"Darn,\"</i> she says, <i>\"Now you're spent and I'm left horny.\"</i> She turns to you with a devilish smile "
					+ "and takes a few steps back. She stands in front of you, well out of reach of where you are stuck to the ground, turns around and bends over until her hands are flat on the ground. You are presented with a great view of her ass and pussy. She swivels her head to face you as best as she can and whistles again.<p>"
					+ "A piece of the slime on the edge of the blob that has you stuck moves up and before you can register what is happening climbs up her legs and covers her pussy. She lets out a moan and you realize that it is pushing its way inside of her. As you watch the slime begins to move in and out of her, fucking her right in front of you. All you can do is watch and listen to her moan and groan. The slime isn't done yet though, you watch it move upward covering the rest of her ass and lower back. She lets out a gasp and you can only guess that another appendage is now pushing its way inside of her ass. The slime is now double penetrating Mara as you stand there and watch. From your angle you can just see her small left breast swaying slightly as the slime pounds her harder and harder. Her right hand has disappeared up by her chest but you can assume that she is playing with her nipple while the left continues to prop her "
					+ "up. You wonder how long she spent training the slime to do all of this, and envy her control over it.<p>"
					+ "Finally she lets out a loud series of grunts followed by a long, low wail and begins to pitch "
					+ "forward. The slime shoots out a couple of tentacles to catch her and slowly lowers her to the ground before it begins to melt down. She lays on the ground breathing heavily while the slime releases you and becomes just a tiny puddle on the ground. Finally she seems to regain her senses and stands up.<p>"
					+ "<i>\"Well that was fun,\"</i> she says with a smile. <i>\"Not quite as good as the real thing, but I will keep "
					+ "working on it. Maybe next time you can test it and let me know.\"</i> She blows you a kiss and walks off.<p>";
		}
		if(flag==Result.intercourse){
			if(character.has(Toy.ShockGlove)&&Global.random(2)==0){
				return "You've got Mara just where you want her. Your arms are wrapped around her, holding her in place as you thrust your cock into her tight pussy over and over. Her moans are getting louder and louder, and you can feel her breath "
						+ "quickening. You're getting close to cumming, but she's definitely closer. She returns your embrace, squeezing her body against yours, stroking your back with her hands. Her hands creep down to grasp your buttocks. "
						+ "All of a sudden, she grins deviously, and she whispers...<p>"
						+ "<i>\"Time for an experiment. Surprise!\"</i><br>"
						+ "Suddenly, you feel a poking sensation in your ass. You feel the pressure of her fingers touching your prostate. Wait... is that the hand that she's wearing her shock glove on...?<p>"
						+ "<b>BZZT!</b> A sharp jolt of pain tears through you as Mara forces electricity through her shock glove and into your ass. You thrash around desperately, but somehow the lithe girl is "
						+ "able to keep her finger pressed against your prostate. You feel an intense pressure welling in your abdomen.<p>"
						+ "Your orgasm hits you like a brick wall. The pain in your rear gives way to pleasure as the pressure in your abdomen releases. Your cock twitches over and over, and you can feel your seed filling up Mara's insides.<p>"
						+ "When your orgasm finally subsides, Mara stands. Thick globs of white cum drip out of her. <i>\"Wow, you came a LOT!\"</i> she remarks happily. <i>\"Just like my research indicated.\"</i> She reaches for her soaked flower. "
						+ "<i>\"Now, I can't go into my next fight this horny. You just hold that sexy, defeated pose. I'll handle myself.\"</i> Exhausted, you can do little more than lie there as Mara masturbates over you.<p>"
						+ "After a few moments of pleasuring herself, Mara suddenly has a revelation. She spreads her pussy lips open and brings her dangerous, gloved hand near her exposed clit. She takes a deep breathe to bolster "
						+ "her courage and giggles nervously. <i>\"This is probably either the best or worst idea I've even had. It looked like it felt great on your prostate... What's good for the goose, right..?\"</i> Before you can respond, "
						+ "she touches an electrified finger to her sensitive love bud. Her whole body goes rigid and she lets out a scream of... probably pleasure?... as she shudders in orgasm.<p>"
						+ "Finally, it's over. The reckless minx collapses next to you, panting. She rolls over and rests her head on your shoulder, then says, <i>\"That felt scary good, but I bet I don't need to tell you that... "
						+ "I'm just worried I might get addicted to that kind of stimulation.\"</i> "
						+ "She pecks your lips with a light kiss, then stands. <i>\"Come visit me every once in a while, okay? I'm working on some new tools that need... testing.\"</i>";
			}else{
			return "Mara's pussy is so tight and wet. She skillfully rides your dick, overwhelming you with pleasure. <i>\"Are you going to cum before me?\"</i> She's panting with pleasure, "+
					"but still sounds confident. <i>\"Go ahead and fill me up. I don't mind.\"</i> Her permission is irrelevant. She's making you cum and there's nothing you can do about it. "+
					"You throw your head back and moan as you shoot your load into her tight womb.<p>"
					+ "Mara slides off your cock as your seed slowly leaks out of her. <i>\"Was I too good "+
					"for you to hold back? You quite a mess down here.\"</i> She stirs her entrance with her finger, making a wet sound. She still looks pretty horny and you recall that "+
					"she hasn't climaxed yet. She smiles and gives you a quick kiss. <i>\"Don't worry, I'll give myself a hand.\"</i> She inserts a second finger, using your ejaculate as a "+
					"lubricant. <i>\"Playing with semen would probably be a little gross to you, right? It's actually turning me on.\"</i> She lets out a quiet moan and gives you a needy "+
					"look. <i>\"Just don't leave, ok? I'd feel lonely masturbating on my own.\"</i> You hug Mara's petite body, feeling her tremble while she continues to play with herself. "+
					"You kiss her neck and stroke her body, which seems to heighten her pleasure. You judge she is on the verge of orgasm and kiss her passionately, while playing with "+
					"her nipples. She moans against your mouth and shudders in your arms as she climaxes.";
			}
		}
		if(c.lastact(character).toString().equalsIgnoreCase("Footjob")){
		    return "You are exhausted and Mara has managed to get you down on the ground.  As you lay there you aren't sure how you are going to manage to get up and back into the fight.  While you lay there panting, Mara comes over and you feel her foot trace along the length of your hard, bare dick.  <p>" +
                    "<i>\"Come on, is that all you've got?\"</i> she taunts as she continues to tease your length.  You lift your head enough to look at her face as she smiles down at you.  Her toes begin to toy with your balls gently as she watches you.  You begin to struggle up, making it into a sitting position beginning to push her foot away from you once you are able to reach her.  She pushes against your hand, trying to get her foot back to your crotch but you continue to fight against her attempts.  Suddenly her eyes light up as if something just dawned on her.<p>" +
                    "<i>\"Oh!\"</i> she exclaims, <i>\"Is this getting to you?\"</i>  As she says this she moves her foot back toward you, forcing you to push it away again.  <i>\"It is, isn't it?\"</i>  You move your hand to cup your balls and pull your erection back against your body so you can cover it with your other hand.  Mara takes a few steps back, and then quickly runs around you.  In your exhausted state you aren't able to follow her.  By the time you start to turn around to look behind you, you feel a couple of metal appendages grab you by the elbows and pull your arms behind your back.  You manage to turn your head and out of the corner of your eye you see that one of Mara's devices has trapped you in its grip.  You try to fight back but even as you struggle you feel a couple of metal rings close around your wrists and your hands are successfully restrained behind you.<p>" +
                    "You rock and struggle as you sit there on the ground.  Mara walks back around you and watches out of amusement as you try to get free of her toy.  She watches until she can see that you have come to the realization that you aren't going anywhere, although you aren't sure if it is to mock you or because she doesn't completely trust you won't get free.  Finally she walks toward you again and immediately her foot finds its way between your legs.  She goes back to gently fondling your balls with her toes while you squirm.  <p>" +
                    "After a couple of minutes of her alternating playing with your balls and running her foot up and down your length, your cock has begun to drool precum but not much else has changed.  You can see the frustration forming on Mara's face as she becomes impatient with the fact that she hasn't been able to make you cum yet and end the match.  On your side though you are thankful for the time to try and think of a way out of this predicament.  Mara finally decides to change things up and sits down in front of you.  You immediately try to move your legs so that you can start to return the favor with your feet, but she grabs a hold of your ankles and holds you at bay while she positions her feet between your legs and back on either side of your crotch.  <p>" +
                    "Once she is in position, she moves one foot up and pushes your erection against your stomach.  As she runs her foot up and down your length, she places her other foot next to your balls and begins to wiggle her toes, tickling them.  You let out a groan as the increased attention is also starting to increase your arousal and decrease your control.  You begin breathing harder as she continues her onslaught, clearly amused that she has brought you so far with just her feet.  You struggle, trying just to get your most sensitive parts away from her teasing feet.  You start to make some progress when she picks up the foot that is tickling your balls and brings it down on top of your scrotum.  She slowly begins to push downward, trapping one of your testicles against the ground and essentially stepping on it.  You yelp in pain and immediately stop struggling.<p>" +
                    "<i>\"Good boy,\"</i> she said, <i>\"Now let me get you off so we can finally move on.\"</i>  She let up on your testicle but lets her foot continue to hover over it threateningly.  Her other foot goes back to stroking your hard cock aggressively.  You try to relax and hold back, but you know at this point it is hopeless and in no time you are moaning as she picks up the pace.  She keeps stroking faster and faster as you begin to throb.  Suddenly you let out a grunt and you begin to cum, splattering your stomach and chest with your own cum as Mara holds your dick against your body.  She smiles, enjoying watching you cover yourself with your own cum.<p>" +
                    "Finally your orgasm subsides and you slump down as you try to catch your breath.  Mara wipes her foot along your thigh, leaving a sticky trail along the way.  She stands up and walks behind you.  Your elbows and wrists suddenly are free to move as Mara releases you from her restraining device.  You watch her off to the side as she picks up both her clothes and yours from the ground and quickly walks off, leaving you there both sticky and naked to plot how you are going to get her back<p>";
        }
		if(character.has(Trait.madscientist)&&character.has(Flask.Lubricant)&&Global.random(2)==0){
			opponent.add(new Oiled(opponent));
			return "You've fallen completely into Mara's hands now. Her nimble fingers dance over your dick and balls, playing you like an instrument. You grit your teeth and "+
				"try to endure her touch until your can finger her to orgasm. It's a lost cause though, groan as you inevitably feel your pleasure building to a peak. Just before "+
				"you hit the point of no return, her wonderful fingers release you. Mara grins impishly as your dick twitches in frustration at being left on edge. As soon as you've "+
				"let your guard down, she kisses you forcefully and pumps your cock rapidly. Your orgasm rocks you as she milks as much of your cum as she can get.<p>"
				+ "You slump to the "+
				"floor, spent, but Mara isn't finished with you. She pulls out a bottle of lubricant and starts to grease you up. She takes her time with it, teasing and tickling you "+
				"as she goes, stopping from time to time to place light kisses. Between her enticing behavior and her naked body pressed against you, your erection recovers in record "+
				"time.<p>"
				+ "Mara makes herself comfortable sitting on your lap and slides your lubed up dick between her thighs. As she leans against your chest, you can feel her hot slit "+
				"pressing against your member. Her finger teases the head of your penis, which is poking out of her lap. <i>\"It's a good thing you're such a horny boy. If you couldn't get it "+
				"up again, I would have to settle for grinding on your leg.\"</i> As she says this, she starts to rub her clit along the length of your penis. She keeps her legs clamped "+
				"tightly together so that her movements stimulate your entire shaft. You contribute by licking and sucking the side of her neck to draw out soft moans of pleasure. You "+
				"support your upper body with your left arm, which leaves your right hand free to play with Mara's small breasts and nipples. Her grinding becomes more needy as she "+
				"quickly approaches her climax and you can feel that your ejaculation is not far off. You recently came once however, and you're still not ready to cum when Mara starts "+
				"trembling and gasping in orgasm. You kiss her cheek and embrace her with your free arm until you feel she slumps limply against you.<p>"
				+ "Mara turns her head to meet your "+
				"eyes and smiles gently. <i>\"Sorry, I came first. But you came alone earlier, so maybe we're even.\"</i> You groan softly as her legs squeeze your painfully hard dick. Her "+
				"smile turns a bit more playful. <i>\"Are you worried I'm going to leave you with blue balls? I wouldn't do something that cruel.\"</i> She bats her eyes at you cutely and "+
				"wets her lips with her tongue. <i>\"Kiss me?\"</i> You press your lips against hers and she immediately starts rubbing your lubricated glans with her palm. Your hips buck "+
				"involuntarily, but she manages to maintain both the kiss and her grip on your cock. The intense stimulation blows away your endurance and your head goes blank as you "+
				"cover her hands with your seed. Mara breaks the kiss and leaves you completely exhausted.";
		}
		else{
			opponent.arousal.set(opponent.arousal.max()/3);
			return "You're completely at Mara's mercy, but she refuses to finish you off. She teases and caresses you, keeping you too on-edge to fight back, but avoids your " +
					"painfully hard cock. She brings her face close to yours with a cat-like grin and whispers, <i>\"You look so desperate. Tell me you surrender and I'll " +
					"let you cum.\"</i> You've already lost. There's no point resisting. You practically beg Mara to finish you, causing her amusement to increase tenfold. " +
					"She straddles your waist and rubs her wet pussy back and forth along the length of you dick, quickly bringing you to an explosive and messy climax. <p>She wets her fingers with your spunk " +
					"and plays with her pussy and clit in front of you. Dominating and teasing you obviously got her worked up. As she begins to moan and shiver on top of you, her free " +
					"hand teases and toys with your spent dick. By the time Mara reaches her own orgasm, she's worked you into a fresh erection. She plants a light kiss on the tip of your " +
					"dick and leaves you naked and frustrated.";
		}
	}

	@Override
	public String defeat(Combat c,Result flag){
		Character opponent=c.getOther(character);
		declareGrudge(opponent,c);
		if(c.stance.sub(character) && (c.stance.enumerate()==Stance.pin || c.stance.enumerate()==Stance.reversepin)){
			return "It doesn't take long before it becomes obvious that Mara doesn't have the strength left to break out of your pin. With your hands holding down both of her wrists and her thighs pinched by your legs, the only way she can possibly fight back against you is to try and kiss your face and neck. Although you're pretty aroused right now, you're not close enough to the edge that a kiss could push you over it, so there's pretty much no way you could lose the fight right now.<p>"
					+ "On the other hand, it's hard to say exactly how you can go about winning this fight. Since you're in the dominant position, you have just enough flexibility that you can bring your head down to Mara's breasts. You've been making good use of this fact, sucking on each breast in turn, licking her nipples, and even occasionally giving one of them a careful bite. This is obviously turning Mara on and has brought her to the edge of orgasm, but it hasn't quite managed to push her over the edge yet. You'll need to get at her pussy if you want to win, but there's no safe way to do that without giving Mara a chance to escape your pin.<p>"
					+ "Despite how aroused she is right now, Mara seems to have picked up on this fact as well. <i>\"Hey "+opponent.name()+"... What do you say we call this a draw?\"</i> she says.<p>"
					+ "You pull back from her breasts and look up at Mara's face, seeing a pleading expression in her eyes. As much as that expression makes you want to give in to her, you still haven't given up on winning this match. Instead, you offer her a alternative deal: She surrenders, and you let her cum rather than endlessly torturing her breasts with pleasure that never quite gets her over the edge.<p>"
					+ "Mara lets out a whimper at this, but she doesn't respond either way. Since that's as good as a refusal, you go back to working on her breasts, hoping that you can get her to surrender before someone else stumbles upon the two of you.<p>"
					+ "<i>\"Ah! Mm... wait... no...\"</i> Mara says through her moans. Her arms push against yours as she tries to free herself, but they don't have nearly enough strength left to escape. She tries to pull her legs out from yours as well, but they're pinched too tightly. Finally, she tries to pull up her hips, probably intending to rub her slit against your balls and see if she can get you off that way. She must be desperate to try that, since it's much more likely that would cause her to climax first.<p>"
					+ "Mara's body falls limp beneath you, with her breathing and moaning being the only actions left that she can manage. This encourages you to pick up the pace. You suck hard on her breast, rapidly kiss around it, and then circle her nipple with your tongue, flick across it back and forth, and finally suck in hard on her nipple. All through this, Mara's moans get louder and louder.<p>"
					+ "<i>\"No more! Please! I... I surr... I surr...\"</i> Mara seems to be having trouble getting the word \"surrender\" out, but that's her problem, not yours. And without saying it in full, she's still in the match. Particularly with Mara, you can never quite write off the possibility that it's part of a trick to get you to let down your guard.<p>"
					+ "You take just a brief break from Mara's breast to let her know that you can't trust her without hearing <i>\"I surrender\"</i> in full, which gets you a groan in response. It seems your suspicions were actually correct; she was planning something after all. And for that, she'll need to be punished. You bring your mouth down to Mara's other breast and bite down on her nipple as punishment for this deceit.<p>"
					+ "<i>\"Ah! Fine, you win! I... I... I... ahhhhhhhh!\"</i> Just as it seems Mara is about to actually surrender, her body jerks beneath you. Her muscles spasm in waves as her orgasm courses through her body, but you manage to hold her firmly within your pin throughout it all, until at last she's completely spent. <i>\"I can't believe I... without you even touching my...\"</i> Mara mutters, more than a small bit dazed from the experience.<p>"
					+ "But she did. Unfortunately, she had to go about losing the hard way, so you aren't exactly eager to take mercy on her right now. You know that she normally gets quite sensitive after an orgasm, but this one was prompted without even touching her pussy, so there's a good chance she can still handle some stimulation there.<p>"
					+ "<i>\"Ah! Wait, I...\"</i> Mara begins to say, but as you catch her gaze, she swallows her protests. <i>\"Just... be gentle. And stop if I say so.\"</i><p>"
					+ "That's a compromise you can work with. You finally let Mara go, giving her a brief moment to stretch out her muscles as you shift your position and line your cock up with her entrance. You rub the tip against her slit, looking up at Mara's face to make sure it isn't too much for her.<p>"
					+ "Mara moans slightly, but she nods at you, prompting you to slowly push yourself inside of her. <i>\"Oh fuck...\"</i> Mara says, throwing her head back. <i>\"Please, just... very, very slowly... I think I can just barely take it.\"</i><p>"
					+ "It's hard to restrain yourself after holding your orgasm back this long, but the need to keep from hurting Mara is enough motivation to keep your movements slow. You gently, gradually push your dick inside of Mara, asking her how it feels as you do so.<p>"
					+ "<i>\"Mm... Good. Just keep like that and I'll be fine,\"</i> she says, smiling at you. You have to agree that it's good. Not only is her pussy slick with her juices, it's just as warm and soft as ever.<p>"
					+ "Smiling back at Mara, you pull your dick slowly back out of her, and then push back in, only a bit more quickly this time. You maintain eye contact with her throughout, trying to go just as fast as she can take it. After a few strokes, you're able to speed up the pace just a bit, but it's obvious from Mara's expression that she's at her limit. Another orgasm is rapidly approaching for her just as quickly as you're reaching your own limit.<p>"
					+ "You slow down for just a moment, giving Mara a bit of a break. The slow pace of this has put you in a bit of a romantic mood, so just as you see her expression relax, you lean in to plant a soft kiss on her lips. Mara hums slightly as she returns the kiss. You give yourself a minute to simply enjoy kissing her, only moving your cock just enough so that she and you can feel it still inside of her.<p>"
					+ "Deciding that the time has come at last, you pick up the pace of your thrusting to just as much as you think Mara can take, never breaking the kiss all the while. Through a series of moans and grunts into the kiss, the two of you quickly reach orgasm and cum simultaneously.<p>"
					+ "Even after your orgasm subsides, you find it hard to move away from Mara. You've been lying on top of her for so long now that you've gotten used it, and she seems to feel the same way, judging by the fact that she's made no motion to leave either. Nevertheless, the games are still ongoing, so you regretfully pull yourself out of her and stand up. Mara still seems to be a bit out of it, so you leave her to rest for a bit as you collect your clothes.";
		}
		if(c.stance.anal() && c.stance.sub(character)){
			return "Mara's orgasm catches you by surprise. It seems to catch her by surprise, for that matter. Mara had just barely finished pleading with you to take your cock out of her ass and just finish her off normally, and you'd just finished agreeing to meet her halfway. You told her you'd keep fucking her ass, but you'd do her the favor of reaching around to give her clit a good rub to finish her off.<p>" +
					"Things didn't work out quite as you planned though. As soon as you made this promise, Mara's body had slumped a bit, accepting the fact that, one way or another, she was going to cum with your dick in her ass. And then she did, without you even needing to give her clit a little extra stimulation to finish her off. Mara's body froze at first, and shudders took over her body all at once. She opened her mouth to let out a strangled scream, which didn't stop until her body collapsed to the ground beneath you.<p>" +
					"You let Mara fall forward, your dick slipping out of your ass as she pulls away. There's no sense tormenting her anymore now that the match is over. Instead, you lean down beside her, embracing her and gently stroking her back and side as a wordless apology for the way you defeated her.<p>" +
					"<i>\"It doesn't make any sense...\"</i> Mara mumbles. You raise an eyebrow at this and wait for her to continue. After a moment, she shakes her head and turns to look at you. The expression on her face seems to be somewhere between confusion and embarrassment. <i>\"I'm not a guy, so I don't have a prostate to pleasure through my ass. There's no evolutionary reason for anal sex to feel good, and besides that, it hurts - well, at least it did when you started. So how the hell were you able to make me cum from my ass alone?\"</i><p>" +
					"She doesn't seem to be asking this question rhetorically, so you try to think of a good answer. The human body can be weird. It can feel pleasure from weird things, and sometimes it even gets the signals for pleasure and pain mixed up. A lot of sex is in the brain too, so maybe Mara got a kick out it for some reason beyond the physical sensations alone. Maybe because it felt forbidden and kinky, maybe because she liked how it made her feel completely dominated by you. Or maybe she's just a butt slut, as simple as that.<p>" +
					"Mara scrunches her face in a rather adorable way, and she lets out a groan. <i>\"Too many possibilities...\"</i> she says. <i>\"We'd need to do a controlled experiment to figure it out for sure.\"</i> Mara is silent for another moment, and then she makes eye contact with you. <i>\"Alright. Here's what we're going to do. You still need to cum, right? So apply some more lube and get back in there. I don't know if my ass will be sensitive after an orgasm like my pussy is, so stop immediately if I say so. But if it's good, keep going - just no trying to dominate me. We'll change that one variable, and see if I cum again. Got it?\"</i><p>" +
					"Well, you certainly can't refuse a deal like that. Even if you don't manage to make Mara orgasm again, you'll still get to cum in her ass. You smile at her, then lean in to place a quick kiss on her lips before you move away. You find some more lube and apply it to your cock while Mara pushes herself back up to her knees. After your cock is lubed up enough, you bring the lube to Mara's ass as well. She lets out an initial squeak as she feels the cold lube against the rim of her asshole, but she slowly relaxes as you circle your finger around her rosebud. Once she's relaxed enough, you push your finger inside. You pull it back out, apply some more lube to it and another finger, and return to give her asshole an extra dose. It might take some time to make her cum again, so you'll need to be generous with the lube so you don't end up hurting her.<p>" +
					"<i>\"Hmm...\"</i> Mara says. <i>\"Okay, datapoint #1: My ass isn't too sensitive. Datapoint #2: That actually feels good.\"</i> It does, does it? You were just about to remove your fingers, but you decide to give her a bit more time with them if she's enjoying this. You twirl your fingers around inside Mara's ass for a bit, then pump them in and out for a bit longer. Eventually, Mara lets out a giggle. <i>\"Okay okay, I like that, but we've still got an experiment to do here, so put your cock in me already.\"</i><p>" +
					"You mouth splits into a grin. You quickly twist your fingers around inside Mara's ass one more time, then pull them out. Playing with Mara's ass has kept your cock rock-hard, so you're ready for the main experiment now. You guide it up to Mara's ass and press the head against her opening. Before pushing forward, you ask her to let you know if it hurts.<p>" +
					"Mara shakes her head. <i>\"It might hurt, but we're still doing this. Just stop if I say so.\"</i><p>" +
					"You agree to this, and then slowly lean forward. Your cock presses in against Mara's entrance, which just isn't quite wide enough for it to slip inside. But as you push harder, it eventually begins to give way and open up. You can hear a bit of a whimper from Mara, but she doesn't tell you to stop. Her sphincter slowly widens to accommodate the head of your cock, and Mara's breathing steadily gets heavier. At last, you're able to push the head inside her completely. You let yourself relax a bit as you see her ass wrap around the head of your cock, and you let Mara know that it's not going to get any wider than this.<p>" +
					"<i>\"Haa... okay, good,\"</i> Mara says. She groans slightly, then says, <i>\"Datapoint #3: That hurts. I don't think I like the pain, but it's fading now, and I think I can get used to it.\"</i><p>" +
					"That's pretty good news, overall. Now, time for the next part of the experiment. You place your hand on Mara's hip, holding onto her as you lean forward once more. Her ass still fights you a bit, but it doesn't need to widen anymore to let your cock slide the rest of the way inside her.<p>" +
					"<i>\"Oh fuck...\"</i> Mara says. <i>\"That's even better than your fingers. Why didn't you fuck my ass this gently during the match? I would have cum in half the time!\"</i><p>" +
					"You chuckle at this, reminding Mara that she was resisting quite a bit more then, and you didn't exactly have the option of being gentle with her.<p>" +
					"<i>\"Bah... okay, fair point,\"</i> Mara says. She turns her head to look back at you, and she wiggles her ass from side to side a bit. <i>\"Okay, final phase: Let's see if you can make me cum again.\"</i><p>" +
					"You make eye contact with Mara and give her a smile. Slowly, you begin to pull your cock out of her ass, and then push it back in. Without the worry of possibly losing the match, you're able to take your time now as you begin to gently fuck Mara's ass.<p>" +
					"<i>\"Mmm...\"</i> Mara says after a minute of this treatment. Her eyes sparkle a bit as she looks at you. <i>\"That's good, but I can take harder. Come on, "+opponent.name()+", fuck my ass properly.\"</i><p>" +
					"You grin at Mara, and immediately shove your cock hard into her ass, plunging it as deep as it can go.<p>" +
					"<i>\"Oh fuck, just like that!\"</i> Mara says, and you're happy to oblige. You pull back and then pound into her again, and again. <i>\"Fuck... yes... fuck fuck fucking yes...\"</i> Mara says. Her words soon turn into mumbles, and her eyes roll back into her head as pleasure overtakes her.<p>" +
					"As you see this reaction from Mara, you realize that you don't have long yourself before you reach your own peak. You're going to have to finish Mara off quickly, lest your cock end up too soft after your own orgasm to finish her off through anal alone. But you can't touch her anywhere else, or that'll ruin her experiment. All you can do is desperately try to distract yourself, think of something, anything other than your cock in Mara's ass.<p>" +
					"It's too late. That one stray thought does it for you. As you slam your cock as deep as possible into Mara's asshole, an orgasm breaks free from your body. Your balls empty themselves, spraying your cum deep into Mara's anus, and you mentally curse yourself for not being able to last longer. You really should be better at holding off than this.<p>" +
					"<i>\"Yes, yeeeesssssss!\"</i> Mara calls out, her body tensing up. As you look down at her in surprise, you see her body going through the same display as before: freezing up, breaking out into shudders and a strangled scream, and then finally collapsing to the ground.<p>" +
					"Chuckling, you lie down beside Mara once more, and ask her if that makes the experiment a success.<p>" +
					"Mara turns her head toward you, giving you a bit grin. She hums for a moment, then leans in to plant a quick kiss on your lips. After she pulls back, she says, <i>\"Yep. We'll need to try this some more to isolate all the variables properly though. I'm probably at least a little bit of a butt slut, but some more experiments will tell us for sure.\"</i><p>" +
					"You can work with that. You lean in to plant another kiss on Mara's lips, and then reluctantly push yourself back to your feet. You're out of time to experiment now, and have to get back to the games, but you'll be sure to find time during the day to help Mara out. And maybe even during the Games again too.<p>";
		}
		if(flag==Result.intercourse){
			return "You bury yourself deep into Mara's tight pussy as she screams in pleasure. Her hot folds shudder and squeeze your cock, confirming she's reached her climax. " +
					"The sensation is amazing, but you're not in danger of cumming with her. You gently stroke her head while spasms of pleasure continue to run through her small body. " +
					"It occurs to you -not for the first time- that she's really cute, even when she's not trying to be.<p>As Mara catches her breath, you see realization slowly dawn " +
					"on her. <i>\"You didn't cum? Why not?\"</i> She actually looks a little hurt. <i>\"Every boy I've been with said it feels really good and tight inside me. They never outlast " +
					"me.\"</i> Every boy she's been with? Mara struck you as a bit of an introvert. How many guys has she been with? <br>She gives you a flick on the forehead. <i>\"Don't be mean. " +
					"I've only slept with a few boys. It's not like you're a virgin either.\"</i> Fair enough, but if she's upset that you didn't cum inside her, you're eager to remedy that.<p>" +
					"You pull most of the way out in preparation for a big thrust, but Mara yelps in alarm. <i>\"Wait!\"</i> She slides her butt backward, causing your dick to fall out completely, " +
					"and curls up protectively. <i>\"I get really sensitive down there after I orgasm. Give me a minute or two to recover before we continue. In the meantime we can always chat, " +
					"or maybe kiss?\"</i> You'd feel pretty silly trying to have a conversation while kneeling over her with a painfully hard boner. You lean down and press your lips against " +
					"hers and she makes a soft noise of approval. <p>After several minutes of kissing and caressing her tiny breasts, Mara looks euphoric, but it's certainly done nothing to " +
					"quench your desire. Hasn't she recovered yet? <i>\"Maybe some more of this first...\"</i> She whispers happily. You've been on edge so long that pre-cum is dripping from your " +
					"cock. You're pretty sure she's teasing you or worse, stalling until someone else interrupts and finishes you off. Mara giggles and prods your penis with her feet. " +
					"<i>\"If you really can't endure any more, I'm sure I can find a few ways to get you off, but think about how much happier we'll both be if you wait until you can cum " +
					"inside.\"</i> <p>She's right about everything except the waiting. You kiss her fiercely, which catches her off guard long enough for you to easily thrust into her soaked " +
					"entrance. She coos softly against your lips. She's clearly not in any discomfort from the sudden penetration. Right now you're too horny to be annoyed at her deception. " +
					"Her pussy is tight, wet, and feels heavenly. You pound your hips against hers for your mutual pleasure. Mara lets out breathy moans in time to your thrusts and when " +
					"you finally reach your sweet release, she tenses in orgasm under you.<p>She softly strokes your cheek as you both relish the afterglow. <i>\"You were just like a wild " +
					"animal there,\"</i> She whispers with a grin. <i>\"You never would have been that intense if I hadn't made you wait.\"</i>";
		}
		if(c.lastact(opponent).toString().equalsIgnoreCase("Tickle")){
		    return "Mara's analytical mind, quick reflects, and mischievous personality makes her both an amazing lover and a terrifying combatant. Any night she's involved, you bet she's already had several  plans in mind to try and secure the win right under everyone's noses.<p>" +
                    "You'd bet all your earnings from this match that Mara doesn't have a plan to get out of this.<p>" +
                    "<i>\"Wait!\"</i> She screams as you push your hands deeper into her skin, \"You don't have to do this!\"<p>" +
                    "Have to? No. Want to? Oh hell yes.<p>" +
                    "<i>\"Noooo!\"</i> Mara's screams quickly turn into laughter as you continue tickling her. She thought she was fast enough to outmaneuver you, flexible enough to escape your pin, but all it took was one misstep to find herself trapped under your unrelenting grip. Any attempt to remove your hands meant nothing as she couldn't muster any strength. However, she still tries and because she does she leaves your real targets unprotected. You quickly reach towards her breasts and unleash your full tickling powers.<p>" +
                    "<i>\"I-I'll get you for this!\"</i> She weakly threatens as she does her best to avoid letting her voice out. Having been already on edge from your fight, it isn't long before she lets out a long suppressed scream, her hips bucking onto you as she loses strength in her body.<p>" +
                    "She's on the ground now with you standing tall on top of her, fingers still prime and ready. Mara tries crawling backwards, eyes looking upwards in fear as she realizes what's about to happen.<p>" +
                    "<i>\"Truce?\"</i> <p>" +
                    "After how much she teases you on a daily basis? Your hands are on her legs before she can yell out again, already tickling her from the base of her feet to the back of her knees. <p>" +
                    "Mara begins to turn red with both arousal and laughter after these sensations ravage her post-orgasm state, causing her to giggle endlessly as she becomes light headed.<p>" +
                    "You put the pressure on, moving up to kiss and tickle her inner thighs. Her giggles start mixing in with heavy moans. Seeing as she's left her legs open, you go for the finishing blow. You lick at her pussy, nibbling her nub as you lightly lick around her mound. Her back arches once more and she yells out in full as love juices escape and hit your mouth. Her legs wrap around your head and she desperately pushes your head down with her trembling hands as she finishes.<p>" +
                    "When her orgasm fades, you break free with a raging erection. Mara notices and slowly turns around, presenting herself for you.<p>" +
                    "<i>\"Come on "+opponent.name()+". You can't work me up like that and then leave yourself unsatisfied. You've got to take your prize.\"</i><p>" +
                    "Your dick was out and ready before she finished talking. Her insides greet you hotly as you enter into her with little resistance. Two consecutive orgasms leave her weak to your advances but you wonder if you could pull a third. As you begin thrusting, the grip you have on her body loosens as you position your hands.<p>" +
                    "<i>\"Don't you dare-\"</i> Mara's too late as once more her laughter fills the room. Her pussy suddenly constricts around your dick like a vice only to let go just as quickly. The sensation of her tunnel probing and squirming matches her tunnel which serves to quickly bring you to the edge.<p>" +
                    "If it was bad for you, it's worse for Mara. You never stopped tickling, focusing on her nipples in an effort to make her cum. She's very aware of just how hard and thick your shaft is inside of her and with every breath that escapes her causing her pussy to constantly tighten and release, it must feel like some crazy new sensation.<p>" +
                    "It's all too much. You both cum, causing Mara to blank out for a second as she hits an intense orgasm. When she comes too, you had already put your clothes on. Mara's content to lay there, too tired to move.<p>" +
                    "<i>\"I think you just opened up a new kink "+opponent.name()+". Oh you're gonna regret the day you open up this box.\"</i><p>" +
                    "You don't doubt it. You give her a quick kiss before leaving her to recover.<p>";
        }
		if(opponent.getArousal().percent()<30){
			return "Mara has fought hard tonight, but you have been especially focused and have outplayed her at "
					+ "every turn. You have managed to find every opening in her defense and managed to block her every "
					+ "offensive attempt. You managed to catch her off guard early on and strip her completely, and every "
					+ "attack has raised her arousal since then. She is completely desperate and unfocused now, meanwhile "
					+ "her inability to land many attacks has left you relatively calm and unaroused.<p>"
					+ "You lunge at Mara, going for her breasts. She sees you coming and tries to deploy one of her "
					+ "mechanical defenses, but it doesn't come out fast enough. You reach her and begin to roughly massage "
					+ "her small tits. After a minute you switch tactics, letting one hand pinch her nipple, eliciting moans from "
					+ "her. You let the other hand drift down between her legs and can feel how wet she has gotten. At this "
					+ "point in a battle with Mara you would normally be feeling a need to get this over with before you are in "
					+ "danger of cumming yourself, but you aren't close at all yet. In fact you are only semi-hard at this point.<p>"
					+ "You decide to take advantage of this unusual circumstance and have some fun with her. "
					+ "You pull her hands behind her back and trap them with a zip tie. She struggles hopelessly "
					+ "against her bounds, obviously unhappy that you have gotten one up on her. You give her a minute to "
					+ "struggle, then when it is clear she understands she is trapped you push her against the wall and hold her "
					+ "there. You run your hands you run up and down her chest and stomach, each pass getting closer and "
					+ "closer to her pussy. She begins to squirm as you tease her, renewing her attempts to get away from "
					+ "you.<p>"
					+ "<i>\"God damn it,\"</i> she yells, <i>\"Let me go and fight me fairly.\"</i><p>"
					+ "Finally you let your hand fall below her crotch to run over her thighs. She lets out a groan as you "
					+ "continue to ignore her protests. After a few more passes you decide you should get this over with and "
					+ "chalk up this win, before something intervenes and turns the tables on this easy win. <p>"
					+ "You drop to your knees in front of her and immediately push your tongue against her clit. She lets out "
					+ "a grunt of surprise followed by a series of moans as you tease her with your tongue. You slowly flick her "
					+ "clit with your tongue as you reach up and shove two fingers inside her dripping slit. With how turned on "
					+ "she already is, your fingers easily slide into her and after a couple passes you add a third finger. "
					+ "You let your tongue attack her clit more roughly as you finger fuck her faster and faster. She is quickly "
					+ "grunting and breathing hard as you attack her pussy, and you know it won't be much longer before you win "
					+ "this round. You switch what you are doing so that your tongue takes the place of your fingers in her "
					+ "pussy while your thumb finds her clit.<p>"
					+ "Less than a minute later you feel her release, flooding your mouth with her juices. You continue "
					+ "licking her through her orgasm, tongue fucking her the entire time. Your thumb continues to move over "
					+ "her clit in slower and slower circles as you feel her begin to come down. You can tell when she has "
					+ "reached the end, but you aren't ready to end your assault. You don't usually get the upper hand on her, "
					+ "so you want to exploit it for all it is worth. You pick up the pace again as she lets out a scream of "
					+ "combined protest and frustration. She tries as hard as she can to squirm away, but you reach up and grab "
					+ "her ass, pushing her closer against your face as you continue to attack her pussy with your tongue.<p>"
					+ "Suddenly, Mara's knee shoots up and catches you in the chin. You fall back away from her and "
					+ "she slumps against the wall. Once you recover from the shock of the blow, you slowly stand up,"
					+ "watching her lean against the wall with her eyes closed, trying to catch her breath. Finally she "
					+ "opens her eyes and looks at you.<p>"
					+ "<i>\"You know I am sensitive after I cum,\"</i> she spits angrily, <i>\"I can't believe you did that to me.\"</i> You "
					+ "just watch her as she looks you over and you can see her body blushing.<p>"
					+ "<i>\"And just to make this even more embarrassing,\"</i> she says, <i>\"You're not even hard.\"</i> You try to "
					+ "shrug it off as you reach behind her to release her hands. You turn around to leave but as you try to walk "
					+ "away you are stopped by a pair of hands reaching around you. Her small hands grab your crotch and one "
					+ "begins to play with your balls while the other wraps around your cock. She begins stroking you frantically, "
					+ "as she nibbles on your neck. You slowly begin to respond to her attention and she eventually manages to "
					+ "get you to full hardness. However, her desperation is apparent because her strokes are erratic and halfhearted. "
					+ "She is also getting rougher and rougher with your balls, going from massaging to manhandling and her actions "
					+ "become more and more painful.<p>"
					+ "<i>\"Why won't you cum?\"</i> she cries in desperation. Finally it has gotten to be too painful and you "
					+ "push her hands away. You turn around just look at her, shaking your head. She reaches down again and tries to grab your cock again but you push her away again.<p>"
					+ "<i>\"Damn it, fine enjoy your blue balls!\"</i> she yells, <i>\"I can't believe I lost to you. It won't happen "
					+ "again.\"</i> She turns to walk away, obviously annoyed although you can't tell if it is more with herself or with you. As she turns you slap her ass, and she shoots you a glare before she hurries off. You smile to yourself as you walk off in the opposite direction to return to the game.";
		}
		if(character.is(Stsflag.horny)){
			return "Mara collapses to the ground, her face a bright shade of red. She makes no effort to cover herself, fingering her soaked slit in full view of you. <i>\"I can’t take this anymore "+opponent.name()+"! Please help me get off!\"</i> Knowing Mara, you’d normally assume this to be another one of her clever deceptions. But the pleading expression she wears is not fake, you’re certain of it.<p>" +
					"Throwing caution aside, you sit down with your back against the wall. She seems a bit confused, until you pull her back and towards you, ending with her back resting against your chest. You place a kiss on the top of her head, before reaching down to help her.<p>" +
					"Mara gasps as two of your fingers slip into her soaking flower, and begin to slowly slide them in and out. All the while she’s lost in a haze of lust, quietly whimpering and whispering for more. You happily oblige, planting a kiss on her neck before kicking it into high gear. Rapidly thrusting them in and out, you quickly push Mara over the edge. She cries out as the orgasm overtakes her, and you can feel a rush of fluid flow past your fingers. <p>" +
					"Her heavy breaths begin to slow as Mara relaxes against your chest. Once she’s collected herself she turns to face you, a blushing grin adorning her cute face. <i>\"Thanks for helping me out "+opponent.name()+". I really underestimated how much feeling like that would impact me... though, it’s your win either way.\"</i> She reaches down between her legs, giving your cock a quick squeeze with an impish giggle. <i>\"I doubt you’re satisfied with just that though, and I’m certainly not. So why don’t we go for another round now that you’ve gotten me all prepped?\"</i><p>" +
					"You chuckle, telling her that you’d like nothing more. Lining yourself up with her slit, you take hold of her hips and get ready to make a powerful thrust in. That is, until Mara suddenly pushes her hips back down, suddenly enveloping your cock in her tight, wet slit. She giggles, an impish grin on her face. <i>\"Sorry "+opponent.name()+", I couldn't hold back any longer... I’m sure my favorite boy doesn’t mind though, right?~\"</i><p>" +
					"Mara trying to take control is something you expected, but see no reason to stop her. Instead, you opt to slightly change the playing field. By pulling Mara towards you, you’re able to swivel her around without removing yourself from her pussy. <i>\"Hm? What was wrong with the other position?\"</i> You simply say it’s nice to see her cute face and reactions, before leaning in to pepper her neck with kisses. <p>" +
					"This only seems to add fuel to Mara’s lustful fire, not that you mind. She starts to move, slowly rocking her hips back and forth. It soon becomes clear that she’s not holding back as the horny girl quickly shifts to bouncing, the sudden increase in stimulation making you gasp.<p>" +
					"As nice as it is being ridden like this, you don’t intend to let her have all the fun. Taking hold of Mara’s slim hips, you start to quickly thrust into her in time with the bouncing. This earns a little yelp from the unexpecting girl before melting back into pleasured moans.<p>" +
					"<i>\"That’s it, keep thrusting just like that! It feels so good...\"</i> Mara clings tightly to your chest, moving as fast as she can. The sound of sex revertebrates through the area as your hips slap together, and you can quickly feel yourself nearing the edge. Taking one hand off of her hip, you tilt Mara’s face up towards you. Leaning in, you kiss her on the lips. She eagerly accepts, her tongue aggressively forcing its way into your mouth and intertwining with your own. <p>" +
					"Trying to fend of your impending orgasm is useless at this point. You break the kiss and tell Mara that you’re about to burst. <i>\"Do it! Fuck me silly and fill me up!\"</i> You make a quick series of thrusts before burying yourself as deep as you can within her. She squeezes her walls down hard around your member, sending you cascading over the edge. A torrent of cum erupts from your cock into her waiting slit, filling her womb with the thick substance. <p>" +
					"<i>\"Fuck, yes!\"</i> Mara’s pussy spasms and contracts around your orgasming member, milking it for all it’s worth as she reaches her own height. After what seems like an eternity, the two of you begin to come down from the pleasure high. You can hear Mara’s shallow breathing and feel her heat against your chest, alongside your own heavy breaths. <p>" +
					"After holding her for a few moments, Mara looks up at you with a wide smile. <i>\"That was awesome "+opponent.name()+". I don’t think I’ve ever needed it that badly before, so thanks.\"</i> A hint of mischief arises in her eyes, and she gives you a quick peck on the cheek. <i>\"Even if it’s your fault I got so riled up in the first place.\"</i> You can’t help but chuckle, returning the kiss with one of your own. The two of you stay and rest for a few moments, before getting up and returning to the Games.<p>";
		}
		if(character.has(Trait.madscientist)&&character.has(Flask.SPotion)&&Global.random(2)==0){
			character.add(new Hypersensitive(character));
			return "Mara begins to panic as she realizes she's on the verge of defeat. She grabs a small bottle of liquid from a pouch on her belt, but it slips from her fingers " +
					"as she shudders in orgasm. You finger her pussy until she goes limp. While you're waiting for her to recover, you take a look at the bottle she dropped. " +
					"You recognize this stuff, it greatly heightens sensitivity when absorbed into skin. It seems like a shame not to use it.<p>"
					+ "Mara sits up, looking at you " +
					"nervously, but doesn't protest or resist as you open up the bottle and dab a small amount on her nipples. She bites her lip to keep from moaning, but can't " +
					"suppress her shivers when you start to play with her breasts. You capture her lips and focus on her hypersensitive nipples until you feel her trembling in " +
					"orgasm again. This seems pretty effective, but you're just getting started.<p>While Mara is still shaking in pleasure, you take the opportunity to coat " +
					"your fingers in the liquid. As she comes to her senses, you slip those fingers into her pussy and rub the potion into her vaginal walls. She writhes against " +
					"your hand and moans loudly, no longer able to control her voice. You continue playing with her pussy and lick her hard nipples. As she starts building to another " +
					"climax, you let up momentarily and use your fingers to peel the hood off her love button. <i>\"Wait! Are you going to put that stuff on my-KYA!\"</i> Her hips jump as " +
					"you rub the liquid directly into her little bud. Her love juice floods out and you realize she's cumming again.<p>"
					+ "You're worried that Mara's love juice will wash " +
					"away the potion, so you pour more onto her pussy lips. She clutches at you desperately even as her orgasm dies down. <i>\"You sexy, wonderful, evil, sexy man! I've " +
					"never needed a penis in me this much! Pleasepleasepleasepleaseplease! Just do me!\"</i> You have been toying with her for awhile. Even if you wanted to be be more " +
					"sadistic, you can't deny your own needs. You thrust your dick into her and she screams in pleasure. She may be climaxing again, but you're not going to slow down " +
					"to check. Her pussy feels amazing! Some of the sensitivity potion is probably soaking into your cock, making it feel better than usual. Between that and the length " +
					"of time you've been playing with her without relief, it doesn't take long for you to shoot your load into her. Her pussy spasms and this time there's no question " +
					"she's cumming. You lean in and kiss her gently as she catches her breath.<p>"
					+ "After you've both recovered enough to get back to your feet, Mara punches you weakly " +
					"in the chest. <i>\"You jerk! Do you have any idea how long this stuff lasts? How am I suppose to win my next fight when I'm this sensitive?\"</i> She pulls your head down " +
					"to her height and kisses you passionately before storming off.";
		}
		if(Global.random(2)==0){
			return "<i>\"Oh!\"</i> Mara stops her struggles as your fingers push her over the edge. While the sudden cry of ecstasy and stiffening of her body would make you think she lost, you wouldn’t put it past Mara to fake an orgasm just to do a last minute reversal. You continue fingering her, noting her meak attempts to remove your fingers, and stop when your fingers are splashed with her juices and a louder cry of pleasure escapes her. Her legs give in from the extra stimulation and she’s now on her knees. <p>" +
					"<i>\"Making me cum twice like that, how merciless,\"</i> Mara says hazily, a slow smile forming on her lips. Huh, so the first one was real, <i>\"And I thought I was good with my fingers. If I had any of my projects around I could show you just how good I am at handling kinks. Maybe I could show you next time?\"</i> <p>" +
					"She’s teasing you, purposefully ignoring your dick to lock eyes with you. While she is under no obligation to deal with it, you did make her cum twice. Seems like common courtesy. You tap your dick onto her face a few times, bouncing it off on her forehead, cheeks, even the tip of her nose, but Mara is content to just grin and ignore your erect cock. <p>" +
					"<i>\"You know, with how hard you’ve made me cum I’m feeling kind of parched. Maybe after the match we could go get a-\"</i> You thrust your cock into her open mouth, unsurprised when she begins sucking your cock without missing a beat. You feel her laughing, or close to it as she doesn’t stop her blowjob. You roll her eyes in what she thinks is a win, letting her play with your dick as she pleases. <p>" +
					"Alternating between deep throats and quick licks of your shaft, Mara devotes herself to your pleasure, never taking her eyes off your as she does. Her hands gently squeezes your balls so she can push out as much sperm as she can. As you reach your peak, your expression contorts in pleasure. Mara notices this and only redoubles her efforts. When you cum, you thrust forward while dragging Mara’s head into your crotch, causing her to muffle out something in surprise as you fill her throat with your load. <p>" +
					"Once your cock stops twitching, you slowly pull away your cock. Mara is dazed, her expression vacant with her hair frazzled from your rough treatment. When your dick finally leaves her lips, she breathes softly onto the softening tip as she gathers her breath. You notice the last bit of cum from your tip is on her lips. Her tongue laps it up happily as she smiles brightly up to you. <p>" +
					"<i>\"Hm, I guess I got my snack,\"</i> She giggles, getting up to her feet in a single jump. She runs up for a hug, burying her head into your chest, <i>\"I am actually a bit hungry. If you want, we can get some food afterwards, kay?\"</i> <p>" +
					"It wouldn’t be good to eat now considering how late it is. You tell her that she has to watch her figure.<p>" +
					"<i>\"Meanie. Guess I’ll just have to mooch off of you then later,\"</i> With one final squeeze she breaks off the hug and scampers off to a different room. You can only shake your head as you move to gather up her clothes. <p>" +
					"<i>\"Oh, by the way!\"</i> Mara’s head pops up from around the corner with a grin, <i>\"That first orgasm was totally fake.\"</i> <p>" +
					"She disappears once more. Hah, you knew it!";
		}
		return "You've managed to reduce the ever arrogant and composed Mara to a puddle of whimpering pleasure. You caress and finger her to a shuddering climax. You don't " +
				"let up, prolonging her orgasm as long as possible. <i>\"Wait! wait! I already came! I'm really sensitive right now!\"</i> You continue toying with her twitching pussy " +
				"while she moans and pleads for mercy, but you are enjoying this rare opportunity to have the scheming minx at your mercy. Soon she arches her back, hitting her " +
				"second peak, and you finally let her catch her breath.<p>"
				+ "After Mara recovers enough to talk, she tells you to lie back and she goes to work, pleasuring " +
				"your straining erection. You've given her two orgasms with no relief yourself, so it only takes a bit of stroking and sucking to bring you to the brink. Just as " +
				"you feel your ejaculation building, she grips your shaft tightly enough to deny your release. She gives you her most impish smile as you look at her questioningly. " +
				"<i>\"I never said I was going to get you off, but feel free to continue on your own.\"</i> She lets go of you and sits back in front of you smugly. <i>\"Just a bit of petty " +
				"revenge for taking advantage of me.\"</i><p>"
				+ "It seems unfair that she's the one setting the terms when you won the fight, but she's not actually obliged to do " +
				"anything for you. You can't just leave in your current state. If someone else caught you this frustrated and aroused, you wouldn't stand a chance. " +
				"You grudgingly take matters into your own hands and begin masturbating while Mara smiles in amusement. Soon you're past your limit and you ejaculate " +
				"onto her face and breasts. She giggles and licks the cum from her lips as you claim her clothes as a trophy and walk away.";
	}

	@Override
	public String describe(){
	    if(character.getPure(Attribute.Temporal)>1){
	       return "Mara's science fiction look has reached a new level. Her assortment of thrown together inventions have been replaced by a sleek, futuristic body-suit. The high-tech gauntlet on her " +
                   "hand seems to be the culmination of all her inventions, but you have no idea how she packed them all into such a compact form. She looks a bit like a supervillain, albiet a " +
                   "very cute and petite one.";
        }
		else if(character.has(Trait.madscientist)){
			return "Mara has gone high tech. She has a rig of equipment on harnesses that seem carefully placed so as not to interfere with clothing removal. The glasses she's wearing appear to be " +
					"computerized rather than prescription. She also has a device of unknown purpose strapped to her arm. Underneath all of that, she has the same cute, mischievous expression she " +
					"you're used to.";
		}
		else{
			return "Mara is short and slender, with small but well shaped breasts. She has dark skin, and short, curly black hair. Her size and cute features make her look a few years " +
					"younger than she actually is, and she wears a near constant playful smile. She's far from physically intimidating, but her sharp eyes reveal her exceptional intellect.";
		}
	}

	@Override
	public String draw(Combat c,Result flag){
		Character opponent=c.getOther(character);
		character.clearGrudge(opponent);
		if(flag==Result.intercourse){
			return "You thrust you dick into Mara's tight pussy while she writhes under you. You tease and pinch her nipples to increase her pleasure, but it's not really necessary. " +
					"She's obviously already on the brink of climax. Thanks to the pleasurable tightness of her entrance, you're not far behind her, but you should be able to endure " +
					"if you're careful with the pace. You've barely finished forming the thought when Mara catches you by surprise with an aggressive kiss and rolls on top of you. She " +
					"rides you passionately, too aroused to prevent her own orgasm, but eager to take you with her. It works. Her already tight pussy squeezes your cock as she orgasms, " +
					"milking you. Pleasure overwhelms you as you fill her womb with your seed.<p>"
					+ "Mara absentmindedly rubs her abdomen as you both enjoy the afterglow. <i>\"You came so much " +
					"inside me. It'll be a wonder if I'm not knocked up. If we have a boy, should we name him after you? "+opponent.name()+" jr. has a nice ring to it.\"</i> Pregnant?! You " +
					"feel a cold panic grip you as you try to imagine balancing college and caring for a baby. Mara, on the other hand lets out a relaxed giggle. <i>\"You don't need to look " +
					"so nervous. I was just making a joke. Do you think they'd let us do this without birth control?\"</i> That's right. You do vaguely remember that being mentioned when the " +
					"competition was explained to you.<p>"
					+ "You relax, but Mara has gone quiet as she stares at your face thoughtfully. <i>\"You might actually make a good father.\"</i> " +
					"She leans down to give you a quick kiss before hopping to her feet. <i>\"But not yet. There are a lot of things I want to accomplish before I settle down to start a family.\"</i>";	
		}
		else if(character.getPure(Attribute.Temporal)>=1 && Global.random(2)==0){
			return "You can feel yourself rushing toward an inevitable orgasm as Mara's mouth works expertly on your cock.  "
					+ "Mara has gotten very good at bringing you pleasure during the games and knows exactly what spots to hit "
					+ "as she licks and fondles you.  However, you have learned a lot about her too and have been working hard on "
					+ "her pussy with your tongue the entire time.  You can feel her getting close as well and have resigned "
					+ "yourself to this becoming a draw.  With a simultaneous grunt you both give in to the pleasure and release.  "
					+ "You stop eating Mara out but she continues sucking on you, working to take every last drop of your cum "
					+ "down her throat until you stop throbbing and begin to shrink.<p>"
					+ "<i>\"Well that was rude,\"</i> she said with a pout, <i>\"Why did you stop?\"</i>  You give her a half smile "
					+ "and start to get up when she pushes you back down.  She straddles your stomach, slowly grinding herself against it, "
					+ "as she stares down at you.<p>"
					+ "<i>\"You owe me some more time,\"</i> she says.  You start to disagree and remind her that this isn't a "
					+ "sparring session but a full, in-progress game.  She holds up her finger as she plays with the device on her wrist.  "
					+ "Suddenly there is a beep and something shoots up into the air.  You start to look around, trying to figure out "
					+ "what it was that went flying, when she grabs your face and pulls you into a deep kiss.  She leans down close to you, "
					+ "pressing her small breasts into your chest as she continues to grind against your stomach.  You close your eyes as "
					+ "her tongue begins to work its way between your lips, your concern melting away beneath her passion.<p>"
					+ "<i>\"This is nice,\"</i> Mara says when she breaks the kiss and lays her head on your chest, "
					+ "<i>\"Just the two of us cuddling.\"</i>  Her hands play over your chest as you again consider reminding her that "
					+ "this isn't really the best time for this.  However, she looks so happy laying there that you don't want to "
					+ "disrupt the moment.  You begin to run your hands down her back as the two of you lay there.  <p>"
					+ "Mara lets out a little shiver as your hands come close her ass.  You feel her begin grind herself harder and "
					+ "harder against you.  She lets out a little moan as you grab her ass cheek with your hand and give a squeeze.  "
					+ "Her hand works down over your stomach and finds its way between your legs, gently toying with your balls.  "
					+ "You smile down at her as her index finger begins to trace up and down the underside of your hardening dick.  "
					+ "You decide to respond by letting your hand trace down over her hip and start to work between the two of you, "
					+ "trying to reach between her legs.  She looks up and you and smiles as she realizes what you are trying.<p>"
					+ "<i>\"You can't just let it be about cuddling can you?\"</i> she asks, lifting herself off you a bit to look you in the eye.  "
					+ "Her shifting is enough for you and she lets out a groan as you find her clit.  <p>"
					+ "<i>\"Damn you,\"</i> She says through her teeth, <i>\"You and your sexy manipulation.\"</i> She shifts herself so "
					+ "that she is over you, and next thing you know she is sinking down on your cock until she has taken you in completely.  "
					+ "She begins to slowly ride you grinding around in slow circles while her hands move up to play with her nipples.  "
					+ "You let one hand wander forward to find her clit again and rub it as she moves.  <p>"
					+ "Your pace at this point, controlled mostly by Mara, is slow and loving instead of the typical fast and furious of a match.  "
					+ "She is clearly in no hurry to finish this and you are enjoying it too much to argue.  You can feel her pussy engulfing your dick, "
					+ "feel it as it moves up and down as well as side to side, and enjoy every time it squeezes your shaft.  Because you came so "
					+ "recently you aren't even close to the edge yet, but watching Mara on top of you is such a pleasurable sight that you just "
					+ "want to stay like this forever.<p>"
					+ "Eventually Mara reaches down and grabs your hands.  She pulls them up to her breasts, letting you take over playing with her "
					+ "nipples as she leans close to you.  She begins to dig her fingernails into your chest as her hips move up and down "
					+ "faster and faster.  She is moaning and groaning harder and harder and you can feel her wetness as she slides up and down your cock.  "
					+ "Suddenly you feel her pussy begin contracting hard around you, milking your dick, and you know she is cumming.  "
					+ "She rides you through it and begins to slow down as her orgasm ends.  Finally she lets out a sigh and climbs off of you.<p>"
					+ "<i>\"I'm too sensitive to continue right now,\"</i> she says as she lays down next to you, her hand grabbing your hard "
					+ "dick and slowly stroking it.  <i>\"Give me a couple minutes.\"</i>  She lays there and you enjoy the feeling of her giving "
					+ "you a hand job.  As the minutes pass, she slowly picks up the pace and you feel yourself getting closer to the edge.  "
					+ "She watches you, gauging your reactions and slowing down when she thinks you are getting close.  She keeps this up for "
					+ "what feels like forever and then begins to get up, moving to straddle you again.<p>"
					+ "Suddenly you hear something hit the ground off to the side.  Mara looks at first surprised and then disappointed.<p>"
					+ "<i>\"Never enough time,\"</i> she mutters under her breath.  She quickens her pumping and in almost no time you explode "
					+ "with a groan, spraying both your stomach and hers with your cum.  She gets up and quickly wipes herself off.<p>"
					+ "<i>\"That was fun,\"</i> she says as she walks off, <i>\"I hope we get to do it again soon.\"</i>";
		}
		else if(character.has(Trait.madscientist)&&character.has(Flask.Aphrodisiac)&&Global.random(2)==0){
			return "You know that you and Mara are both approaching your limits, yet she's chosen to break away from you rather than finish you off. You spot her one hand grasping a bottle and realize "
					+ "she is trying to load it into the device on her arm. You quickly lunge at her waist, attempting to tackle her to the ground, and the container is tossed "
					+ "into the air as you both tumble to the ground.<p>"
					+ "You conveniently land with Mara's legs splayed open and your face resting on her thigh as you both "
					+ "recover. You take advantage of her disoriented state and lower your mouth to her dripping nether lips "
					+ "and begin to lick up her juices. Mara seemingly begins to write in ecstasy under the attention of your "
					+ "tongue. Her breathing quickens, and she pulls her legs back, encouraging you to press them inwards as "
					+ "you greedily lap up her juices.<p>"
					+ "What you fail to notice at first is that Mara's writhing has actually been a coordinated attempt to recover the "
					+ "Aphrodisiac she dropped earlier. You hear all too late her pop it into the device on her arm.<p>"
					+ "<i>\"This is for tackling me, you brute!\"</i><br>"
					+ "You try to look up only to take a face full of pink mist as you're both spritzed due to the close proximity. "
					+ "Your minds slips into a lust filled frenzy and Mara's taste is no longer enough for you. Based on the "
					+ "inviting grin on her face, and the slickness of her entrance, you're sure she wants it just as badly as you "
					+ "when you plunge your cock into her waiting folds.<p> "
					+ "Both of your pent up lust explodes as you both climax in unison after the first few thrusts. This isn't "
					+ "enough for either of you, as you continue your frantic love making until you've both cum a handful of "
					+ "times. Slowly coming down from the high of what you both shared, you plant a kiss on Mara's head "
					+ "while catching your wind. As you peek down into her eyes, she gets a devious grin that shows she is "
					+ "scheming again already. You help her up, both thoroughly satisfied, and she pauses.<p>"
					+ "<i>\"Hey, maybe next time you could just let me hit you with the aphrodisiac first, then you can just cum "
					+ "your brains out for me and let me win. How's that sound instead?\"</i> You both chuckle before walking away.";
		}
		else{
		return "Mara is already trembling in impending orgasm and for a moment it seems like you've beaten her, but in her last second desperation, she redoubles her efforts. She manages to find " +
				"and stimulate all the most sensitive spots on your dick and gently squeezes your balls, pulling you over the edge with her. The two of you stay locked in " +
				"an embrace as you shudder through your simultaneous orgasms, your spurting cock making a sticky mess on her stomach. You enjoy your afterglow, holding the " +
				"lovely, petite girl, who for just a moment is too tired to put up her normal facade.<p>"
				+ "Soon, however, Mara has recovered. She gives you her usual, mischievous grin " +
				"and opens her mouth for what will surely be a snarky and insincere quip that ruins this genuine moment. You interrupt her by pressing your mouth against hers in a deep kiss. " +
				"To your surprise, she practically melts in your arms. Encouraged by her reaction, you run your hands down her body and gently probe her nethers. Despite having " +
				"just cum, you find a fresh flood of moisture leaking from her pussy. You eventually break the kiss and Mara looks up at you silently, eyes wet with desire. The " +
				"sight revives your spent erection. You kiss her again and slide your dick into her tight depths. She moans softly against your lips and moves her hips in time with yours. " +
				"Soon you're thrusting against each other in earnest, no longer competing, but just trying to bring each other pleasure. You cum inside her, setting off her second " +
				"shivering orgasm.<p>"
				+ "This time, when you both recover Mara just smiles contently and gives you a peck on the lips. If only she was always this honest and cute. Somewhat " +
				"regretfully, you collect her clothes and go on your way.";
		}
	}
	@Override
	public boolean fightFlight(Character opponent){
		return !character.nude()||opponent.nude();
	}
	@Override
	public boolean attack(Character opponent){
		return true;
	}
	@Override
	public void ding(){
		if(character.getRank()>=4){
			character.mod(Attribute.Science, 1);
			character.mod(Attribute.Temporal, 1);
			int rand;
			for(int i=0; i<(Global.random(3)/2)+1;i++){
				rand=Global.random(4);
				if(rand==0){
					character.mod(Attribute.Power, 1);
				}
				else if(rand==1){
					character.mod(Attribute.Seduction, 1);
				}
				else if(rand==2){
					character.mod(Attribute.Cunning, 1);
				}
				else if(rand==3){
					character.mod(Attribute.Science, 1);
				}
			}
		}
		else if(character.getPure(Attribute.Science)>=1){
			character.mod(Attribute.Science, 1);
			int rand;
			for(int i=0; i<(Global.random(3)/2)+1;i++){
				rand=Global.random(4);
				if(rand==0){
					character.mod(Attribute.Power, 1);
				}
				else if(rand==1){
					character.mod(Attribute.Seduction, 1);
				}
				else if(rand==2){
					character.mod(Attribute.Cunning, 1);
				}
				else if(rand==3){
					character.mod(Attribute.Science, 1);
				}
			}
		}
		else{
			character.mod(Attribute.Cunning, 1);
			int rand;
			for(int i=0; i<(Global.random(3)/2)+1;i++){
				rand=Global.random(3);
				if(rand==0){
					character.mod(Attribute.Power, 1);
				}
				else if(rand==1){
					character.mod(Attribute.Seduction, 1);
				}
				else if(rand==2){
					character.mod(Attribute.Cunning, 1);
				}
			}
		}
		character.getStamina().gain(4);
		character.getArousal().gain(4);
		character.money += character.prize()*5;
	}
	@Override
	public String victory3p(Combat c, Character target, Character assist){
		character.clearGrudge(target);
		character.clearGrudge(assist);
		if(target.human()){
			return "Mara settles between your legs, holding your dick between her bare feet. Her soft, smooth soles begin to stroke the length of your shaft. She frequently " +
					"uses her toes to tease your balls and the head of your penis. As you start to leak pre-cum, she smears it all over your dick, using the lubricant to give " +
					"you a more intense footjob. Mindful of her own needs, she reaches between her own legs and starts rubbing her pussy and clit, giving you a sexy show. " +
					"For a moment, it crosses your mind that if you can hold out long enough, she may slip up and climax before you. The moment passes, however, when Mara " +
					"accelerates her stroking and you realize you won't be able to last more than a few more seconds. Sure enough, your jizz soon shoots into the air like " +
					"a fountain and paints her legs and feet. Mara continues to stimulate your oversensitized dick and balls while she finishes herself off, apparently too " +
					"caught up in her own enjoyment to notice your discomfort.";
		}
		else{
			if(target.hasDick()){
				return String.format("Mara approaches %s like a panther claiming its prey. She runs her fingers down the length of %s's body, eliciting a shiver " +
					"each time she hits a ticklish spot. Her probing fingers avoid %s's nipples and genitals, focusing instead on the ticklish areas under %s arms, " +
					"behind %s knees and on %s inner thighs. You struggle to hold onto %s as %s squirms and pleads for mercy. After a few minutes, %s pleas " +
					"shift in tone and you realise Mara's dancing fingers have moved to %s dick and balls. %s entire body trembles as if unable to decide whether it's being " +
					"tickled into submission or stroked to ejaculation. You finally hear a breathless gasp as %s hits %s climax and shudders in your arms. You release " +
					"%s and %s collapses, completely exhausted. Mara grins at you mischievously. <i>\"%s obviously enjoyed that. Do you want to be next?\"</i>",
					target.name(),target.name(),target.name(),target.possessive(false),target.possessive(false),target.possessive(false),target.name(),target.pronounSubject(false),
					target.possessive(false),target.possessive(false),target.possessive(false),target.name(),target.possessive(false),target.pronounTarget(false),target.pronounSubject(false),
					target.possessive(true));
			}else{
			return "Mara approaches "+target.name()+" like a panther claiming its prey. She runs her fingers down the length of "+target.name()+"'s body, eliciting a shiver " +
					"each time she hits a ticklish spot. Her probing fingers avoid "+target.name()+"'s nipples and pussy, focusing instead on the ticklish areas under her arms, " +
					"behind her knees and on her inner thighs. You struggle to hold onto "+target.name()+" as she squirms and pleads for mercy. After a few minutes, her pleas " +
					"shift in tone and you realise Mara's dancing fingers have moved to her pussy and clit. Her entire body trembles as if unable to decide whether it's being " +
					"tickled into submission or fingered to ecstasy. You finally hear a breathless gasp as "+target.name() +" hits her climax and shudders in your arms. You release " +
					"her and she collapses, completely exhausted. Mara grins at you mischievously. <i>\"She obviously enjoyed that. Do you want to be next?\"</i>";
			}
		}
	}
	@Override
	public String intervene3p(Combat c, Character target, Character assist){
		if(target.human()){
			return "You face off with "+assist.name()+", looking for any opening. Her eyes dart momentarily past you, but before you can decide if her distraction is " +
					"real or a feint, a small hand reaches between your legs and grabs your nutsack tightly. You can't get a good look at your attacker, clinging to your back, " +
					"but her small size and mocha skin give away Mara's identity. This information doesn't really help you much, as it's too late to defend yourself." +
					" She yanks on your jewels, forcing you to your knees. Both girls work to restrain your arms, but it's " +
					"not really necessary since Mara literally has you by the balls. She releases your abused jewels once the fight has left you and focuses on holding your arms, " +
					"while "+assist.name()+" moves to your front.<br>";
		}
		else{
			return "So far this hasn't been your proudest fight. "+target.name()+" was able to pin you early on and is currently rubbing your dick between her thighs. " +
					"You've almost given up hope of victory, until you spot Mara creeping up behind her. She seems thoroughly amused by your predicament and makes no " +
					"move to help you, despite being easily in reach. You give her your best puppy-dog eyes, silently pleading while trying not to give away her presence. " +
					"Mara lets you squirm a little longer before winking at you and tickling "+target.name()+" under her arms. "+target.name()+" lets out a startled yelp " +
					"and jumps in surprise. You use the moment of distraction to push her off balance and Mara immediately secures her arms.<br>";
		}
	}
	
	public String watched(Combat c, Character target, Character viewer){
		if (viewer.human()){
			if(character.has(Trait.madscientist)){
				return "You are sneaking around, looking for your next opponent when you hear something just ahead "
						+ "of you. It sounds like two people in the process of fighting, but you decide to be cautious in case it is "
						+ "really a trap. You move forward quickly and quietly, and in a minute come up on the edge of a battle "
						+ "between Mara and " + target.name() + ". The two are absorbed in their battle so they definitely didn't notice your "
						+ "approach. You consider intervening to help determine the winner, but after watching a minute you see "
						+ "that Mara clearly has the upper hand and doesn't appear to need help. In fact you fear that if you try to "
						+ "step in and help " + target.name() + " then there is a good chance she will still lose and you will just end up being an "
						+ "easy target for Mara. So you decide to find a good spot to hide and watch how it proceeds, maybe you "
						+ "can pick up some tricks to use in the future.<p>"
						+ "You find a spot for yourself where you are sure the girls won't see you and start to get a feel for "
						+ "where the fight stands. Mara is currently battling topless, however she has managed to strip her "
						+ "opponent down to just one piece of clothing. Both are breathing hard but you can't really tell at this "
						+ "distance whether that is from exertion or arousal. Both girls are definitely giving it their all, but Mara is "
						+ "keeping the upper hand through the use of her assortment of gadgets. You are pretty sure you know "
						+ "how this match is going to end, and are pretty sure it won't be long before " + target.name() + " knows it as well.<p>"
						+ "Suddenly there is a flash and you watch as Mara's opponent's last piece of clothing "
						+ "disintegrates. She stops for a minute to cover up and the hesitation is enough for Mara to make a move.<p>"
						+ "Mara rushes her and knocks her to the ground. Suddenly you see Mara's slime move toward the prone "
						+ "body of her opponent. The slime must have been lying low to the ground all this time because you "
						+ "didn't see it when you originally surveyed the scene, but it doesn't seem to surprise the girl as it begins "
						+ "to ooze up her body. As you and Mara both watch, " + target.name() + " scrambles to try and get up from the ground "
						+ "and away from the slime. However she was still too stunned by Mara's attack and the slime continues "
						+ "to move up her legs and over her stomach. Finally she realizes it was too late and just lays there "
						+ "conserving her energy as she is covered to her neck by the slime. The slime begins to pulsate and " + target.name() + " "
						+ "begins to moan so loudly that you can hear.<p>"
						+ "Mara moves over and stands over her opponent defiantly, acting as if the match is already hers. "
						+ "To further demonstrate her confidence in her pending victory, she strips off her remaining clothing. She "
						+ "stands, straddling her trapped opponent and begins to slowly finger her clit as she watches " + target.name() + " squirm "
						+ "in apparent ecstasy. Unfortunately Mara got too cocky too fast, while she thinks " + target.name() + " was squirming in "
						+ "ecstasy it turns out the other girl is actually fighting through the slime to find it's weak spot. Suddenly "
						+ "the slime spasms violently and melts away, freeing " + target.name() + ". The second her arms are free, she grabs "
						+ "Mara's legs and turns the tables, bringing her to the ground and landing on top of her.<p>"
						+ "You are surprised, you have never seen someone get free after being fully engulfed in a slime, "
						+ "and like Mara you believed the match would be hers. Now you can see that she is the one in trouble, "
						+ "laying on the ground under " + target.name() + ". Mara tries to protect her pussy with her hands but her opponent "
						+ "goes for her nipples first. Mara squirms under her while trying to mount a defense, but only succeeds in "
						+ "opening her pussy up for her attacker's fingers, which she takes advantage of. Mara is struggling against"
						+ "the hands that are now simultaneously assaulting her and holding her to the ground.<p>"
						+ "Finally Mara gets her hands up and puts them against the other girl's chest, just under her "
						+ "breasts. " + target.name() + " suddenly goes rigid and you realize that Mara has sent an electric charge through her "
						+ "body to incapacitate her. She pushes her off of her, and immediately dives between her legs. Mara "
						+ "begins to finger fuck her while licking her clit. Mara has clearly decided the best course is to end this "
						+ "quickly instead of risking losing the upper hand again. " + target.name() + " tries to push Mara's head away, but Mara "
						+ "pulls her fingers out and grabs her hips in both hands, allowing her the leverage to keep her close.<p>"
						+ "Mara's attack is obviously working as her opponent's struggles diminish and you can tell she is getting "
						+ "closer and closer to cumming. " + target.name() + " finally gives up completely and begins pinching her own nipples, "
						+ "and in no time you can hear her groan as she loses the match.<p>"
						+ "Mara isn't done yet though. She seems to decide that she deserves something for all of the "
						+ "trouble. She gets up and before her opponent recovers, straddles the fallen girl's head. From your angle you "
						+ "can't see everything, but you can see Mara moving back and forth slowly, so she appears to be getting "
						+ "the pussy licking that she wants. Mara's hands go up to her small breasts and begin playing with her "
						+ "nipples as she moves faster and faster. She must have been closer to orgasm then she let on because it "
						+ "seems like no time before she is tilting her head back and letting out a loud groan. Suddenly she stops "
						+ "moving and you see the muscles in her ass flex as she cums.<p>"
						+ "After Mara recovers, she looks down at her vanquished foe and says something that you can't "
						+ "hear. Then she gets up, gets dressed quickly, and walks off with " + target.name() + "'s clothes in hand. You wait until "
						+ "" + target.name() + " also gets up and walks off before you leave your hiding spot and continue the match.";
			}else{
				return "You catch sight of two figures engaged in combat and move to approach, but at the last moment you spot a tripwire laid out in front of you and just barely manage to avoid it. You can probably get past it now that you know where it is, but something in the back of your mind is telling you that it might not be the only trap set up here. Avoiding this trap could land you right in another Even though this would normally be a prime opportunity to intervene in the match and gain an easy win, getting yourself trapped right now could easily convince the two competitors to put aside their differences and take you out first. It's probably safest to simply hide out of sight and see how this match plays out without getting involved.<p>" +
						"The smaller figure is easy to identify as Mara, and the other would then be "+target.name()+". Mara's already completely naked, while "+target.name()+" hasn't lost a single scrap of clothing. Combining this with the fact that "+target.name()+" has just managed to knock Mara to the ground and is currently tickling her mercilessly, it would seem that Mara's in deep trouble right now.<p>" +
						"<i>\"Stop! Ah-haha...\"</i> Mara cries out, trying weakly to push "+target.name()+" off of her, but her opponent refuses to budge. <i>\"No fair... no fair...\"</i> Tickling alone isn't likely to bring Mara to orgasm, but with how much it's weakening her right now, it leaves "+target.name()+" with the perfect opportunity to bring her fingers down to Mara's pussy and begin trying to get her off. <i>\"Ahh! No!\"</i> Mara cries out.<p>" +
						"You can just barely make out "+target.name()+" grinning down at Mara as she pushes the poor girl closer and closer to orgasm with one hand, while the other continues tickling across her bare chest and stomach. As you look at "+target.name()+"'s face though, you notice that her cheeks are flushed red. Despite being at the advantage clothing-wise, she seems to already be quite aroused herself. It takes a minute of glancing around the area before you spot the reason: the broken remains of an aphrodisiac trap that "+target.name()+" must have stumbled into.<p>" +
						"It seems that "+target.name()+" has recovered well enough from that trap, even if she is still quite aroused, and has this victory firmly in hand, at least until a voice blares out, \"Don't worry, Mara, I've got her!\"<p>" +
						""+target.name()+" immediately leaps to her feet, looking around the area frantically for the source of the voice, but she isn't able to spot anything before Mara sweeps her legs out from beneath her and she tumbles to the ground. <i>\"About time... that damn thing went off...\"</i> Mara says through heavy breaths as she climbs on top of "+target.name()+" and holds her to the ground. <i>\"I knew... I should have... set the timer for... two minutes instead of three.\"</i><p>" +
						""+target.name()+" lets out a grunt from beneath Mara. She tries to overpower the girl and turn the two of them over, but Mara must be mustering up every remaining ounce of her strength, as she somehow manages to maintain her superior position. On top of that, she even manages to slip her hand inside "+target.name()+"'s clothing and between her legs, fingering at her pussy.<p>" +
						"<i>\"Ggah!\"</i> "+target.name()+" lets out a mix of a groan and a moan as she finds herself suddenly at Mara's mercy. She squeezes her eyes shut as she tries to resist the smaller girl's fingers, but the redness in her cheeks evidences to the fact that this is a losing battle right now. She seems to realize this as well, as she tries to reach around to return the assault on Mara, in hopes of getting her off first, but her efforts are in vain. Bringing her hands behind her back like this just gives Mara and opening to grab her wrists. Mara then capitalizes on this by pulling a zip tie out of her supplies and binding "+target.name()+"'s wrists together, leaving her completely at Mara's mercy.<p>" +
						"With both of her hands now free to pleasure "+target.name()+", Mara slips one hand beneath her shirt and another between her legs, feeling up both her breast and her pussy at once. "+target.name()+" lets out a loud moan from this, and her body soon breaks out into a bout of shuddering as her orgasm suddenly overcomes her.<p>" +
						"Mara pulls her hands back from "+target.name()+", surprise showing on her face. <i>\"You came? But I didn't even have a chance to get your clothes off yet!\"</i> Mara scrunches her face for a moment in a pout. <i>\"This isn't fair at all. How is it that I'm the naked one here when you lost?\"</i> Mara glares at "+target.name()+" for a moment, then says, <i>\"Well, you have to strip anyway, so get at it.\"</i><p>" +
						""+target.name()+" looks up at Mara, then wordlessly glances toward her back. Her message is clear: She's not going to be able to strip very easily with her hands still bound.<p>" +
						"Mara follows "+target.name()+"'s gaze and then chuckles gently. <i>\"Right. Of course. I guess then I'll just have a bit of fun myself.\"</i> You catch sight of "+target.name()+"'s eyes widening for just a moment before Mara moves, obscuring your view. You can't see much of what's happening from this angle, but the clothing flying to either side of Mara gives you a pretty good idea of what's going on. <i>\"Good. Now let's see how you like it...\"</i><p>" +
						"Mara's voice is cut off by a scream and then laughter coming out of "+target.name()+". You let out a chuckle yourself; it seems like Mara has forgone an orgasm of her own as a reward for victory, deciding instead to get revenge on "+target.name()+" for the tickle attack earlier. That's not the choice you would have gone with, but you can't exactly blame Mara for making it.<p>" +
						"With the sounds of laughter filling the air, now's probably a good time for you to sneak off. With a final stolen glance at Mara mercilessly tickling "+target.name()+", you duck back the way you came and get ready for your next fight.<p>";
			}
		}
		return "";
	}
	@Override
	public String startBattle(Character opponent){
		if(character.getGrudge()!=null){
			switch(character.getGrudge()){
			case perfectplan:
				if(opponent.nude()){
					return "Mara looks strangely disappointed as she looks over your naked body. It's kinda a blow to your ego. "
							+ "Mara shakes her head hurriedly as she sees your expression.<p>"
							+ "<i>\"Oh no, you look hot! I like the view. I just had a cool plan to strip you naked, and now it's "
							+ "going to waste.\"</i>";
				}else{
					return "Mara smiles confidently as she sees you. <i>\""+opponent.name()+", I knew I'd find you here.\"</i><p>"
							+ "She seems self-assured, but you know she's just bluffing to compensate for her previous loss. She couldn't "
							+ "have known where you'd be.<p>"
							+ "Mara shakes her hips in a sort of stationary swagger and points downward. <i>\"Oh no? Then what are you standing on?\"</i><p>"
							+ "You look down and see that you're standing on an 'X' marked in chalk. While your attention is focused downward, you're "
							+ "suddenly soaked from above. Your clothes rapidly dissolve away, leaving you completely naked. Looking up at the ceiling "
							+ "far too late, you see the overturned bucket that previously contained the solution.";
				}
			case inspired:
				return "Mara looks pretty upbeat despite her recent loss. Did something good happen to her since your last fight?<p>"
						+ "<i>\"Not really, I'm just feeling particularly inspired right now. I've got a dozen new ideas running through my head.\"</i><p>"
						+ "She grins and gives you a seductive wink. <i>\"Maybe getting an orgasm from a hot boy is my muse. Why don't you come by "
						+ "my lab next time I'm working so we can test that theory.\"</i>";
			case planB:
				return "For once, Mara looks too frustrated to hide with her normal cutesy facade. It's probably because you've dealt her consecutive losses.<p>"
						+ "<i>\"OK, maybe my last couple plans haven't ended up going too well. I'm not exactly the sturdiest fighter here. "
						+ "But this time I've prepared some emergency supplies in case things go bad. This time- this time- this time I'll beat you!\"</i>";
			case experimentalweaponry:
				return "Mara shows up out of breath, holding a bag. She didn't have it with her at the start of the match. Did she run off to retrieve it?<p>"
						+ "<i>\"I wasn't planning to use any of these before they were properly tested, but you're stronger than I thought.\"</i> She pulls "
						+ "several sex toys out of the bag. <i>\"I designed these to be way more effective than normal toys. I'm like 80% certain they "
						+ "aren't dangerous.\"</i>";
				
			default:
				break;
			}
		}
		if(opponent.pantsless()){
			return "Mara giggles and crouches down to get a closer look at your dick. <i>\"It's such a funny looking thing, but I really want to play with it.\"</i><p>"
					+ "She looks up at your face and gives you a cute wink. <i>\"I bet you'd like that too.\"</i>";
		}
		if(character.nude()){
			return "Mara giggles sheepishly as a mild blush darkens her cheeks. She doesn't cover her naked body, but you can tell she's a bit self-conscious.<p>"
					+ "<i>\"I don't suppose you'll let me go get dressed before we start? No? Well, next time I catch you naked, you better be prepared for some "
					+ "serious teasing.\"</i>";
		}
		if(character.getAffection(opponent)>=30){
			return "Mara signals a 'Time Out' and walks toward you without any sign of hostility. There's no 'Time Out' rule in the Games, so this could be "
					+ "a trap, but you relax and let her approach anyway.<p>"
					+ "She stands on her tiptoes and kisses you tenderly on the lips. Technically, that is a sneak attack, but it's a pretty weak one.<p>"
					+ "She smiles as she backs away. <i>\"I wasn't trying to turn you on. I just wanted to kiss my boy before we started. I may need "
					+ "to use some dirty tricks during the fight, but that should remind you how I feel about you.\"</i>";
			
		}
		if(character.has(Trait.madscientist)){
			return "Mara shows off the complex gadget on her arm. <i>\"I've got some new toys now. You aren't just up against me, "
					+ "you're up against the power of science!\"</i>";
		}
		return "Mara smiles and faces you, practically daring you to attack.";
	}
	@Override
	public boolean fit(){
		return character.getStamina().percent()>=75&&character.getArousal().percent()<=10&&!character.nude();
	}
	@Override
	public String night(){
		Global.gui().loadPortrait(Global.getPlayer(), this.character);
		return "On your way back to your dorm after the match, you feel a sudden weight on your back that almost knocks you off your feet. It turns out to be Mara, who jumped " +
				"on your back in her enthusiasm to spend the night together.<p>"
				+ "You give her a piggyback ride back to the dorm, and per her request, head up to the roof. Unsurprisingly, " +
				"there's no one here this late at night and there's a good view of the stars.<p>"
				+ "Mara strips off her clothes and dances naked onto the rooftop. <i>\"There's nothing like " +
				"being naked in the moonlight. Come on!\"</i> You undress and put your clothes in a neat pile, taking the time to gather up hers as well. You walk up behind her and hold " +
				"her while enjoying the view. The night air is slightly cool, but her nude body is warm in your arms. She turns her head to give you a tender kiss before stepping out of " +
				"your embrace.<p>"
				+ "<i>\"Have you ever danced naked under the stars?\"</i> It's a strange question, but she looks too lovely in this light to refuse. The two of you dance without any " +
				"hint of style or rhythm, not caring how ridiculous you'd look to a third party.<p>"
				+ "When you've both tired, you spend some time just looking at the stars together. You " +
				"never would have imagined this is how you'd be spending your night, but Mara always finds ways to surprise you.<p>"
				+ "You suddenly realize she's no longer standing next to " +
				"you. You spot her back by the door, holding your clothes. She winks mischievously and dashes into the building. You give chase, still naked. You manage to catch her just " +
				"as she reaches your room.<p>"
				+ "You consider it a minor miracle no one saw the two of you streaking through the dorm building. You're going to have to find a way to pay her back " +
				"before morning.";
	}
	public void advance(int rank){
		if (rank >= 4 && character.getPure(Attribute.Temporal)==0){
			character.mod(Attribute.Temporal,1);
			character.outfit[Character.OUTFITTOP].removeAllElements();
			character.outfit[Character.OUTFITBOTTOM].removeAllElements();
			character.outfit[Character.OUTFITTOP].add(Clothing.nipplecover);
			character.outfit[Character.OUTFITTOP].add(Clothing.chestpiece);
			character.outfit[Character.OUTFITTOP].add(Clothing.techarmor);
			character.outfit[Character.OUTFITBOTTOM].add(Clothing.groincover);
			character.outfit[Character.OUTFITBOTTOM].add(Clothing.techpants);
			character.closet.add(Clothing.nipplecover);
			character.closet.add(Clothing.chestpiece);
			character.closet.add(Clothing.techarmor);
			character.closet.add(Clothing.groincover);
			character.closet.add(Clothing.techpants);
			character.clearSpriteImages();
		}
        if(rank >= 2 && !character.has(Trait.smallhands)){
            character.add(Trait.smallhands);
        }
		if(rank >= 1 && !character.has(Trait.madscientist)){
			character.add(Trait.madscientist);
			character.outfit[0].removeAllElements();
			character.outfit[1].removeAllElements();
			character.outfit[0].add(Clothing.bra);
			character.outfit[0].add(Clothing.shirt);
			character.outfit[0].add(Clothing.labcoat);
			character.outfit[1].add(Clothing.underwear);
			character.outfit[1].add(Clothing.pants);
			character.closet.add(Clothing.pants);
			character.closet.add(Clothing.labcoat);
			character.clearSpriteImages();
			character.mod(Attribute.Science, 1);
		}	
	}
	@Override
	public NPC getCharacter(){
		return character;
	}
	
	
	
	public boolean checkMood(Emotion mood, int value){
		switch(mood){
		case confident: case desperate:
			return value>=30;
		default:
			return value>=50;
		}
	}
	@Override
	public float moodWeight(Emotion mood){
		switch(mood){
		case confident: case desperate:
			return 1.2f;
		default:
			return 1f;
		}
	}
	@Override
	public String image(){
		return "assets/mara_"+ character.mood.name()+".jpg";
	}
	@Override
	public void pickFeat(){
		ArrayList<Trait> available = new ArrayList<Trait>();
		for(Trait feat: Global.getFeats()){
			if(!character.has(feat)&&feat.req(character)){
				available.add(feat);
			}
		}
		if(available.isEmpty()){
			return;
		}
		character.add((Trait) available.toArray()[Global.random(available.size())]);
	}
	@Override
	public String resist3p(Combat c, Character target, Character assist){
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public CommentGroup getComments(){
		CommentGroup comments = new CommentGroup();
		comments.put(CommentSituation.VAG_DOM_CATCH_WIN, "<i>\"Do you like it inside of me? Nice and tight?\"</i>");
		comments.put(CommentSituation.VAG_DOM_CATCH_LOSE, "<i>\"Oooh! Even now... Please, please...\"</i>");
		comments.put(CommentSituation.VAG_SUB_CATCH_WIN, "<i>\"Mmm, it doesn't matter if you're on top; you can't beat science!\"</i>");
		comments.put(CommentSituation.VAG_SUB_CATCH_LOSE, "<i>\"Oh wow! Oh... Harder!\"</i>");
		comments.put(CommentSituation.ANAL_CATCH_WIN, "<i>\"That's what you get for being such a dirty boy! Cum in my ass already!\"</i>");
		comments.put(CommentSituation.ANAL_CATCH_LOSE, "<i>\"Wait! Finish me in any other way!\"</i>");
		comments.put(CommentSituation.ANAL_PITCH_WIN, "<i>\"Hah! I thought men would be ashamed of this, but you seem to like it! Show me how much!\"</i>");
		comments.put(CommentSituation.SELF_BOUND, "<i>\"Pff, I'll have my hands free in just a second, and then I'll use them on you!\"</i>");
		comments.put(CommentSituation.BEHIND_DOM_WIN, "<i>\"Shall I use my hands to finish you off now? You know how good I am with them...\"</i>");
		comments.put(CommentSituation.OTHER_BOUND, "<i>\"Do you like being tied up? I am happy to oblige.\"</i>");
		comments.put(CommentSituation.SIXTYNINE_WIN, "<i>\"Come on! Try a little harder down there!\"</i>");
		comments.put(CommentSituation.SIXTYNINE_LOSE, "<i>\"No fair! I can barely reach your dick. Stop being so tall!\"</i>");
		comments.put(CommentSituation.SELF_HORNY, "<i>\"Why are you so sexy!? It's not fair!\"</i>");
		comments.put(CommentSituation.OTHER_HORNY, "<i>\"Is my hot body too much for you? Your dick looks ready to burst!\"</i>");
		comments.put(CommentSituation.OTHER_OILED, "<i>\"Your cock is all shiny with oil. It makes me want to play with it!\"</i>");
		comments.put(CommentSituation.PIN_DOM_WIN, "<i>\"You didn't think a little girl could pin you? It's called leverage!\"</i>");
		comments.put(CommentSituation.SELF_BUSTED, "Mara looks like she just had the wind knocked out of her, and says between gasps; <i>\"Owww!  That was uncalled for, you big bully!\"</i>");
		return comments;
	}
	@Override
	public int getCostumeSet(){
		if(character.getPure(Attribute.Temporal) > 0){
			return 3;
		}
		if(character.has(Trait.madscientist)){
			return 2;
		}else{
			return 1;
		}
	}
	@Override
	public void declareGrudge(Character opponent, Combat c){
		if((character.getGrudge()==Trait.inspired || character.getGrudge()==Trait.perfectplan)){
			character.addGrudge(opponent,Trait.planB);
		}else{		
			switch(Global.random(3)){
			case 2:
				if(character.has(Trait.madscientist)){
					character.addGrudge(opponent,Trait.experimentalweaponry);
					break;
				}
			case 1:
				character.addGrudge(opponent,Trait.inspired);
				break;
			case 0:
				character.addGrudge(opponent,Trait.perfectplan);
				break;
			default:
				break;
			}
		}
		
	}
    @Override
    public void resetOutfit(){
        character.outfit[0].clear();
        character.outfit[1].clear();
        if(character.getPure(Attribute.Temporal)>0){
            character.outfit[Character.OUTFITTOP].add(Clothing.nipplecover);
            character.outfit[Character.OUTFITTOP].add(Clothing.chestpiece);
            character.outfit[Character.OUTFITTOP].add(Clothing.techarmor);
            character.outfit[Character.OUTFITBOTTOM].add(Clothing.groincover);
            character.outfit[Character.OUTFITBOTTOM].add(Clothing.techpants);
        }
        else if(character.has(Trait.madscientist)){
            character.outfit[0].add(Clothing.bra);
            character.outfit[0].add(Clothing.shirt);
            character.outfit[0].add(Clothing.labcoat);
            character.outfit[1].add(Clothing.underwear);
            character.outfit[1].add(Clothing.pants);
        }else{
            character.outfit[0].add(Clothing.bra);
            character.outfit[0].add(Clothing.Tshirt);
            character.outfit[1].add(Clothing.underwear);
            character.outfit[1].add(Clothing.shorts);
        }

    }
}
