package characters;

import Comments.CommentGroup;
import Comments.CommentSituation;
import actions.Action;
import actions.Movement;
import combat.Combat;
import combat.Result;
import daytime.Daytime;
import global.*;
import items.Clothing;
import items.Toy;
import items.Trophy;
import pet.Ptype;
import skills.*;
import stance.Stance;
import status.Drowsy;
import status.Energized;
import status.Stsflag;

import java.util.ArrayList;
import java.util.HashSet;

public class Angel implements Personality {
	/**
	 *
	 */
	private static final long serialVersionUID = -8169646189131720872L;
	public NPC character;

	public Angel(){
		character = new NPC("Angel", ID.ANGEL, 1,this);
		character.outfit[0].add(Clothing.Tshirt);
		character.outfit[1].add(Clothing.thong);
		character.outfit[1].add(Clothing.miniskirt);
		character.closet.add(Clothing.Tshirt);
		character.closet.add(Clothing.thong);
		character.closet.add(Clothing.miniskirt);
		character.change(Modifier.normal);
		character.mod(Attribute.Seduction, 2);
		character.mod(Attribute.Perception, 1);
		character.getArousal().gain(10);
		character.money += 300;
		Global.gainSkills(character);
		character.add(Trait.female);
		character.add(Trait.undisciplined);
		character.add(Trait.experienced);
		character.add(Trait.lickable);
		character.setUnderwear(Trophy.AngelTrophy);
		character.plan = Emotion.hunting;
		character.mood = Emotion.confident;
		character.strategy.put(Emotion.sneaking, 1);
		character.preferredSkills.add(FaceSit.class);
		character.preferredSkills.add(Straddle.class);
		character.preferredSkills.add(Fuck.class);
		character.preferredSkills.add(Taunt.class);
		character.preferredSkills.add(LustOverflow.class);
		character.preferredSkills.add(Flick.class);
		character.preferredSkills.add(Paizuri.class);
		character.preferredSkills.add(LustAura.class);
		character.preferredSkills.add(AssFuck.class);
		character.preferredSkills.add(Strapon.class);
		character.preferredSkills.add(TailPeg.class);
		character.preferredSkills.add(FingerAss.class);

		if(Global.checkFlag(Flag.PlayerButtslut)){
			character.gain(Toy.Strapon);
			character.money -= 200;
		}

	}
	@Override
	public Skill act(HashSet<Skill> available,Combat c) {
		HashSet<Skill> mandatory = new HashSet<Skill>();
		HashSet<Skill> tactic = new HashSet<Skill>();
		Skill chosen;
		for(Skill a:available){
			if(a.toString()=="Command"||a.toString().equalsIgnoreCase("AssFuck") || a.toString() == "Strapon"){
				mandatory.add(a);
			}
			if(character.is(Stsflag.orderedstrip)){
				if(a.toString()=="Undress"||a.toString()=="Strip Tease"){
					mandatory.add(a);
				}
			}
			if(character.has(Trait.strapped)){
				if(a.toString()=="Mount"){
					mandatory.add(a);
				}
				if(a.toString()=="Turn Over"){
					mandatory.add(a);
				}
			}
		}
		if(!mandatory.isEmpty()){
			Skill[] actions = mandatory.toArray(new Skill[tactic.size()]);
			return actions[Global.random(actions.length)];
		}
		ArrayList<HashSet<Skill>> priority = character.parseSkills(available, c);
		if(Global.checkFlag(Flag.hardmode)&&Scheduler.getMatch().condition!=Modifier.quiet){
			chosen = character.prioritizeNew(priority,c);
		}
		else{
			chosen = character.prioritize(priority);
		}
		if(chosen==null){
			tactic=available;
			Skill[] actions = tactic.toArray(new Skill[tactic.size()]);
			return actions[Global.random(actions.length)];
		}
		else{
			return chosen;
		}
	}

	@Override
	public Action move(HashSet<Action> available, HashSet<Movement> radar, Match match) {
		Action proposed = character.parseMoves(available, radar, match);
		return proposed;
	}

	@Override
	public void rest(int time, Daytime day) {
		if(character.rank>=1){
			if(character.money>0){
				day.visit("Black Market", character, Global.random(character.money));
			}
		}
		if(!(character.has(Toy.Dildo)||character.has(Toy.Dildo2))&&character.money>=250){
			character.gain(Toy.Dildo);
			character.money-=250;
		}
		if(!(character.has(Toy.Onahole)||character.has(Toy.Onahole2))&&character.money>=300){
			character.gain(Toy.Onahole);
			character.money-=300;
		}
		if(!(character.has(Toy.Strapon)||character.has(Toy.Strapon2))&&character.money>=600&&character.getPure(Attribute.Seduction)>=20){
			character.gain(Toy.Strapon);
			character.money-=600;
		}
		String loc;
		ArrayList<String> available = new ArrayList<String>();
		available.add("Hardware Store");
		available.add("Black Market");
		available.add("XXX Store");
		available.add("Bookstore");
		available.add("Play Video Games");
		if(character.rank>0){
			available.add("Workshop");
		}
		for(int i=0;i<time-4;i++){
			loc = available.get(Global.random(available.size()));
			day.visit(loc, character, Global.random(character.money));
		}
		if(character.getAffection(Global.getPlayer())>0){
			Global.modCounter(Flag.AngelDWV, 1);
		}
		character.visit(2);
	}

	@Override
	public String bbLiner() {
		if(character.getAffection(Global.getPlayer())>=25){
			return "Angel smiles just a bit too sweetly. <i>\"Sorry lover, but by now you should know I'm a bit of a dom.\"</i>";
		}
		switch(Global.random(3)){
			case 1:
				return "Feigning remorse, Angel says <i>\"Sorry, cheap shots are all I can afford,\"</i> As she giggles sweetly at her own joke.";
			case 2:
				return "Angel cups her hands over her pussy, mocking your pain.  <i>\"Oh you silly little boys and your weak little balls.  That looks like it had to hurt.\"</i>";
			default:
				return "Angel seems to enjoy your anguish in a way that makes you more than a little nervous. <i>\"That's a great look for you, I'd like to see it more often.\"</i>";
		}
	}

	@Override
	public String nakedLiner() {
		return "Angel gives you a haughty look, practically showing off her body. <i>\"I can't blame you for wanting to see me naked, everyone does.\"</i>";
	}

	@Override
	public String stunLiner() {
		return "Angel groans on the floor. <i>\"You really are a beast. It takes a gentle touch to please a lady.\"</i>";
	}

	public String winningLiner() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String taunt() {
		return "Angel pushes the head of your dick with her finger and watches it spring back into place. <i>\"You obviously can't help yourself. If only you were a little bigger, we could have a lot of fun.\"</i>";
	}

	@Override
	public String victory(Combat c,Result flag) {
		character.arousal.empty();
		Character opponent=c.getOther(character);
		character.clearGrudge(opponent);
		if(flag==Result.anal){
			if(Global.getValue(Flag.PlayerAssLosses)==0) {
				Global.flag(Flag.tookAssAngel);
				Global.modCounter(Flag.PlayerAssLosses, 1);
				return "Angel holds you by the hips as she pistons in and out of you, doggy style. Although this wasn't something you'd expected to be very effective, you can't help " +
						"but feel a bolt of pleasure shoot through you each time Angel's strapon bumps up against your prostate. It's not enough to really make you lose " +
						"control, though, so you ignore the feelings and shift your weight to try to struggle free. For a moment, the steady fucking stops, and you're about " +
						"to lunge forward and escape when a harsh slap on your ass takes you by surprise. The shock of it causes your arms to give out, and a small gasp " +
						"escapes your lips as you collapse face-sideways to the ground. You see a slight flush and a triumphant grin on Angel's face. <i>\"Don't try to act like " +
						"you want out of this. I can see how hard you are - there's no way this is the cock of a boy who's able to fight back.\"</i> Demonstrating her point, she " +
						"reaches underneath you and gives your penis a firm squeeze, to which it throbs in response. <i>\"Or did you just want a proper breaking in?\"</i><p>" +
						"Angel resumes fucking you, but more slowly this time, with one hand wrapped tightly around your cock. With her large breasts pressed against your back, " +
						"you're made intensely aware of her femininity even as you feel her probing deeply inside you. She lets out a satisfied murmur as your dick twitches for " +
						"her in her hand, and in response begins to stroke it in time with her thrusts. You don't want to admit it, but she's right - you're enjoying this, and " +
						"it's not just because of the attention your cock is receiving. It isn't long before you find yourself close to the edge. " +
						"<i>\"Are you going to cum for me soon?\"</i> Angel purrs into your ear. <i>\"Shoot it out into my hand. Be a good boy and give Angel your cum.\"</i><p>" +
						"Her sudden dirty talk has the intended effect - with her breasts against your back, her hand around your cock, and her strapon repeatedly smashing itself " +
						"against your prostate, and her hand around your cock, " +
						"even the slightest extra stimulus is able to force you past your limit. Your anus tightens around the foreign object penetrating it, " +
						"and your penis throbs in Angel's grip for half a second before firing its first sticky spurt of semen across the floor. Angel quickly shifts her hand " +
						"and the second jet of cum shoots into her waiting fingers, as well as the third. You feel exhausted and slightly ashamed as a fourth and final " +
						"trickle of cum leaks out into Angel's hand, which she then withdraws and raises to her lips. Pushing several fingers into her mouth, she works " +
						"her tongue around them and savours the prize she took from you. Pinned underneath her, you can do nothing but pant and moan quietly as the aftershock of your " +
						"orgasm leaves you stunned. Angel looks at the back of her hand and lazily samples the small amount of cum left over at the base of her ring finger. " +
						"<i>\"I hope this wasn't too rough for your first time? I thought I'd better give you a bit of practice before someone like Jewel really " +
						"goes to town on you.\"</i> She pauses for a moment, swiftly but gently removes her toy from inside you, and firmly grips both of your asscheeks " +
						"with a sly smile. <i>\"Of course, since I was the first I guess these belong to me now. Whoops.\"</i> The added <i>\"whoops\"</i> sounds more taunting " +
						"than remorseful, and the smile doesn't leave Angel's face as she stands up and proudly walks away with your clothing.";
			}
			else if(Global.getValue(Flag.PlayerAssLosses)>=10 && Global.getValue(Flag.AngelDomme)>0 && Global.getValue(Flag.Bisexual) > -1){
				if(Global.getValue(Flag.Bisexual) == 0)
					Global.modCounter(Flag.Bisexual, 1);
				Global.modCounter(Flag.PlayerAssLosses, 1);
				return "You find yourself in an embarrassingly familiar position. Your shoulders on the floor, your legs in the air, "
						+ "and Angel's smug devilish grin inches above your face as she pistons her prick in and out of your ass. <i>\"Ahuhuhuhu. "
						+ "I didn't even need to try this time. As soon as you saw me put my cock on, you all but presented your little "
						+ "boypussy for me to conquer. Mmmmnh, you've really become addicted to being fucked like this, haven't you? Face to face as "
						+ "I ream your slutty ass?</i>\" Angel accentuates her taunt with a sudden series of prostate-smashing thrusts that make your vision go white with "
						+ "pleasure and leave you unable to restrain your voice. Angel's gloating expression becomes gleefully predatory as she slows down "
						+ "to take in the sight of you whimpering under her, enraptured by her domineering ministrations. She leans forward and you moan "
						+ "as you feel her hand forcefully pull your head forward, her teeth grazing your earlobe, her breath whispering hot and heavy in your ear. "
						+ "\"<i>Look how far you've fallen. What would the other girls say if they walked by and saw us right now? What would "
						+ "our mysterious benefactor think if he could see you, moaning out loud while getting dicked down hard by your mistress?\"</i><p>"
						+ "Your chest was flush with arousal before, but it absolutely throbs with Angel's lascivious taunting. "
						+ "This pales in comparison to how your brightly your face burns in embarrassment at the prospect of someone watching you "
						+ "getting dominated like this, but you feel your dick twitch at the thought. Angel lets out a short laugh as she returns to "
						+ "gloating over you, her thrusting picking up in both speed and force as she enjoys her absolute control over you. "
						+ "\"<i>Of course with how loudly you moan for me, they probably know all about your addiction to a good hard "
						+ "assfucking already. I need to teach Cassie how to abuse this little 'weakspot' of yours, but I know Jewel and Reyka "
						+ "have taken more than a few passes each at this tight ass already. Add Samantha's skills and Eve's 'endowment', and your slutty boyhole "
						+ "must be filled with cock more often than not. </i>\" Angel bursts into another peal of giggling at your expense, "
						+ "her thrusting slowing to an agonizingly sensual grind as a contemplative expression appears on her face. \"<i>Hmmm, "
						+ "with how much you like this...</i>\" Her eyes lock with yours, and a wicked grin appears on her lips as she leans in "
						+ "and salaciously purrs in your face. <p>\"<i>...I bet you're curious about taking a guy in here too.\" </i><p>"
						+ "She gives your butt a sharp slap and a hard blow of the strapon for emphasis, breaking whatever denial "
						+ "you may have given. Angel's slow grinding against your prostate has had your dick ready to burst for a while already, "
						+ "but she's taking her time to make you writhe under her as she goes on. \"<i>\"Oooh, I can see it already. The blissed "
						+ "out expression on your face as a man's cock ravishes you like the naughty whore you are, "
						+ "as it opens your ass and mind in one smooth thrust.</i>\" Angel moans softly and punctuates her point with a firm slam against your prostate. "
						+ "\"<i>Fufufufu. I can have that arranged, you know. My friends and I know a few boys who would love to introduce you to the pleasures men can provide you.</i>\" "
						+ "Angel's slow grind speeds up to a steady fucking as she continues tempting you with the idea, her dirty fantasies "
						+ "having a clear effect on her. Your mind goes numb as it considers the possibility as best it can with Angel fucking you like this. \"<i>Would you like that, "
						+ "my little slut? Would you like to rest your head against my chest and hold my hand as a man claims your asshole with his cock? Would you like me to "
						+ "teach you how to suck a thick, hard dick?</i>\" Angel leans in and runs her tongue along your neck. \"<i>Maybe we can take turns polishing the same one until it covers us both with cum, hm? "
						+ "The girls and I would love to cheer you on as you experience your first time exploring the same gender.\"</i><p> "
						+ "Angel's assfucking hits a fever pitch, causing that familiar pressure to rise in your groin again. Would you really feel good with another guy? You can't deny "
						+ "that something about the idea turns you on. Maybe it's how well she sells it, or maybe it's how good she's making you feel. "
						+ "<p>Or maybe you're just more like her than you realize.<p> "
						+ "\"<i>Consider it payback for all those threesomes we've had. I'd love to see you learn something about yourself. "
						+ "But for now...</i>\" A solid slap rings out against your butt. \"<i>Wrap those legs around me, slave. I'm going to teach you "
						+ "the feeling of being covered in cum.</i>\" You're too horny to even think about disobeying, and you wrap your legs around her waist, "
						+ "locking your calves behind her back. Angel leans further over you and pushes your hips up as she starts railing you mercilessly, each slam of "
						+ "her hips sending pleasure screaming through every nerve in your body. You cry out as you explode in orgasm, covering your face and chest in "
						+ "jets of your own semen. <p>"
						+ "Angel relents her railing and giggles softly, but not in a demeaning way. \"<i>That's a good look for you. Why don't I teach you one more thing?</i>\" "
						+ "she whispers as she leans down and licks up some of your cum before wrapping her arms around you and kissing you on the lips, gently yet passionately making out with you while sharing "
						+ "your cum between you. \"<i>I'm completely serious, lover.</i>\" She purrs between passes at your tongue, her rubber cock still "
						+ "moving gently inside you.\"<i>The girls and I would love to help you expand your horizons. Imagine how crazy our game nights could get with more boys, hm? Now...</i>\" "
						+ "Angel pulls away and puts a finger to your lips, leaving you with a mouthful of cum and a strand of saliva between you. \"<i>Swallow.</i>\" "
						+ "The surprisingly intimate experience entices you to without hesitating, eliciting another giggle and a look of... adoration? \"<i>How about it, lover? "
						+ "You don't need to give me your answer right now, and you don't need to say yes but...</i>\" She leaves one last kiss on your nose before disentangling herself from you. "
						+ "\"<i>It would make me really happy if you trusted me with that side of you. Just remember though.</i>\" Angel cups your face in her hands and stares into your eyes. "
						+ "<p>\"<i>You belong to me.</i>\"<p>"
						+ "And with that she stands up, takes your clothes, and walks away with a wink and a smile, leaving you breathless and with a nagging curiosity.";
			}
			else if(c.getOther(character).get(Attribute.Submissive)>=10 && Global.getValue(Flag.PlayerAssLosses)>=3 && Global.random(1)==0){
				Global.modCounter(Flag.PlayerAssLosses, 1);
				if(Global.getValue(Flag.AngelDomme) == 0)
					Global.modCounter(Flag.AngelDomme, 1);
				return "Angel gives your ass another firm blow of her strap on, making you squirm in a mix of pain and pleasure. "
						+ "You try to crawl away from her, increasing the distance between you and her cock, but she just pounces "
						+ "on top of you, pressing her large breasts against your back as she forces all her length into you and "
						+ "pins you down to the floor, with your dick sandwiched between the floor and your torso.<p>"
						+ "<i>\"Stop running away and enjoy it for once, you already lost, and I'm fucking you so often lately you'd "
						+ "be better off enjoying it.\"</i> She grins and starts to slowly remove her prick from your ass. "
						+ "<i>\"Just be the naughty bitch you are supposed to be.\"</i> Right after that she gives you another hard "
						+ "blow. Completely caught up by surprise, the pleasure that emerged from the cock that suddenly filled your "
						+ "hole has you moaning loudly for what feels like an eternity before reasoning comes back for you to cease "
						+ "it. You completely blush while, above you, Angel explodes in laughter with the shameful display you just "
						+ "gave her.<p>"
						+ "She leans forward and whispers to your ear. <i>\"That's it, I knew you loved it, I see it "
						+ "in the way you shiver every time I fuck you in the ass. I bet you don't even put up a real fight when I'm "
						+ "wearing this dangling cock, do you?\"</i> She laughs again, while taking her prick out of you. <i>\"Flip.\"</i> "
						+ "She commands. You do as you're told and turn to lay on your back. She holds both your legs up and works to "
						+ "penetrate you with her dick once again. "
						+ "<i>\"See, you don't need to be a pain in the ass, you can make things much easier by losing, I get to win "
						+ "the fight and you get to be my personal whore, It's good for everyone, don't you agree?\"</i> Maybe it's "
						+ "the numbing arousal, but you feel tempted to agree, the own thought of being dominated by an overwhelmingly "
						+ "gorgeous girl like Angel, the submissive side of you takes control and, without realizing you were at the "
						+ "edge of your climax, your own dirty thoughts have your cock twitching before shooting globs of semen all "
						+ "over your stomach.<p>"
						+ "Angel once again giggles at your pathetic inability to hold back your ejaculation. <i>\"I'll take that as "
						+ "a yes,\"</i> she says, sliding her finger through the semen in your belly. <i>\"If you are ever feeling "
						+ "like being my bitch again, we don't need to be at the games you know, I would take you like this anytime, "
						+ "just say the word.\"</i> Her finger now sliding through the cum on your chest. <i>\"Maybe I can even "
						+ "tell my friends about this and we can all humiliate you together.\"</i> She now takes her finger and "
						+ "slides it across your chin, reaching your lips and touching them with the traces of your own seed.<p>"
						+ "<i>\"Would you like that?\"</i> You shake your head in refusal. <i>\"Well, maybe I'll do it if you piss "
						+ "me off or something.\"</i> You can't tell if she's kidding or not. <i>\"Now lick it off, slave.\"</i> "
						+ "She says, forcing your own cum against your lips. At first you remain hesitant, but reminding the "
						+ "threat given to you just now you are quick to comply and lick her finger clean. <i>\"That's a good boy.\"</i> "
						+ "She stands up and leaves you on the floor, wasted and full of jizz. <i>\"Now go take a shower and clean "
						+ "this mess you made, and call me the next time you need a domme.\"</i>";
			}
			else {
				Global.modCounter(Flag.PlayerAssLosses, 1);
					return "Angel leans over you as she grinds her hips against yours. <i>\"You're going to come for me, aren't you?\"</i> she purrs into your ear. You shake your head; " +
							"no way could you live it down if you came while you had something in your ass. Angel frowns and gives your ass a firm slap. <i>\"No reach around for you. " +
							"then.\"</i> she snaps. <i>\"We'll just do this the old fashioned way.\"</i> She renews her assault on your poor ass and you feel your will slipping. Another solid slap " +
							"to your ass sends you into a shuddering orgasm. Angel's triumphant laughter rings in your head as the shame makes you flush bright read.<p>" +
							"Pulling her strapon " +
							"from your ass with a wet slurp Angel flips you over and removes the strapon.  She then squats down and lines your cock up with her now soaked pussy, <i>\"Do " +
							"a good enough good job and I might not tell my friends how you came like a whore while I fucked your ass.\"</i> She gloats with a smug grin on her face. " +
							"Appalled at the idea that she might share that information with anyone, you strengthen your resolve to fuck the woman above you.<p>Several minutes later, " +
							"you are breathing hard. Angel sits not far from you, face flush with pleasure. You smile internally as you sit, trying to catch your breath. No way " +
							"she could have been disappointed with that performance.  You can only gape as you look up to see Angel is gone along with your clothes. You sigh as you " +
							"stand and ready yourself to move on. You wouldn't put it past Angel to tell her girlfriends regardless of how well you performed, you just hope that's as " +
							"far as that information goes.";
			}
		}
		else if(c.stance.en==Stance.flying){
			return "<i>\"There's no point in resisting.\"</i> Angel says. <i>\"Not all the way up here.\"</i> Caught in Angel's grasp and flying high above the ground with your dick trapped in her gripping vagina with no way to escape, you had hoped that you might be able to outlast her. Judging from the confident smirk she's giving you, however, it's clear you're merely delaying the inevitable. A few more squeezes of her walls in time with the beats of her wings taking you higher and higher, and your resistance is completely eroded as you release a load of semen into the succubus' velvet depths.<br>" +
					"You groan in defeat, and Angel hums slightly as her smirk becomes a full grin. <i>\"Very good, lover. But we're not done.\"</i> She moves her hips, working at getting your cock hard again while the two of you float through the air. You risk a glance down, wondering how far up you are. Your stomach drops when you realize that you're a good distance above even the tallest building on campus.<br>" +
					"<i>\"Hmm?\"</i> Angel noticed your discomfort, and you suggest maybe returning to ground level before you continue. <i>\"Oh, don't worry about that, "+opponent.name()+". Do you really think I'd be fucking you in the air without knowing how to keep us up here?\"</i> You consider that for a moment, but are distracted from your thought when Angel wraps her legs around yours, giving her more leverage to squeeze your dick and force you back to full hardness. <i>\"Focus on me.\"</i> She orders. <i>\"You still have a job to finish.\"</i><br>" +
					"She starts moving up and down on your cock, and you marvel at the control she has to be able to do so while maintaining altitude. Deciding to trust her, you wrap your arms around her and start answering her movements with some of your own, hoping to bring her to orgasm and return to Terra Firma as soon as possible. <i>\"Mmm. Good job...\"</i> Angel makes her pleasure known as the two of you fuck in the night sky.<br>" +
					"Her mouth catches you in a kiss, her tongue masterfully controlling yours. The pleasure begins to overtake you as you both start to move faster, the breeze around you becoming stronger. Angel starts to moan... and you swear you just felt your altitude drop. In a panic, you manage to break the kiss and look down again. And you're absolutely certain that the ground is much closer than it was just a short time ago.<br>" +
					"Angel growls in a predatory fashion. <i>\"Didn't I tell you to focus on me? Come on, we're not finished!\"</i> You try to warn her about the height issue, but she immediately catches you in another kiss, much more forceful than the last. She far too focused on the sex to notice the danger, and you try to struggle against her. Caught in her grasp as you are, however, all you manage to do is thrust in and out of her even harder, heightening her pleasure. Her arms and legs tighten around you as she moans into your mouth, her breasts pressing into your chest. Deciding that your best bet at avoiding an accident is to bring her to orgasm as quickly as you can, you put everything you have into thrusting into her and rubbing against her body.<br>" +
					"A few minutes pass in this manner as the two of you lazily circle downward. You're very much aware of the ground getting slowly closer and Angel unknowingly forgoes altitude for pleasure. You wonder how in the world she has managed to not catch sight of the ground despite looking in its direction. Finally, the pleasure explodes from within you once more as you're brought to a second orgasm. Thankfully, this is enough to make her cum as well, and she moans deeply into your mouth one last time and her pussy milks you for everything you have.<br>" +
					"She finally breaks the kiss to look at you in surprise and approval. <i>\"Where did that enthusiasm come from?\"</i> You yell in terror about crashing, and Angel's eyes widen in shock as she finally notices the ground getting closer...as well as a building that you're headed straight towards. She pulls up and to the side, barely avoiding direct contact with the building. As it is, you can feel it brushing past your back, causing no physical harm, but shaving a few years off your life.<br>" +
					"Angel is able to regain control after that close call, and the two of you descend back to ground level. You collapse to your knees, kissing the ground in appreciation of its firm safety. You regain your wits after a moment, and you stand facing Angel. She's shuffling in place, looking incredibly embarrassed, a far cry from her usual in-charge attitude. <i>\"I'm...sorry, "+opponent.name()+".\"</i><br>" +
					"She seems unsure about what else to say, and you decide to assure her that you're not upset. She gives you a relieved smile, thankful for your works. <i>\"So...it felt good, right?\"</i> You certainly can't deny that. <i>\"Then maybe after I get a bit more practice in, we can do that again sometime?\"</i> You reply that you'll think about it. Her confidence restored, she reverts to her usual seductive grin and gives you a kiss before walking away the victor.<br>";
		}
		else if(flag==Result.facesitting){
			String spar_memory = "";
			if(opponent.has(Trait.disciplinarian)){//i.e. has seen the second sparring scene, therefore probably the first
				spar_memory = "You have a brief flashback to losing in this position before, with Angel's friends cheering on as you came helplessly. "
						+ "This time, though, it seems Angel plans to take her time. ";
			}
			return "You're not quite sure how you ended up in this position, but despite your free access to Angel's pussy, you seem to have lost. " +
					"Whenever you think you're close to making her cum, she lifts herself away and jerks you off a little more roughly, leaving you gasping in arousal. " +
					spar_memory +
					"After teasing your cock and occasionally tickling or squeezing your balls for a while, Angel lowers her pussy to your mouth again." +
					"<i>\"Lick,\"</i> she commands."; // TODO finish this
		}
		else if(flag==Result.intercourse){
			return "Angel rides your cock passionately, pushing you inevitably closer to ejaculation. Her hot pussy is wrapped around your shaft like... well, exactly " +
					"what it is. More importantly, she's a master with her hip movements and you've held out against her as long as you can. You can only hope her own orgasm is equally " +
					"imminent. <i>\"Not even close,\"</i> She practically growls. <i>\"Don't give up now.\"</i> That's an impossible command. How can she expect you not to cum when " +
					"her slick love canal is milking your dick so expertly. As the last of your restraint crumbles, you let out a groan and shoot a thick load of semen " +
					"into her depths. <p>"
					+ "You lie on the floor panting as Angel looks down at you, somehow annoyed despite her victory. <i>\"Is that the best you can do? " +
					"You know it's rude to finish before your lover.\"</i> She starts to lick and suck on her finger, sensually. <i>\"Don't think you can get off on your own and the " +
					"sex is done just like that. I never let a man go until I'm satisfied.\"</i> You're quite willing to try to satisfy her in a variety of ways, but more " +
					"fucking is a physical impossibility at this point. Your spent penis has completely wilted by now, and it'll be a little while before there's any possibility " +
					"of it recovering. Angel gives you a pitiless smile and reaches behind her. <i>\"Don't worry. I know a good trick.\"</i><p>" +
					"Whoa! You jerk in surprise as you feel " +
					"her spit-coated finger probing at your anus. <i>\"Don't complain,\"</i> She says, sliding the digit into your ass. <i>\"It's your own fault for being such a quick " +
					"shot.\"</i> As she moves her finger around, it creates an indescribable sensation. You dick starts to react immediately and returns to full mast faster than you " +
					"ever would have imagined. Angel wastes no time impaling herself on the you newly recovered member and rides you with renewed vigor. Fortunately she removes " +
					"the invading finger from your anus so you can focus on the pleasure of being back in her wonderful pussy. <p>"
					+ "She grinds against you, clearly turned on and " +
					"enjoying being filled again. She moans passionately and her vaginal walls rub and squeeze your cock. You move your hips to match Angel's movements and " +
					"her voice jumps in pitch. She's obviously enjoying your efforts much more this time, but she's so good too. You've just recently cum, but she's riding " +
					"through your endurance at an alarming rate. If you end up cumming again before she finishes, you're going to get the finger treatment again or worse. " +
					"Fortunately, you don't have to worry about that. Angel throws back her head and practically screams out her orgasm. Her love canal squeezes tightly, milking " +
					"out your second ejaculation. <p>"
					+ "Angel quickly recovers, standing up as a double load of cum leaks out between her thighs. <i>\"That'll do... for now.\"</i>";
		}
		else if(character.pet != null){
			return "Angel and her imp are a difficult team to beat - every time you shift your focus to one of them, the other finds a way to get one-up on you; kissing your neck, fondling your balls, and sucking your eager erection.  Their advances become increasingly more difficult to counter, and you quickly find yourself on all fours, at their mercy. <p>"
					+ "Dizzy with arousal and desperate for release, you are helpless as Angel stands in front of you, wearing a look of both compassion and pity, with her hands on her hips and her legs slightly apart.  Angel reaches down and gently puts a finger beneath your chin, lifting your face up and bringing it close to hers.  Without breaking eye contact, she raises your head until your hands come off the ground, and you are sitting upright on your knees.  With the glimmer of a predatory smile, Angel finally averts her gaze as she turns around and bends over, putting your face at the perfect height to lick her slit from behind.  She bends over further to grab your hands and pulls them forward, burying your face in her warm, inviting wetness.<p>"
					+ "Angel lets out a soft yet domineering moan as your tongue begins to explore her vulva, and for a moment you feel that you may still yet win this match, when you suddenly feel something warm and slippery begin to work itself around the tip of your penis.  With your face planted firmly in Angel's velvety lower lips, you didn't notice her imp positioning herself on her hands and knees on the ground in front of you, doggy-style.  Angel is still holding your hands, so there is nothing you can do as the imp backs herself into you, starting with shallow strokes, and slowly working herself further and further down your entire length until your whole member is tightly stuffed in her little pussy.  The imp reaches between her legs to massage your balls with one hand, and massages her clit with the other.<p>"
					+ "As you lick and suck Angel, your desire to give in becomes greater and greater, and your mind can't focus on anything besides your mounting need to cum.  The combined efforts of their fingers and tongues are finding all the sweetest spots in your most erogenous areas, and with a flurry of licks, squeezes and strokes you are brought to the brink of an explosive orgasm.  You feel a deep familiar pressure at the base of your dick, and your balls begin to contract in preparation for an enormous load. You have no choice but to give in to your urges and resign yourself to the relief of defeat, so you close your eyes and tilt your head back, anticipating the welcome release of a spectacular orgasm.<p> "
					+ "Angel suddenly jerks your hands forwards, pulling you off balance; now the only thing keeping you upright is your head being propped up by Angel's ass.  On cue, the imp hastily pulls herself off of you, and Angel, without turning around, says <i>\"Not so fast, boy.  Now you're going to have to earn it.\"</i>  Your balls lurch as the orgasm is snatched tantalizingly just out of reach, and with your entire manhood throbbing for release, you redouble your efforts on Angel's pussy; it has a sweet taste, and while you eagerly lap at her juices, you feel their intoxicating effects inch you closer and closer to orgasm.<p>"
					+ "You hear Angel's breathing quicken, and her imp kneels down in front of you, opening her small mouth wide and hungrily inhaling your cock until your glans bumps the back of her throat.  You can feel Angel begin to tense up - victory would be yours if you could just hold out for a few more seconds - but you can't contain yourself any longer, and your dick explodes with pleasurable release as you blow a hot, massive load down the imp's throat.  The imp swallows your load hungrily, rubbing herself to climax as she sucks every drop of cum from your glistening shaft.  Angel, her victory secured, follows your orgasm with her own, again pulling your arms forward as she bends over further, almost suffocating you in her dripping flower as she convulses with pleasure.<p>"
					+ "Angel's imp winks out of existence with a flash, and you and Angel are left alone, breathing heavily.  She lets go of your hands, so you take a minute to slump forward and catch your breath.  By the time you regain your composure, you see Angel walking off without so much as a good-bye.";
		}
		else if(character.has(Trait.succubus)&&opponent.is(Stsflag.horny)){
			return "Angel releases you from her grasp, her Lust Aura having done it's work. You collapse to the ground with the horniness she's inflicted on you, so desperate and sensitive that you're certain the slightest touch will make you cum. Angel watches you with smug satisfaction for just a moment before she places a foot on your throbbing shaft and gives it a single stroke. You cry out in pleasure as you erupt powerfully, shooting your seed while painting her sole and your belly white.<p>" +
					"Despite the blissful orgasm, your lust is not satiated. Angel removes her foot from your cock, allowing your erection to stand right back up. You moan luridly, needing more relief. “Oh my, what's this? Are you offering me more, Lover? How generous of you~” The succubus makes a show of slowly lowering herself between your legs, lying down so that she can place her bountiful breasts around your sensitive shaft. You moan again as you feel them, looking down to take in the attractive sight. Angel wastes little time in beginning her titfuck, the remnants of your previous ejaculation serving as lubricant. She goes slowly to ensure that you feel every little movement against your cock. The sensations are incredible...but it's not enough to satisfy your desperation.<p>" +
					"After suffering the torturous pleasure for a few moments, you begin bucking your hips, seeking another orgasm. <i>\"Oh? That eager to feed me? Well now, I certainly won't refuse that~\"</i> She speeds ups, bouncing her breasts in your lap and squeezing them tightly around you. You gasp at the feeling of your shaft being completely enveloped like this. Just when you think it can't get much better, Angel proves you wrong by taking your cockhead into her mouth. She sucks and licks slowly, never letting it escape as she continues squeezing your shaft between her tits. The pleasure nearly drives you mad, and you cry out for more. Angel giggles around your shaft, but acquiesces. Her oral ministrations grow more intense. Her tongue applies more pressure. Her sucking gets stronger. It all feels so good and you joyfully submit, shooting ropes of cum into Angel's mouth.<p>" +
					"She skillfully keeps most of it in her mouth. Only letting a little escape and dribble down from the corner of her lips. She savors the taste of the rest, slowly and methodically swallowing your load down bit by bit. Finally, she licks your cock clean before letting it slip out of her mouth. She grins. <i>\"You're still hard. Good~\"</i> You look down once more as the high from your orgasm comes to an end, shocked to realize that Angel is right. Despite those two great orgasms, your shaft stands proudly erect, ready for more.<p>" +
					"The succubus begins straddling your hips, slowly frotting her pussy lips against your sensitive shaft. You groan at the feeling, lust beginning to cloud your mind once more. <i>\"That was just the appetizer, Lover. You still owe me a main course~\"</i> With that declaration, Angel lifts her hips, and lowers herself back down to take your length. <p>" +
					"You spasm beneath her as her tight walls close around your shaft. She immediately begins squeezing and massaging your member without even moving. All the while staring down at you with amusement in her eyes as she pleasures you with minimal effort. <i>\"It's time for a feast~\"</i> She starts grinding your hips together, slowly forcing you deeper inside of her. You moan, grabbing her by the hips to try and pull her down further. She responds by pulling your hands away and pinning them at your side.<p>" +
					"With you immobilized, she starts bouncing. Up and down, Angel rides you, slowly getting faster and harder. It doesn't take long for your breathing to begin getting rougher, your oversensitive cock twitching within her folds as she brings you to the edge of your third orgasm. A small part of you wants to try to hold on. To make her work for her meal. Almost as if she could read your mind, she just gives you a smile. <i>\"Give me everything you have, Lover.\"</i> Her pussy tightens even further, strangling your shaft. You cry out in pleasure as your body submits yet again, spurting ropes of seed deep into the succubus' womb. You can feel her juices squirting around you shaft, signaling her climax as well. <p>" +
					"Drained of your seed and your energy, you lay limp under Angel's form. She takes her time lifting herself off of you, making a point to tease your body. Her pussy gives your softening cock a few more squeezes. She runs her breasts against your chest. Finally she leaves you there with nary a word, victorious and well fed.<p>" +
					"A few minutes later, you're able to return to the Games as well.<p>";
		}
		else if(character.has(Trait.succubus)&&Global.random(2)==0){
			c.getOther(character).add(new Drowsy(c.getOther(character),4));
			character.add(new Energized(character,10));
			return "You can't hold out against Angel anymore. You're practically drowning in her lusty aura as her hands and tongue pleasure your entire body. Her demonic transformation "
					+ "has turned her into the embodiment of pure sex. As you feel the pleasure overwhelm you, you find yourself wondering how you could hope to match a such an erotic "
					+ "and seductive being.<p>"
					+ "<i>\"Ah crap!\"</i><br>"
					+ "Angel lets out a less than dignified outburst as you ejaculate in her hands. <i>\"I was going to try to absorb some of that energy, but I missed the "
					+ "key moment. I haven't really got the hang of this yet.\"</i> She licks your cum off her hand with a slightly sulky expression. <i>\"You need to get "
					+ "hard so I can try again.\"</i><p>"
					+ "You find yourself chuckling, despite yourself. You weren't expecting her to let you go after a single orgasm, but that's not why you're laughing. Her "
					+ "clumsiness with her new powers is a cute contrast to her normal, self-assured attitude. She almost never admits a mistake in front of you. Anyway, "
					+ "you'll probably need a bit of help to get hard again so soon.<p>"
					+ "You barely finish the thought before Angel starts licking and sucking the semen off your soft cock. You let out a quiet groan at the pleasant sensation. "
					+ "She seems to really like the taste of cum. <i>\"That's a side-effect of the demon thing. Not that I ever minded the taste before.\"</i> She releases your "
					+ "rapidly hardening cock with an intentionally wet slurp. <i>\"But... as a succubus, I'm even more interested in tasting this with my other mouth.\"</i><p>"
					+ "She eagerly mounts you and slips you inside her hot, wet hole. You're hit by a jolt of pleasure as her vaginal walls expertly squeeze your cock. <i>\"Oh "
					+ "yes... I can definitely feel your pleasure this time.\"</i> You writhe in bliss as she energetically rides you. Her lust aura is radiating at full blast, and "
					+ "she's pushing you to orgasm unnaturally quickly. <i>\"Your spirit is leaking into me even before your jizz. How about I give it a little tug?\"</i><p>"
					+ "You back arches involuntarily as a wave of euphoria surges through you, headed straight for your groin. Your semen rushes into your urethra like it's "
					+ "being sucked out, but is halted by a sudden, agonizing tightness. Looking down, you see her devilish tail wrapped around the base of your penis, cutting "
					+ "off your ejaculation.<br>"
					+ "<i>\"That was perfect.\"</i> She smiles down at you, sadistically. <i>\"But I want to get some more practice. If I make another mistake, I might get laughed "
					+ "at again by a cheeky boy.\"</i><p>"
					+ "Oh shit. Angel is the queen of petty grudges. She immediately resumes riding your cock without giving you a chance to rest. You moan pathetically at the "
					+ "sensation. Her pussy feels amazing, but you are desperate for release. She moves her hips relentlessly, intent on driving you crazy. The sensation brings "
					+ "you to multiple mini-orgasms, but the lack of ejaculation just makes you more frustrated. Eventually, you break down and beg her to let you cum.<p>"
					+ "<i>\"Well, I came three times already, so I guess it's your turn. Let it all out!\"</i> She unwinds her tail and drops her hips, taking your full length "
					+ "inside her. Your long delayed ejaculation burst forth and your vision goes completely white. Angel screams in ecstasy as you fill her. Your mind is clouded "
					+ "by euphoria and exhaustion. You're sure you gave Angel more than just semen.<p>"
					+ "<i>\"Wow! I feel like a million bucks!\"</i> Angel basks in her stolen energy. <i>\"These succubus powers are awesome! Thanks for the delicious meal.\"</i> "
					+ "You groan as you try to shrug off your lethargy, but you don't seem to be dying. Fortunately, it appears Angel showed enough restraint not to take your "
					+ "entire soul. <i>\"Soul? I guess you could consider it the soul of a man.\"</i> She giggles in amusement. <i>\"Succubi just feed on sexual energy. I don't "
					+ "think I could seriously hurt you, even if I wanted to.\"</i> She lightly hops to her feet and gives you a dazzling smile. <i>\"Maybe you should rest a little "
					+ "longer. I want those balls nice and full before we meet again.\"</i>";
		}
		else{
			if(Global.random(2)==0){
						return "You collapse to the floor, unable to resist Angel's increasingly pleasurable advances. She casually strolls up to you, before planting the sole of her foot onto your engorged shaft. The sudden stimulation draws a gasp from you, the strong stimulation nearly sending you over the edge in a single motion. <i>\"What's wrong lover, aren't you going to try and resist? Not that I mind an easy target, but it's in your best interest.\"</i> <p>" +
								"She's right. Mustering up the last of your strength, you try to pull yourself up... only to have Angel step down hard on your chest, pushing you back down. She flashes you a domineering smile, before starting to rapidly rub her soft sole against your cock. You can only moan as her assault continues on, pushing you to and then over the edge without a moment's hesitation. Thick semen splatters across your stomach, and Angel lets out a sinister giggle. <p>" +
								"<i>\"Well, that's the match "+opponent.name()+". I'm certain you know I'm not through with you just yet though...\"</i> You weakly ask what she's planning to do with you. <i>\"Simple; I'm not moving on to my next fight without getting off, and you're going to help me.\"</i> She positions herself above your head and sits down, burying your face underneath her plump rear. <p>" +
								"Well, she did win. If that's what she wants, you have no problem helping her out. After giving her a moment to get properly situated, you begin. You make a slow and shallow lick across her slit, savoring the taste of her nectar before licking again. You can hear Angel let out a soft sigh of contentment, and she grinds herself against your mouth a bit more. <p>" +
								"<i>\"Good... keep on licking Lover. In the meantime, I'll treat you to something nice.\"</i> She entraps your sensitive dick between her feet, and starts to slowly stroke. Her toes curl around your cockhead, squeezing gently before resuming. You let out a muffled moan into her slit, and start to make deeper licks. Your tongue easily parts her folds and rubs against her tight walls, prompting a barely restrained moan from her.<p>" +
								"Angel begins to alternate her motions, switching back and forth between slow and rapid strokes. Your first orgasm and a growing amount of pre serve as a lubricant, making each motion feel slippery and warm. You're starting to feel a little dizzy, not from the stimulation itself, but from only being able to breathe in air mixed with the smell of her sex. <p>" +
								"Through this pleasurable haze you do your best to keep on licking and sucking, doing everything in your power to bring her to a height before you reach your second. Despite having only ejaculated less than a minute ago, your shaft is completely erect with another load slowly but surely building. <p>" +
								"Angel doesn't let up for an instant, squeezing and kneading your cock mercilessly. It's like a feedback loop of pleasure: Whenever you please the domineering girl above you she increases the intensity of her footjob, causing you to gasp and dig deeper into her flower. <p>" +
								"All at once, her assault comes to a screeching halt. <i>\"You're swelling up a lot, but it would be a shame for you to cum so early. Keep going, and earn it~\"</i> Frustrated by the denied release, your redouble your efforts. Nibbling her clit, licking her slit, and piercing further into her depths. <p>" +
								"It's not long before your efforts are rewarded. While she might talk a big game, her sexual stamina is far from limitless. Her resistance crumbles under your unyielding assault, and she cries out as an orgasm rocks her body-- covering your face with even more of her juices. <p>" +
								"<i>\"Wonderful job Lover. As promised, here's your reward.\"</i> Angel snares your cock between her lubricated soles once more, and begins to feverishly rub them together. She alternates sides, squeezes, and teases your neglected member, quickly bringing you back to the edge. All you can do is moan into her as the ministrations bring you to a blissful release. Thick strands of pent-up cum shoot out, briefly flying upwards before raining back down upon Angel' feet and your stomach. <p>" +
								"The victorious party finally dismounts, giving you a smug smile in the process. <i>\"It certainly seems like you enjoyed yourself. Be careful Lover, I might not be so merciful next time.\"</i> She walks away, leaving you to catch your breath and prepare yourself before heading back to the Games.";
					}else{
				if(opponent.human()) {
							Global.gui().displayImage("premium/Angel Faceride.jpg", "Art by AimlessArt");
				}
				return "It's too much. You can't focus on the fight with the wonderful sensations Angel is giving you. She smiles triumphantly and mercilessly teases your " +
						"twitching dick. Your orgasm is imminent, but you concentrate on holding it back as long as you can, determined not to give up until the end. Angel's " +
						"expression gradually changes to one of impatience. <i>\"Just cum already!\"</i> She slaps your dick and the shock breaks your concentration. Your pent-up " +
						"ejaculation bursts forth and covers her hands. <p>"
						+ "Without giving you a chance to recover, Angel pushes you on your back and positions her soaking " +
						"pussy over your face. <i>\"Show me you're good for more than cumming on command.\"</i> She grinds against your mouth as you eat her out. She reaches behind her " +
						"and roughly grabs your balls, encouraging you to focus more on pleasing her.<p>"
						+"Soon her writhing grows more passionate and her moans express her building " +
						"pleasure. She rewards your efforts by moving her hand to your dick, which is already starting to harden again. She jerks you off, using your previous climax " +
						"for lubricant. The growing volume of Angel's cries reveal that she's close to the end, so you focus on licking and sucking her clit, quickly bringing her to " +
						"a loud climax. Your own peak isn't far behind and soon you shoot another jet of cum into the air.<p>"
						+ "Angel licks her semen-covered hands clean and walks away with your clothes, having seemingly forgotten about you.";
			}
		}
	}

	@Override
	public String defeat(Combat c,Result flag) {
		Character opponent=c.getOther(character);
		declareGrudge(opponent,c);
		if(flag==Result.anal){
			return "Angel can be a mean winner. She can be mean when you draw with her. She can even be mean when you defeat her. So when the match is going well enough for you that you have the flexibility to give her a little taste of her own medicine, you decide to go for it.<br>" +
					"You really shouldn't have expected it would be that easy.<br>" +
					"At first, things seemed to be going well for your plan. When you lubricated Angel and teased her ass with your cock, her body had frozen in place out of fear. And when you'd forced your cock into her ass, she gave out a delightful groan from the pain. But then it only took a few thrusts before she started humming in pleasure from it. You were hoping to be able to degrade her a bit, mock her for getting fucked in the ass, and then make her cum from it to top it off. But that wouldn't work nearly as well with her enjoying it this much.<br>" +
					"As you're trying to figure out how best to salvage your plan, Angel throws another wrench into the works. <i>\"Mmm... I surrender...\"</i> she says.<br>" +
					"You blink in surprise as you process this. She... surrenders? Is that even allowed? Your mind goes back through the rules of the Game, but you don't remember hearing anything about surrendering. She could use a safe word, but that was different. So without any rule allowing it, it's certainly not official, but she can just choose to stop fighting back if she wants.<br>" +
					"Angel takes advantage of your confusion to pull herself forward, your dick slipping out of her ass. She rolls over onto her back and reaches down to her knees, pulling them both up so that you have access to her asshole again. <i>\"Don't worry, no tricks. Come, get back in there and finish me off. I just wanted to be able to look into your eyes as I cum.\"</i><br>" +
					"You look down at her, growling a bit at how well she's spoiled your plans. You move down, sliding your cock against the length of her slit and then to her asshole, toying with it as you figure out what to do now. Finally, you decide on your course of action. You look into Angel's eyes as you tell her that it's not a good idea to surrender without negotiating terms first. She just gave you an unconditional surrender, which means you get to do whatever you want with her.<br>" +
					"A hint of nervousness appears in Angel's expression as it dawns on her that she might not be in complete control of this situation. <i>\"Well yeah, but... that's after you finish me off like this,\"</i> she says, giving you a confident grin as she tries to exert control once more. <i>\"You get to fuck my ass, and I get to look into your lovely eyes. And then you get whatever you want as a reward.\"</i><br>" +
					"It's hard to resist her. A big part of you wants to just give in and give her what she wants, but you're not going to let her win this easily - even if it isn't technically winning the match, it would still be a personal win for her. But fucking her ass in this position is certainly tempting.<br>" +
					"A lightbulb clicks on in your head as you figure out what to do. You tell Angel to stay in that position while you reach over to her supplies. Finding a couple zipties in there that she'd been saving, you borrow them for your own use. You use the zipties to fasten her wrists to her ankles, keeping her trapped in this exposed position. This doesn't seem to put Angel off - she simply hums in excitement - but this wasn't the key part of your plan. Going back to her belongings, you pull out her strap-on and hold it up to her face.<br>" +
					"You know quite well what she'd gotten this for. It might be useful for other girls, but it was also for your asshole. Not today, though. Today her own ass was going to be subject to it.<br>" +
					"You lean back, working to detach the dildo from the strap-on's harness. As you do this, Angel looks at you in confusion. <i>\"I was hoping you'd fuck my ass with your cock again... but if you want to use that instead, I guess you can.\"</i><br>" +
					"Instead? It wasn't your plan to use it instead of your cock. It was going in alongside your cock.<br>" +
					"Angel's eyes widen as you say this. <i>\"Wait,\"</i> she says, fear filling her features.<br>" +
					"You don't wait. You take a bottle of lube from Angel's supplies and open it up, making sure both the dildo and your cock are well-lubricated. You're going to need it for this. Once they're ready, you start with the dildo, poking it at Angel's asshole and slowly forcing it in.<br>" +
					"<i>\"Wait!\"</i> Angel says again, a bit more frantic this time. <i>\"There's no way they can both fit in my ass. Your cock was already a stretch.\"</i><br>" +
					"Well, you won't know for sure until you try. Her ass is certainly stretching enough right now to accept the dildo. You're sure it can stretch enough for your cock as well, with a bit of work. You move the dildo around within Angel's ass, making sure she can feel it hit every wall inside of her. She whimpers a bit from this attention, and she looks up at you with big doe eyes.<br>" +
					"You try to push one of your fingers into Angel's ass alongside the dildo, but it's too tight to make much progress. Angel squeezes her eyes shut, letting out a soft groan. You ask her how it feels, earning a reply of, <i>\"The dildo alone is fine, but your finger is too much,\"</i> from her. <i>\"I really can't take anymore. My ass is at its limit. Please... just take the dildo out and fuck me with your cock. I'll let you do anything you want.\"</i><br>" +
					"From your prodding, it feels like Angel is right: There's no way you'll be able to get your cock in as well if you can't even get a finger in. You seem to have found her limit here, both physically and mentally. Even if she could physically take more, you don't want to push her to the point where she has to use her safe word. You pull your finger back and hold the dildo again, slowly moving it around inside Angel to give her a bit of pleasure as an apology for the pain.<br>" +
					"As the threat of being stretched beyond her limit subsides, you can see Angel beginning to enjoy herself again. You lean down and place a gentle kiss on her lips, letting her know without words that she's off the hook. Angel hums in response, closing her eyes as she simply enjoys the feeling of the dildo in her ass.<br>" +
					"You pick up the pace of the dildo, soon pounding it rapidly into Angel's ass. She might have said she surrendered, but without any rule to officially allow that, you do have to make sure she cums first. Once you're sure she's far enough ahead that you don't have anything to worry about, you pull the dildo out of her ass and replace it with her cock. <br>" +
					"<i>\"Oh yes... yes... fuck my ass...\"</i> Angel says, the feeling of your cock inside her causing her to go into a horny haze now. You reach up to her nipple, giving it a good twist to help with the mix of pain and pleasure she's experiencing right now. The little bit of extra pain seems to be just what she needed, as Angel lets out a loud moan in response.<br>" +
					"Angel keeps pleading with you to fuck her, but her words slowly fade into meaninglessness. As her orgasm nears though, she still has the presence of mind to look up at you now, just as she wanted to. She holds your gaze as an orgasm slowly overtakes her body, shudders spreading up from her feet until finally causing her to roll back her head and let out a deep moan.<br>" +
					"With Angel satisfied, you let yourself relax so you can finish yourself off as well. Angel hums as you continue to pound her ass, and she lets out a deep moan as you explode into her, painting her insides white with your seed.<br>" +
					"As you recover from your orgasm, you grin down at Angel. This certainly didn't go too badly in the end.<br>" +
					"<i>\"Hmm...\"</i> Angel says. Her eyes lazily open and she looks up at you with a smile. <i>\"You'd better not wait too long before cashing in that ‘anything you want to do me,' okay? A girl's got needs, after all.\"</i><br>" +
					"You snort and shake your head. She's off the hook on that one. That is, unless she doesn't want to be.<br>" +
					"<i>\"Well...\"</i> Angel says, her eyes narrowing at you as she gives you a mischievous smile. <i>\"Let's just say, anything you'd be fine with me doing to you, because I just might.\"</i><br>" +
					"She almost certainly will, but if you play your cards right, that just means double the fun. You smile back at Angel and lean down to give her one more kiss.<br>";
		}
		else if(flag==Result.intercourse){
			return "You thrust your cock continuously into Angel's dripping pussy. Her hot insides feel amazing, but you're sure you have enough of an advantage to risk " +
					"it. She lets out breathy moans in time to your thrusts and her arms are trembling too much to hold herself up. She's clearly about to cum, you just " +
					"need to push her over the edge. You maul her soft, heavy boobs and suck on her neck. Angel closes her eyes tightly and whimpers in pleasure. <p>You keep " +
					"going, sure that your victory is near, but after awhile there's no change in her reactions.<p>"
					+ "How has she not cum yet? She's obviously loving your efforts, " +
					"but you can't seem to finish her off. Worse yet, if you keep going at this pace, your own control is going to give out. You'll have to pull out so you can " +
					"switch to your fingers and tongue. It'd be way more satisfying to win by fucking her, but right now you just have to focus on winning at all.<p>"
					+ "When you try " +
					"to pull out, Angel's legs wrap around you and keep you from escaping. Her heels jab you in the butt, forcing you to thrust back inside and you feel her pussy " +
					"squeeze your cock tightly. <p>"
					+ "Oh God, she's actually going to make you cum while you're on top of her. You were overconfident in your dominant position, you " +
					"underestimated Angel's remarkable staying power, and now you've lost. Despite your desperate attempts to hang on, you're overwhelmed by pleasure and cum " +
					"inside her tight womanhood. You slump down on top of her as you both catch your breath.<p>"
					+ "Pretty soon Angel is fully recovered and back on her feet, but you " +
					"continue to lie on the floor, too dispirited to move. Angel gives you a sharp prod with her foot. "
					+ "<p><i>\"How long are you going to lay there? You only came once. " +
					"I had a continuous orgasm for at least two minutes and that's way more exhausting. It's been a long time since anyone's made me do that.\"</i> Wait, what? You'd " +
					"never have guessed that she came if she hadn't said anything. <p>"
					+ "<i>\"Just because you managed to beat me this time doesn't mean you can suddenly start acting " +
					"lazy. If you let your guard down, I'm going to turn you into my own personal toy.\"</i> At that, she walks away naked.";
		}
		else if(opponent.pet!=null && opponent.pet.type() == Ptype.impmale){
			return "Having a pet to assist you in combat can make all the difference. While Angel is occupied with defending herself from your imp's lecherous assault, you're able to safely grab a zip tie from your supplies and bind her hands behind her back. Left with only her legs to fend off your imp, Angel is left nearly defenseless against your own assault on her.<p>" +
					"You place a hand on each of Angel's breasts, then lean down to capture her lips in yours. She tries to roll away at first, but you slowly manage to make her melt into your kiss. She's too far gone to resist now; all you have to do is finish the job. You slowly pull back from the kiss, waiting until Angel makes eye contact with you, and then ask her how she wants you to finish her off.<p>" +
					"<i>\"Hmm...\"</i> Angel says. A smile slowly forms on her face. <i>\"Your cock feels like it's ready to burst. Maybe if you cum inside me, that'll be just enough to push me over the edge too.\"<p>" +
					"Your cock? You blink in confusion as Angel says this. She isn't touching your cock in any way right now, so how could she think it's ready to burst? Then it hits you: It's not your cock she's feeling in her pussy right now: It's your imp's. You return Angel's smile with a wicked grin of your own. You whisper to her, telling her you're just about ready to cum, but only if she promises to cum as well.<p>" +
					"Angel hums in response, and you reward her by leaning down and capturing her lips in a kiss once more. All through the kiss, you continue to play with her breasts, making sure she's ready to cum as soon as your imp does. And judging by the sounds both of them are making right now, that's no worry at all. It's only a matter of moments until you hear both of them scream out in ecstasy as they reach a mutual orgasm.<p>" +
					"The imp pops out of existence as soon as he cums, leaving Angel's pussy suddenly devoid of his cock. Her eyes open in confusion, as her post-orgasmic brain tries to figure out just what happened. She turns her head away from your kiss as she says, <i>\"Wha- where did your cock go? And what was that...\"</i> Angel blinks slowly as her brain puts the pieces together. Slowly, a fierce blush fills her face.<p>" +
					"Suddenly, you hear her snap the zip tie that was binding her wrists. She pushes you away and brings her hands to cover her face. You reach a hand out to her, apologetically placing it on her shoulder. At first, she shrugs it away, but then she seems to have second thoughts, bringing her shoulder back and letting you console her a bit.<p>" +
					"<i>\"Fuck... I can't believe I just had a mutual orgasm with that fucking thing...\"</i> Angel says through her hands. <i>\"Ugh, I can even feel its cum still inside me. I'm going to have to head straight for the showers after this...\"<p>" +
					"You grimace at this, apologizing to Angel for defeating her this way, but she shakes her head. Even as she does this, she still shields her face with her hands.<p>" +
					"<i>\"Haa... no, it's fine. All's fair in the Games... It's my own stupid fault for forgetting about it, anyway,\"</i> Angel says. Slowly, the blush in her face seems to subside, and she lowers her hands. You worry for a moment that she might be out for revenge now, but thankfully her expression doesn't appear to be angry. <i>\"Alright, "+opponent.name()+". What's it going to take to make sure you never tell a single other person about what just happened?\"<p>" +
					"You blink, caught off-guard for a moment by the offer. It's a bit hard to figure out just what to ask for. You don't want to waste such an offer, but you also don't want to ask for too much and end up getting rejected, so you have to...<p>" +
					"<i>\"Haa... How about we start with a mind-blowing blowjob, and go from there?\"</i> Angel says. You focus on her face again, once more seeing the confident Angel you're used to. Your cock throbs for a moment, telling you that this does indeed sound like a good idea, so you smile at Angel and agree... agree that it's a start, at least.<p>" +
					"Angel purrs in response. You adjust your position, allowing her to crawl up to your lap. She wraps a hand around your rock-hard cock, stroking it for just a moment before she leans down to bring her mouth to it. She brings her hand up to keep her hair out of her face as she begins to lick her tongue around your cock. Slowly, every square inch of it is caressed by her tongue, until at last she places her lips on the head of your cock.<p>" +
					"Angel looks up at you for a moment. Holding your gaze, she slowly descends, taking your cock inch by inch into her mouth. As her lips slide down your shaft, her tongue circles around it, causing you to roll back your head as you succumb to the heavenly sensations. With no match to worry about losing, you surrender yourself to Angel's expert blowjob, letting your orgasm approach without a fight.<p>" +
					"As you feel your climax nearing, you warn Angel, giving her a chance to pull back if she wants to. Instead, she plunges her head as far down as she can, letting you explode into her throat. Somehow, Angel manages to take your cum without coughing on it, and she's even able to retain enough control to suck on your cock, draining the last few drops of cum from it before she finally swallows.<p>" +
					"Angel pulls back, a smile on her face. She sits up, then leans in to plant a quick kiss on your lips. <i>\"Was that good enough to make sure you never tell anyone what just happened?\"<p>" +
					"You blink in mock confusion. Why would she not want you to tell people how good she is at giving blowjobs?<p>" +
					"Angel chuckles, then leans in to plant a kiss on your cheek. <i>\"Perfect.\"<p>";
		}
		else if(character.is(Stsflag.masochism)){
			return "The rougher you have been with Angel, the more she has liked it.  She doesn't even try to stop you when you wind up to smack her, dropping her guard as you slap your hand firmly across her breasts.  Her tits shake and wobble against the impact of the blow, and she softly whimpers in both pain and pleasure.  Clutching at her injured tits, she can't stop you from reaching down and pinching her clit, squeezing it between your thumb and forefinger.<p>" +
					"Angel's moans immediately intensify, and when she takes one of her hands off her tits to weakly resist your grasp, you take the opportunity to grab her exposed nipple in your other hand, twisting it slowly as your fingers dig into her flesh.  Her moaning gets louder still as you antagonize her most sensitive areas, and she becomes so well-lubricated that it is difficult to maintain a firm hold on her love button.  On the verge of ecstasy, Angel is panting and sweating, practically begging for release; you let go of her lady bits and promptly slam your knee directly upwards into her groin, smashing her vulva and clit against her pubic bone.  The hit was exactly the thing Angel needed to drive her into a wild, screaming orgasm.<p>" +
					"Angel's legs buckle and she collapses onto her knees, holding her pussy in both hands with her face on the ground in front of her.  You watch for almost a full minute as she wails and convulses on the floor, wiggling her ass and pussy in the air behind her.  The fight itself was certainly arousing, and you realize you are rock hard as you watch Angel squeal on the ground in front of you.  Seizing your victory, you kneel down behind her, grab her hips, and slowly enter her ample wetness with your throbbing erection.  She lets out a gasp of surprise as your glans effortlessly slides between her petals, but she doesn't protest, getting up on all fours and backing herself onto you. <p>" +
					"As you slowly inch further inside of her, you move your hands up her torso and cup her breasts.  Her nipples are still fully erect, and you grab one in each of your hands, again pinching and twisting them.  You use Angel's tender nipples to pull her closer to you as you penetrate her; you can feel her contracting around you as she whimpers in rhythm with your torment.<p>" +
					"Your steady, measured strokes quickly become faster, longer thrusts, and you bury yourself as deeply into Angel as you can while she squirms in your grip.  Your lovemaking intensifies further, and soon you are giving her a real pounding, jackhammering yourself into her as you both clutch at each other and moan in pleasure.  By the time you approach climax, you are fucking her so hard that the whole campus can probably hear the sound of her ass smacking against your crotch.  Aggressively yanking her nipples towards you, you insert yourself completely into Angel and explode inside of her, and she cums as well; the two of you grind against each other, spasming in unison.  You feel your balls emptying themselves entirely as you release a hot, copious load inside of her.<p>" +
					"You remain inside of her for a few moments until your dick stops twitching, and you finally release her nipples.  Still gasping for air, Angel looks out of breath, sliding off of your cock and onto the floor; she runs her hands up and down her body, releasing one last tremble of pure pleasure.  She almost looks disappointed when you stand up to leave, giving you a come-hither look while spread-eagle on the floor - Angel has always been insatiable.<p>" +
					"You stand over her for a moment, between her legs; Angel is gyrating her hips and massaging her breasts, trying to seduce you into another round.  You would love to, but you are also eager to get back to the games and continue your win streak, so you figure out a compromise.  You plant your foot firmly on Angel's flower, with her clit under your toes.  Angel gasps in surprised ecstasy, her gasp quickly giving way to an ecstatic moan as you apply pressure to your foot, and Angel writhes in approval.  She arches her back, closes her eyes, squeezes her breasts and opens her mouth wide, and you realize just how easy it would be for you to give her another orgasm.<p>" +
					"With a sharp twist of your ankle, you grind the balls of your foot into Angel's little bean.  She screams in pleasure, bringing her knees to her chest and covering her privates with her hands as you withdraw your foot.  She rolls onto her side while as her whole body is wracked by another orgasm, and you silently slip back out into the games before she returns to her senses. <p>";
		}
		else if(character.has(Trait.succubus)&&character.get(Attribute.Dark)>=6&&Global.random(2)==0){
			return "Angel shivers as she approaches her climax and her legs fall open defenselessly. You can't resist taking advantage of this opening to deliver the " +
					"coup de grace. You grab hold of her thighs and run your tongue across her wet pussy. Her love juice is surprisingly sweet and almost intoxicating, " +
					"but you stay focused on your goal. You ravage her vulnerable love button with your tongue and a flood of tasty wetness hits you as she cums. You " +
					"prolong her climax by continuing to lick her while lapping up as much of her love juice as you can. The taste seems almost familiar, but you can't " +
					"quite place it. Sweet and tangy like a dessert wine? Not a perfect comparison, but not far off.<p>" +
					"Angel should be coming down from her peak, but " +
					"she's still moaning quite passionately. Oh well, it can't hurt to drink up the last of her love juice. You're the one who made her juice herself, so " +
					"it seems only fair. It is very tasty. Intoxicating was the word that came to mind earlier, but addictive seems to fit too. Angel's flower is mostly " +
					"clean, but you stick your tongue deep inside to be sure. There seems to be some fresh love juice in this bit... and this one.... Here too.<p>" +
					"Angel's " +
					"pussy tenses up and you're treated to another flood of her wonderful flavor. You can't let this much juice go to waste. You diligently continue to " +
					"lick Angel's trembling girl parts as she squeals in passion. You feel her hands grip your hair desperately and you have to hold her hips to keep her " +
					"from squirming away. She's producing a decent amount of delicious nectar, but it occurs to you that she'll probably give you more if you focus on her " +
					"clit. You target her pearl and lick it rapidly until she screams in pleasure and rewards you with another surge of juice. This seems like the best " +
					"way to get more of her wonderful juice. You could just stay here drinking this stuff all night, and you just may.<p>" +
					"You suddenly feel Angel's tail wrap " +
					"tightly around your balls. Your head jerks up in surprise and her thighs clamp together on it, holding you out of reach of her delicious honey pot. " +
					"<i>\"Down boy!\"</i> Angel scolds you as she covers her groin protectively. <i>\"I appreciate the dedication, but after a couple orgasms, I need a chance to " +
					"catch my breath.\"</i> You feel your head clear a bit and realize you completely fell victim to her addictive love juice.<p>" +
					"Angel uses her grip on your " +
					"head to force you onto your back. <i>\"I do love being eaten out, but right now I'm ready to be filled.\"</i> She releases the head scissor and positions herself " +
					"over your dick before dropping her hips to engulf you to the hilt. A jolt goes through you and you realize exactly how horny you are. I addition to " +
					"not having any relief, Angel's fluids have started to affect you. You're incredibly hard and sensitive, but even though Angel is riding you intensely, " +
					"your ejaculation feels painfully out of reach. You don't feel your climax start to build until Angel is moaning and approaching yet another orgasm. Is " +
					"that an innate succubus ability? Is she controlling the timing of your orgasm? You don't have time to dwell on the question, your hips thrust involuntarily " +
					"as you shoot your load into her waiting quim. Angel gives you a deep, passionate kiss as she gets off of you. <i>\"Thanks lover. You sure know how to show a " +
					"girl a good time.\"</i>";
		}
		if(Global.random(2)==0){
			return "You can tell that Angel is close to cumming and press your advantage. She gives a small <i>\"ah!?\"</i> in surprise as you grab her and hold her tight, bringing your leg between her thighs and rubbing against her pussy. She gasps and attempts to disengage, but the arousal you've already forced on her gets the best of her. Her struggles turn to acceptance after only a few moments, and she hugs you tightly while moaning in pleasure. You rest your head between her large breasts, enjoying their incredible softness while you work her towards orgasm in comfort.<p>" +
					"It doesn't take much longer before Angel shudders in your grasp, groaning out as she cums all over your leg. You don't stop quite yet though, continuing to extend her orgasm for just a little longer. Soon enough though, you let her out of your grasp. Breathing heavily, she sits down to recover. You take the opportunity to try and catch your breath as well. Angel speaks after a few moments of rest. <i>\"I didn't think you could make me cum with just foreplay.\"</i> She's still gasping slightly, but she gazes at you with an extra bit of respect in her eyes. <i>\"I'll admit, I'm impressed.\"</i> Then her expression becomes the usual lewd look you expect from Angel as she stares at your erect dick. <i>\"Though it looks like you're still pretty needy yourself~ would you like some help with that?\"</i><p>" +
					"You most certainly would. You look over Angel's body, trying to decide how to ask her to get you off. It doesn't take long for your gaze to settle on her breasts. Her large, soft, incredible breasts where you had been resting your head only a few minutes ago. She smirks as she sees where you're looking, and reaches up to play with them a bit. She gropes and squeezes, pinching her own nipples and pushing them together for your viewing pleasure. <i>\"If you want to feel these around your dick, just say so~\"</i><p>" +
					"Not one to pass up such an offer, you ask her to lie down. Her brows raise, seeming a bit surprised by your request, but her smirk never leaves her face as she lies on her back. Knowing what you have in mind, she holds her breasts apart in an invitation to slide between them. You take her up on the offer, straddling her chest with your knees on either side of her, being careful not to put too much weight on her. Once you're comfortable, you check to make sure Angel's ready. She just gives you a seductive smile. <i>\"Go on. Fuck my tits~\"</i> And you do, thrusting into the valley between her breasts as she squeezes them together, enveloping you in their incredible softness. You moan loudly at the sensation before you start moving back and forth, thrusting in and out of her cleavage. Angel facilitates the titfuck, moving her breasts in time with your motions, squeezing them together at just the right moments to make sure you feel it most. <p>" +
					"Your lust soon gets the better of you as you reach down and bat Angel's hands aside, grabbing her breasts yourself as you take full control. <i>\"Oh~? How eager of you, "+opponent.name()+". Go right ahead then. Just make sure you give me everything you have~\"</i> She takes on a more passive role, making herself comfortable as you massage and grope her breasts while thrusting between them. She moans quietly as you continue, clearly enjoying it as much as you. Aroused as you are, it doesn't take long for you to start getting close. You close your eyes as you focus on the pleasure, squeezing her tits around your shaft.<p>" +
					"Finally, you moan loudly as you cum, shooting ropes of semen into Angel's cleavage. A few spurts manage to go further, landing on her face. You take a moment to rest, and she speaks up as you lean back to recover. <i>\"Enjoy yourself, Lover? You certainly gave me a lot.\"</i> She runs a finger through the mess you left behind, then brings it to her mouth for a taste. The sight is rather arousing, and you have to make yourself stand up to resist the urge to continue. The smirk on her face suggests she's quite amused by this. <p>" +
					"She poses attractively on the ground with one hand behind her head and the other arm slung under her breasts, giving you a good view of your cum on her face and chest. <i>\"Like what you see?\"</i> You admit that you do. Quite a lot, in fact. She seems pleased by your answer. <i>\"Good. Enjoy it while you can, because I'm going to win next time. And when I do, my breasts are going to wring out every. Last. Drop. Of your cum.\"</i> With a grin, you tell her that you look forward to seeing her try. With a wave over your shoulder, you return to the Games.<p>";
		}
		return "Angel trembles and moans as you guide her closer and closer to orgasm. You pump two fingers in and out of her pussy and lick her sensitive nether lips. " +
				"Her swollen clit peeks out from under its hood and you pinch it gently between your teeth. Angel instantly screams in pleasure and arches her back. A " +
				"flood of feminine juice sprays you as she loses control of her body.<p>It takes her a little while to catch her breath. She quickly pushes you on your " +
				"back and begins blowing you, never once meeting your eyes. What you can see of her face and ears is completely red. If you didn't know better, you'd " +
				"say that she's embarrassed about the one-sided orgasm you gave her earlier. You don't have much attention to devote to it though, Angel is a very good " +
				"cock-sucker. Her tongue finds all your most sensitive areas and soon you're filling her mouth with your seed.<p>Angel swallows your load and happily " +
				"licks the stray drops from her lips. <i>\"Did you enjoy that?\"</i> She asks, looking a lot more composed. <i>\"You weren't bad either.\"</i>";
	}

	@Override
	public String describe() {
		if(character.has(Trait.succubus)){
			return "Angel seems to have taken the path opposite her namesake. She has wings, but they're black as midnight. Small horns are visible through her hair and " +
					"a demonic tail sways lazily behind her. Her appearance should be frightening, but she's more beautiful and seductive than ever. Her entire being seems to " +
					"radiate sex and you struggle to ignore a treacherous little voice in the back of your mind that tells you to just give yourself to her.";
		}
		else{
			return "Angel has long, straight blonde hair that almost reaches her waist. She has a model's body: tall and very curvy, with impressively large breasts. " +
					"Beautiful, refined features complete the set, making her utterly irresistible. Her personality is prideful and overbearing, as though you belong to " +
					"her, but you don't know it yet.";
		}
	}

	@Override
	public String draw(Combat c,Result flag) {
		Character opponent=c.getOther(character);
		character.clearGrudge(opponent);
		if(character.has(Trait.succubus) && Global.random(2)==0){
			return "You think Angel's gotten better at using her succubus powers. Having gotten more used to the concept of magical and demonic powers in the Games, you can sense when your body is being invaded with these supernatural energies. With Cassie learning magic and Angel's succubus powers, you either had to adapt or else face overwhelming odds.<p>" +
					"Facing her, you were both attacking each other with the same moves. Her lips are glued to yours, tongue wrestling for superiority. Her breasts and nipples rub roughly on your chest, pleasuring her and making you very aware of her tips roughly tickling you everytime you thrust. Your arms are too busy holding up her legs as your dick takes as much of her weight on as it could, letting gravity and your strength keep her afloat on your shaft. Her hands are on your head, trying to imbue whatever sexual energy that her concentration can allow. <p>" +
					"Her hands are the real problem. If you risk letting go of a leg, it would give her enough leverage to fly out. You'd hoped to trap both her arms and legs when you lifted her up and pinned her to the wall but she was too fast and you are rapidly losing control. Your only hope is fucking the concentration out of her. <p>" +
					"You've both made love to each other plenty of times and can tell that you're both on edge. You take a deep breath, registering Angel's sweet and heavy scent for just a second, before you grunt as you begin heavily thrusting into her. She's caught off guard and her hands glow with enough demonic energy to partly blind you. For one split second, you're hit with a mind breaking amount of ecstasy. You barely register Angel's accompanying scream and tightening as you lean harder into her. <p>" +
					"Any restraints you had about holding yourself back are demolished as you feel every bit of energy accumulated rush down into your balls. Angel screams raise an octave the second she feels your cum makes contact with her womb. Her scream eventually turn silent as your dick continues pumping more and more cum into her, her eyes having rolled back in bliss. <p>" +
					"It's only when your cock finally stops pumping cum that you let go of her body. She falls down to the ground as you lean onto the wall with your arms to support yourself. You catch your breath, doing all you can not to fall over in pain. The fact that you can't feel your dick is concerning. <p>" +
					"At least it was till you felt a slight tickle. You glance down and Angel is between your legs, gently kissing and licking at the fluids left still on you. Your eyes glance below her head onto her ass as you notice a small white puddle underneath her. Well damn. <p>" +
					"<i>\"Sorry lover,\"</i> Angel speaks once she's done cleaning your dick, <i>\"I was using more power than I was used to. Nearly won that but you made me lose control for a split second.\"</i> <p>" +
					"You accept that reasoning with a tired wave of a hand. Not like you could say much with your brain in a mess. <p>" +
					"<i>\"Yeah, totally my fault. Nearly broke the both of us.\"</i> <p>" +
					"You look down at her, eyebrow raised. She points to the cum pile beneath you. <p>" +
					"<i>\"If you think you had it bad, imagine feeling all the pleasure you did plus already being on edge. I just double orgasmed so hard I passed out for a few seconds. Thanks for dropping me by the way.\"</i> <p>" +
					"You mention if she didn't overload your body with energy that wouldn't have happened. She rolls her eyes and continues gently licking your dick. Once you can finally feel her tongue gently licking around the tip and your breath returns to normal, she lets go with a gentle smile. <p>" +
					"<i>\"Wouldn't be fair for me to drain you so hard due to a mistake. Can't have my favorite boytoy out of commission for the rest of the night.\"</i> <p>" +
					"Huh, you kind of expected her to jump you. She smirks. <p>" +
					"<i>\"Your cute balls need a break. After all, you just spilled everything you had into me so hard I doubt the cleaning crew could get it out of the floor. Can't milk out anything if there's nothing there.\"</i> <p>" +
					"She gets back to her legs, your cum flowing out of her pussy liberally. She walks out with a swing to her hips until she's out of the room, causing you let out a breath you didn't realize you were holding. You can't imagine what she'll be like when she has full control over her abilities. You just hope you and your dick can survive her when the time comes. <p>";
		}
		if(flag==Result.intercourse){
			return "Angel pins you on your back, riding you with passion. You're close to the edge, but she's too far gone to take advantage of it. She's fucking you " +
					"for her own pleasure rather than trying to win. Just as you feel your climax hit, Angel cries out in ecstasy and her pussy tightens to milk your " +
					"dick dry. <p>" +
					"Angel stays on top of you as you both recover, and as your wilting penis starts to slip out of her, her vagina squeezes again to hang " +
					"onto it. <i>\"I hope you're not finished yet,\"</i> she whispers sultrily. <i>\"I won't be satisfied with just one time.\"</i> You're already starting to harden " +
					"again inside her. She pushes her perky breasts into your face and lets you lick and suck her nipples.<p>"
					+ "By the time you're completely erect, she's " +
					"acting noticeably pleasure drunk again. She grinds her hips against yours and soon she reaches her second orgasm. She only slows down for a moment, " +
					"riding you as quickly as when she started.<p>"
					+ "Your next climax builds faster than hers. She grabs your balls, pinching and squeezing to delay your " +
					"ejaculation each time you get close. As she nears her peak, she lets you go. You cum inside her again, setting off her third screaming orgasm. <p>" +
					"By the time Angel's finally satisfied, you're exhausted, but very content.";
		}
		return "You and Angel lie on the floor in 69 position, desperately pleasuring each other. Angel is extremely good at giving blowjobs and each flick of her tongue " +
				"tests your self-control. Fortunately, she's quite receptive to your oral ministrations. Her pussy trembles as you polish her clit with your tongue.<p>"
				+ "For a " +
				"moment, you think you have the upper hand, but then her tongue finds a particularly sensitive bit of flesh under your cockhead, and her hand fondles your " +
				"balls. Your hips jerk involuntarily as you cum in her mouth. Fortunately, a flood of Angel's love juice hits your face, indicating she orgasmed at the same " +
				"time.<p>" +
				"You wipe the juice from your mouth, but Angel doesn't give you any time to rest. She continues licking and sucking your cock in the aftermath of your " +
				"ejaculation. Your penis is extremely sensitive right now, but she keeps it from softening. She gives your balls a light squeeze, which you interpret as a " +
				"demand to keep eating her out.<p>"
				+ "You shove your tongue into her pussy and feel her tremble as she lets out a stifled moan. Angel redoubles her efforts and blows " +
				"you even more intensely. You retaliate by focusing on your tongue work, exploring her labia and clit to find her weaknesses.<p>" +
				"You and Angel continue servicing " +
				"each other until you both cum again. She still shows no sign of stopping and continues sucking your painfully overstimulated dick. You were sensitive after the " +
				"first time you ejaculated, but now it almost feels like you're being shocked. This is practically torture. " +
				"You pull away from her slit and beg her to stop.<p>" +
				"Angel gives you a few more very intentional licks before releasing you, as if to make a point. She sits on " +
				"your torso and looks down at you with a superior smirk. <p>"
				+ "<i>\"Since we came at the same time, I was worried you might get the crazy idea that you're a match " +
				"for me. I figured I should prove to you which of us has the most staying power.\"</i> She strokes your hair with a surprising amount of affection. <i>\"Don't " +
				"worry if you can't keep up. As long as you keep making me cum, I'll let you be my pet.\"</i>";
	}
	@Override
	public boolean fightFlight(Character opponent) {
		return !character.nude()||opponent.nude();
	}
	@Override
	public boolean attack(Character opponent) {
		return true;
	}
	@Override
	public void ding() {
		if(character.getPure(Attribute.Dark)>=1){
			character.mod(Attribute.Dark, 1);
			int rand;
			for(int i=0; i<(Global.random(3)/2)+1;i++){
				rand=Global.random(4);
				if(rand==0){
					character.mod(Attribute.Power, 1);
				}
				else if(rand==1){
					character.mod(Attribute.Seduction, 1);
				}
				else if(rand==2){
					character.mod(Attribute.Cunning, 1);
				}
				else if(rand==3){
					character.mod(Attribute.Dark, 1);
				}
			}
		}
		else{
			character.mod(Attribute.Seduction, 1);
			int rand;
			for(int i=0; i<(Global.random(3)/2)+1;i++){
				rand=Global.random(3);
				if(rand==0){
					character.mod(Attribute.Power, 1);
				}
				else if(rand==1){
					character.mod(Attribute.Seduction, 1);
				}
				else if(rand==2){
					character.mod(Attribute.Cunning, 1);
				}
			}
		}
		character.getStamina().gain(4);
		character.getArousal().gain(5);
		character.getMojo().gain(2);
	}
	@Override
	public String victory3p(Combat c, Character target, Character assist) {
		character.clearGrudge(target);
		character.clearGrudge(assist);
		if(target.human()){
			return "Angel looks over your helpless body like a predator ready to feast. She kneels between your legs and teasingly licks your erection. She circles her " +
					"tongue around the head, coating your penis thoroughly with saliva. When she's satisfied that it is sufficiently lubricated and twitching with need, " +
					"she squeezes her ample breasts around your shaft. Even before she moves, the soft warmth surrounding you is almost enough to make you cum. When she " +
					"does start moving, it's like heaven. It takes all of your willpower to hold back your climax against the sensation of her wonderful bust rubbing against " +
					"your slick dick. When her tongue attacks your glans, poking out of her cleavage, it pushes you past the limit. You erupt like a fountain into her face, " +
					"while she tries to catch as much of your seed in her mouth as she can.";
		}
		else{
			if(target.hasDick()){
				return String.format("You present %s's naked, helpless form to Angel's tender ministrations. Angel licks her lips and delicately explores her victim's body "
								+ "with her fingers. She takes her time and makes several detours on the way, before arriving inevitably at %s's erect penis.<p>"
								+ "<i>\"Ooh, what a cute cock. I wonder how it tastes...\"</i> She should be talking to %s, but she's staring straight at you. She gives you "
								+ "a wink before breaking eye contact and for a brief moment, you imagine she'll push %s out of the way and suck you off instead. She doesn't, "
								+ "though, and soon %s is bucking %s hips at the mercy of Angel's skilled blowjob.<p>"
								+ "Angel sucks %s dry before giving you a seductive smile. <i>\"Are you feeling lonely? I don't mind giving you some service too.\"</i> You're "
								+ "sorely tempted to accept her offer. You can imagine how good her mouth would feel on your dick. However, you can't simply give her "
								+ "a free win. This is a competition after all.",
						target.name(),target.name(),target.name(),target.name(),target.name(),target.possessive(false),target.name(),target.pronounSubject(false) );
			}
			else{
				return "You present "+target.name()+"'s naked, helpless form to Angel's tender ministrations. Angel licks her lips and begins licking and stroking "+target.name()+"'s body. She's " +
						"hitting all the right spots, because soon "+target.name()+" is squirming and moaning in pleasure, and Angel hasn't even touched her pussy yet. " +
						"Angel meets your eyes to focus your attention and slowly moves her fingers down the front of "+target.name()+"'s body. You can't see her hands from " +
						"this position, but you know when she reaches her target, because "+target.name()+" immediately jumps as if she's been shocked. Soon it takes all of "+
						"your energy to control "+target.name()+" who is violently shaking in the throes of orgasm. You ease her to the floor as she goes completely limp, " +
						"while Angel licks the juice from her fingers.";
			}
		}
	}
	@Override
	public String intervene3p(Combat c, Character target, Character assist) {
		if(target.human()){
			return "You manage to overwhelm "+assist.name()+" and bring her to the floor. You're able to grab both her arms and pin her helplessly beneath you. " +
					"Before you can take advantage of your position, pain explodes below your waist. "+assist.name()+" shouldn't have been able to reach your groin " +
					"from her position, but you're in too much pain to think about it. You are still lucid enough to feel large, perky breasts press against your back " +
					"and a soft whisper in your ear. <i>\"Surprise, lover.\"</i> The voice is unmistakably Angel's. She rolls you onto your back and positions herself over your face," +
					" with her legs pinning your arms. Her bare pussy is right in front of you, just out of reach of your tongue. It's weird that she's naked, considering " +
					"she caught you by surprise, but this is Angel after all.<p>";
		}
		else{
			return "You and "+target.name()+" grapple back and forth for several minutes. Soon you're both tired, sweaty, and aroused. You catch her hands for a moment and " +
					"run your tongue along her neck and collarbone. Recognizing her disadvantage, she jumps out of your grasp and directly into Angel. Neither of you " +
					"noticed Angel approach. Before "+target.name()+" can react, Angel pulls her into a passionate kiss. "+target.name()+" forgets to resist and goes limp " +
					"long enough for Angel to pin her arms.<p>";
		}
	}

	public String watched(Combat c, Character target, Character viewer){
		if(character.has(Trait.succubus)) {
			return "Towards the end of a bout, Angel always looks like she's on the verge of losing, right up until she doesn't. She's probably most dangerous when she's in a situation that any other competitor would consider the verge of defeat. You keep forgetting that, even after multiple encounters with her in the Games.<p>"
					+ "Apparently, you're not the only one. " + target.name() + " has Angel cornered, stripped, and at close quarters. She's about to press what she thinks is an advantage.<p>"
					+ "Then Angel's wings spread out, enfold them both, and they're gone, on a ride you know all too well by now. One of " + target.name() + "'s shoes falls to the ground in their wake as Angel yanks her up and out into the air, the next best thing to invisible against the night sky.<p>"
					+ "A couple more pieces of clothing drift down in their wake, most of it " + target.name() + "'s, before Angel brings them back down. The landing's not entirely gentle, and " + target.name() + " hits the floor like a sack of grain, rolling over onto her back with that Games-specific look of defeat: shame, irritation, post-orgasmic afterglow.<p>"
					+ "Angel smiles wickedly, and pulls off what's left of her bikini. There isn't much fight left in " + target.name() + "--if 'fight' is even the right word--as Angel straddles her, her knees on either side of " + target.name() + "'s face, and lowers her pussy to " + target.name() + "'s mouth.<p>"
					+ "<i>\"Lick,\"</i> Angel commands.<p>"
					+ "" + target.name() + " does. Angel closes her eyes and arches her back, biting her lip to keep from making a sound. She rides " + target.name() + "'s face to at least two, maybe more orgasms, each one announced by a full-body shudder and a sharp snap of her hips, before rolling off of " + target.name() + ".<p>"
					+ "Angel rearranges herself, leaning over on hands and knees to kiss " + target.name() + ", pausing to lick the taste of herself off " + target.name() + "'s lips and chin. They exchange a quick, whispered conversation you can't hear, before Angel laughs and pulls herself upright on shaky legs.<p>"
					+ "She takes off into the air a moment later, her arms full of hers and " + target.name() + "'s surviving clothes. " + target.name() + " lays on her back a moment longer, then lets out a single explosive breath and staggers in the general direction of the Student Union.";
		}
		else{
			return "Ahead, you see Angel and "+target.name()+" about to square up, and they don't seem to notice your approach.  You decide to sit this one out, and hang back to watch the action.<p>" +
					"Angel gives "+target.name()+" a sly, seductive look, and approaches her with outstretched arms and a confident swagger.  The two embrace, and with little hesitation, they share a long, tender kiss.  They each slowly caress the other, and Angel moves her arms around "+target.name()+"'s waist, bringing the two closer together.  Their breathing starts to intensify as their tongues go deeper and deeper, and even from your distance you can hear the unmistakable licking and sucking sounds of a wet, passionate kiss.  Angel begins to undress "+target.name()+", "+target.name()+" reciprocates in kind, and the two are stripped down to their panties in a matter of seconds, with their clothes lying in a circle around them. <p>" +
					"Angel and "+target.name()+" are still joined at the mouth, gently brushing their stiff nipples together, when Angel grabs "+target.name()+" tightly and pivots her body unexpectedly, surprising "+target.name()+" and bringing her to the ground, falling flat on her back with Angel straddling her.  Pinning "+target.name()+"'s arms, Angel's leans forward and smothers "+target.name()+" with her heavy, pendulous breasts, as "+target.name()+" kicks her feet out in a vain attempt to regain control.   <p>" +
					""+target.name()+" finally manages to wrench both her arms free, and she goes on the offensive by reaching up and pinching both of Angel's fully erect nipples between her thumbs and forefingers.  As "+target.name()+" squeezes and twists, Angel lets out a high-pitched yelp and tries to back away, her breasts being pulled painfully in both directions.  Angel grasps at "+target.name()+"'s fingers to break the hold, and "+target.name()+" manages to slip out of Angel's pin, letting go of her tender teats in the process.  As she scrambles away, Angel looks deeply indignant, and she gives "+target.name()+" a mean, vindictive glare, coddling her stinging udders. <p>" +
					"Pressing her advantage, "+target.name()+" lunges at Angel to tackle her, but Angel is quick; Angel grabs "+target.name()+" by the hair and drops backwards into a kneeling position, pulling "+target.name()+" down in front of her.  "+target.name()+" is face down on all fours with her ass in the air, and Angel kneels next to her; Angel wraps her arm firmly around "+target.name()+" to hold her in a bent-over position.  Angel brings her free hand high up in the air, and slaps it forcefully down onto "+target.name()+"'s butt – you hear a loud, clear <i>\"SMACK!\"</i> as "+target.name()+"'s body recoils, her ass jiggling for several seconds after the blow.  "+target.name()+" tries to move her hands to protect her bum, but Angel adjusts her grip around "+target.name()+" and pins both her arms helplessly to her body in the process.<p>" +
					"Again, Angel's hand goes up into the air, and again it comes down onto "+target.name()+"'s bottom with a sound like the crack of a whip.  This time Angel hit "+target.name()+"'s other butt cheek, which also jiggles satisfyingly under the blow.  "+target.name()+" lets out a loud squeal, thrashing about, trying unsuccessfully to protect her backside.  You think you see a smile begin to form on Angel's face as she raises her hand in the air a third time.  This time, instead of bringing her hand straight down onto "+target.name()+"'s rear, she swings it out to the side, coming up underneath "+target.name()+"'s slightly parted legs, and slapping her squarely on the pussy.  "+target.name()+" makes a sound somewhere between a groan and a cough, contracting her body forwards to protect her flower, and Angel releases the hold.  Reeling from the attack, "+target.name()+" rolls on the floor, whimpering in pain and cupping her vulva; Angel stands up and takes a second to admire her handiwork, looking at "+target.name()+" with equal parts pity and lust. <p>" +
					"You are directly behind Angel as she effortlessly removes "+target.name()+"'s panties, and you notice that Angel is so wet the light glistens in her glorious ass and pussy as she bends over to remove her own panties as well. <p>" +
					"Now "+target.name()+" is fully naked, lying on her back, with her hands still covering her groin. Angel crouches down next to "+target.name()+", and begins caressing and consoling her.  In a warm, soothing tone, you can barely hear Angel quietly say <i>\"Shhh, it's okay, it's okay...I'll make it all better...\"</i>  The comforting caress gently glides between "+target.name()+"'s breasts and down her stomach, with Angel's hands gradually coaxing their way under "+target.name()+"'s at her crotch, and she begins softly massaging "+target.name()+"'s lips.<p>" +
					"Angel starts slowly, taking her time while "+target.name()+" still has the wind knocked out of her.  When "+target.name()+" begins to collect herself and sit up, Angel is already waiting with another deep, tender kiss, and you see her fingers begin to disappear inside of "+target.name()+".  "+target.name()+"'s hand reaches up between Angel's legs and begins lightly touching Angel's slippery pearl, and the two return to an increasingly vigorous rhythm of kissing and rubbing.<p>" +
					"Even in the throes of a passionate embrace, Angel can somehow stay calm and collected; it is obvious she is still in control here.  Though Angel is observably enjoying herself, "+target.name()+" is clearly losing as her breathing becomes quicker and deeper.  "+target.name()+"'s soft moans gradually become louder and more frequent, until her body begins to convulse under Angel's skilled fingers – "+target.name()+" finally climaxes with a scream as Angel looks on, approvingly.  With her victory secured, Angel climaxes shortly after, seemingly on command.<p>";
		}
	}

	@Override
	public String startBattle(Character opponent) {
		if(character.getGrudge()!=null){
			switch(character.getGrudge()){
				case icequeen:
					return "Angel gives you a look that chills you to the bone. <i>\"Don't get full of yourself just because you gave me a good time. "
							+ "Believe me, I have plenty of experience resisting men's advances. Your sweet talk won't get you anywhere.\"</i>";
				case seductress:
					return "Angel licks her lips and runs a finger down her body. Her sultry smile is somehow even sexier than usual. <i>\"I've been "
							+ "slipping a bit since I'm getting so much sex lately, but if I put the effort in, I can always get who I want.\"</i>";
				case untouchable:
					return "Angel begins stretching as you prepare for the fight. Wow, she's more flexible than you realized. <i>\"You better believe it. "
							+ "I can be very hard to get when I want to be. Last time you won because I let your fingers, tongue, and other bits touch me. "
							+ "That's not going to happen this time. I'm going to be the one touching you!\"</i>";
				case succubusvagina:
					return "Angel grins at you as her hand strays down to her pelvis. <i>\"You actually made me cum first during sex. Want to try it again?\"</i> "
							+ "Her pointed tail sways playfully, drawing your attention to her inhuman features.<br>"
							+ "<i>\"Do you want to see what these powers can do to my pussy when I'm prepared? I can't wait to show you.\"</i>";
				case strapped:
					return "Angel licks her lips and smirks, swaying her hips in a sultry stride as she meets your gaze with hers. \"<i>Hey there, lover. Like what you see? "
							+ "Why don't you do me a favor and show me your tight little hole? If you make this easy, I'll even fuck you the way you like...</i>\" ";
				default:
					break;
			}
		}
		if(character.nude()){
			if(opponent.has(Trait.buttslut)&&Global.getValue(Flag.PlayerAssLosses)>=1&&Global.random(2)==0){
				return "Angel poses sexily, flaunting her naked breasts. <i>\"Do you like what you see? If you sit still and behave yourself, I'll "
						+ "let you feel these soft tits on your back while I tease your prostate.\"</i>";
			}
			return "Angel poses sexily, flaunting her naked breasts. <i>\"Do you like what you see? If you sit still and behave yourself, I'll "
					+ "let you feel these soft tits around your dick.\"</i>";
		}
		if(opponent.pantsless()){
			if(opponent.has(Trait.buttslut)&&Global.getValue(Flag.PlayerAssLosses)>=1&&Global.random(2)==0){
				return "Angel eyes your exposed dick hungrily, a dominant glint in her eye. <i>\"Did I catch you at a bad time, lover? Or "
						+ "maybe you just want me to bend you over and fuck all the fight out of you. I don't mind one bit.\"</i>";
			}
			return "Angel eyes your exposed dick hungrily. <i>\"Did I catch you at a bad time, lover? Or maybe you just want me to drain you "
					+ "dry. I don't mind one bit.\"</i>";
		}
		if(character.getAffection(opponent)>=30){
			return "Angel's smile softens for a moment as she looks at you, but she quickly regains her edge. <i>\"I was hoping to see you, lover. "
					+ "You're just so much fun to fuck.\"</i>";
		}
		if(character.has(Trait.succubus)){
			return "Angel spreads her black wings and gives you a predatory grin. <i>\"Are you ready to cum for me?\"</i>";
		}
		return "Angel licks her lips and stalks you like a predator.";
	}
	@Override
	public boolean fit() {
		return !character.nude()&&character.getStamina().percent()>=50;
	}
	@Override
	public String night() {
		Global.gui().loadPortrait(Global.getPlayer(), this.character);
		return "As you start to head back after the match, Angel grabs your hand and drags you in the other direction.<p>"
				+ "<i>\"You're officially kidnapped, because I haven't had " +
				"enough sex yet tonight.\"</i><p>"
				+ "That makes sense... kinda? You did just finish three hours of intense sex-fighting. If she wants too much more than that, you're " +
				"both going to end up pretty sleep deprived.<p>"
				+ "Angel looks like she's struggling to put her thoughts into words. <i>\"I had enough sex in general, but I want some " +
				"more time having you all to myself.\"</i> That's quite flattering coming from her, but why you specifically? Angel is openly bisexual, she could just as easily " +
				"take one of the other girls back with her.<p>"
				+ "She looks back at you and blushes noticeably. <i>\"It's better with you, and not just because you have a cock. It is " +
				"a pretty good fit though. I don't know. It doesn't matter. I'm kidnapping you, so we're going to go back to my room, have sex, and you're going to stay the night " +
				"in case I want more sex in the morning.\"</i> You follow without protest.<p>"
				+ "You lose a lot of sleep, but you don't regret it.";
	}
	public void advance(int rank){
		if(rank >= 2 && !character.has(Trait.scandalous)){
			character.add(Trait.scandalous);
		}
		if(rank >= 1 && !character.has(Trait.succubus)){
			character.add(Trait.succubus);
			character.add(Trait.tailed);
			character.outfit[0].removeAllElements();
			character.outfit[1].removeAllElements();
			character.outfit[0].add(Clothing.bikinitop);
			character.outfit[1].add(Clothing.bikinibottoms);
			character.closet.add(Clothing.bikinitop);
			character.closet.add(Clothing.bikinibottoms);
			character.mod(Attribute.Dark,1);
			character.clearSpriteImages();
		}

	}
	@Override
	public NPC getCharacter() {
		return character;
	}

	public boolean checkMood(Emotion mood, int value) {
		switch(mood){
			case horny:
				return value>=50;
			case nervous:
				return value>=150;
			default:
				return value>=100;
		}
	}
	@Override
	public String image() {
		return "assets/angel_"+ character.mood.name()+".jpg";
	}
	@Override
	public void pickFeat() {
		ArrayList<Trait> available = new ArrayList<Trait>();
		for(Trait feat: Global.getFeats()){
			if(!character.has(feat)&&feat.req(character)){
				available.add(feat);
			}
		}
		if(available.isEmpty()){
			return;
		}
		character.add((Trait) available.toArray()[Global.random(available.size())]);
	}
	@Override
	public float moodWeight(Emotion mood) {
		switch (mood) {
			case horny:
				return 1.2f;
			case nervous:
				return .7f;
			default:
				return 1f;
		}
	}
	@Override
	public String resist3p(Combat c, Character target, Character assist) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public CommentGroup getComments() {
		CommentGroup comments = new CommentGroup();
		comments.put(CommentSituation.VAG_DOM_CATCH_WIN, "<i>\"Mmmm, are you going to fill me up now?\"</i>");
		comments.put(CommentSituation.VAG_DOM_CATCH_LOSE, "<i>\"No! I can't lose like this! Cum now! Oh!\"</i>");
		comments.put(CommentSituation.VAG_SUB_CATCH_WIN, "<i>\"Keep going! Fill me with your cum!\"</i>");
		comments.put(CommentSituation.VAG_SUB_CATCH_LOSE, "<i>\"Oh fuck! Why aren't you cumming yet?\"</i>");
		comments.put(CommentSituation.ANAL_PITCH_WIN, "<i>\"I knew you were a sucker for assplay. Take it!\"</i>");
		comments.put(CommentSituation.ANAL_PITCH_LOSE, "<i>\"Ah! This is so hot... I want to cum while I've got you pegged!\"</i>");
		comments.put(CommentSituation.ANAL_CATCH_WIN, "<i>\"I knew you were a sucker for ass. Fill me!\"</i>");
		comments.put(CommentSituation.ANAL_CATCH_LOSE, "<i>\"Ah! That's a new feeling... but it's a good feeling!\"</i>");
		comments.put(CommentSituation.MOUNT_DOM_WIN, "<i>\"Shall I make you cum now? Are you going to cum all over yourself?\"</i>");
		comments.put(CommentSituation.SIXTYNINE_WIN, "<i>\"Think you can outlast me? Better get licking.\"</i>");
		comments.put(CommentSituation.BEHIND_DOM_WIN, "<i>\"Hello lover. You're all mine now...\"</i>");
		comments.put(CommentSituation.BEHIND_SUB_WIN, "<i>\"What are you doing back there, naughty boy?\"</i>");
		comments.put(CommentSituation.OTHER_CHARMED, "<i>\"You want to please me, don't you? Your beautiful Angel?\"</i>");
		comments.put(CommentSituation.OTHER_ENTHRALLED, "<i>\"What a fun little pet you are...\"</i>");
		comments.put(CommentSituation.OTHER_BOUND, "<i>\"You're all wrapped up like a present... It's not even my birthday.\"</i>");
		comments.put(CommentSituation.SELF_HORNY, "<i>\"I need you! Now! Put your cock in me!\"</i>");
		comments.put(CommentSituation.OTHER_HORNY, "<i>\"You're a little hot for me aren't you? I can help with that.\"</i>");
		comments.put(CommentSituation.SELF_OILED, "<i>\"I'm all wet... Slimy... Slippery... Shiny... Do you like me like this?\"</i>");
		comments.put(CommentSituation.OTHER_SHAMED, "<i>\"Are you embarrassed? But your cock looks so eager...\"</i>");
		comments.put(CommentSituation.SELF_BUSTED, "Angel seems deeply indignant; she narrows her eyes and gives you an icy glare, trying to hide her grimace.");
		return comments;
	}
	@Override
	public int getCostumeSet() {
		if(character.has(Trait.succubus)){
			return 2;
		}else{
			return 1;
		}
	}
	@Override
	public void declareGrudge(Character opponent, Combat c) {
		if(c.eval(character)==Result.intercourse && character.has(Trait.succubus)){ //Special, triggers on intercourse defeat.
			character.addGrudge(opponent,Trait.succubusvagina);
		}
		if(c.eval(character)==Result.anal){
			character.addGrudge(opponent,Trait.strapped);
		}
		else if(character.getGrudge()==Trait.icequeen || character.getGrudge()==Trait.seductress){ //Tier 2 Grudges, Trigger if she's lost with a Tier 1 grudge
			switch(Global.random(2)){
				case 0:
					character.addGrudge(opponent,Trait.untouchable);
					break;
				case 1:
					character.addGrudge(opponent,Trait.strapped);
					break;
			}

		}else{
			switch(Global.random(3)){ //Tier 1 grudges, triggers on any loss that doesn't flag the special.
				case 0:
					character.addGrudge(opponent,Trait.icequeen);
					break;
				case 1:
					character.addGrudge(opponent,Trait.seductress);
					break;
				case 2:
					if(character.has(Toy.Strapon) || character.has(Toy.Strapon2) && Global.checkFlag(Flag.PlayerButtslut))
						character.addGrudge(opponent,Trait.strapped);
					else
						character.addGrudge(opponent,Trait.seductress);
				default:
					break;
			}
		}
	}

	@Override
	public void resetOutfit() {
		character.outfit[0].clear();
		character.outfit[1].clear();
		if(character.has(Trait.succubus)){
			character.outfit[0].add(Clothing.bikinitop);
			character.outfit[1].add(Clothing.bikinibottoms);
		}else{
			character.outfit[0].add(Clothing.Tshirt);
			character.outfit[1].add(Clothing.thong);
			character.outfit[1].add(Clothing.miniskirt);
		}
	}
}
