package characters;

public enum ID {
    PLAYER,
    CASSIE,
    MARA,
    ANGEL,
    JEWEL,
    YUI,
    KAT,
    REYKA,
    EVE,
    SAMANTHA,
    MAYA,
    VALERIE,
    JUSTINE,
    AESOP,
    LILLY,
    AISHA,
    SUZUME,
    JETT,
    RIN,
    ALICE,
    GINETTE,
    CAROLINE,
    SARAH,
    MEI,
    SOFIA,
    CUSTOM1,
    CUSTOM2,
    CUSTOM3,
    CUSTOM4,
    CUSTOM5;


    public static ID fromString(String name){
        if(name == "Cassie"){
            return CASSIE;
        }else if(name == "Mara"){
            return MARA;
        }else if(name == "Angel"){
            return ANGEL;
        }else if(name == "Jewel"){
            return JEWEL;
        }else if(name == "Yui"){
            return YUI;
        }else if(name == "Kat"){
            return KAT;
        }else if(name == "Reyka"){
            return REYKA;
        }else if(name == "Eve"){
            return EVE;
        }else if(name == "Samantha"){
            return SAMANTHA;
        }else if(name == "Maya"){
            return MAYA;
        }else if(name == "Valerie"){
            return VALERIE;
        }else if(name =="Sofia") {
            return SOFIA;
        }else {
            return PLAYER;
        }
    }
}
