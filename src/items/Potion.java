package items;

import characters.Dummy;
import skills.Tactics;
import status.Abuff;
import status.Beastform;
import status.Buzzed;
import status.Caffeinated;
import status.Status;

import java.util.ArrayList;

import actions.Movement;
import characters.Attribute;
import characters.Character;

public enum Potion implements Item{
	Beer		( "Beer",10,"Tastes like horse piss, but it'll numb your senses","a can of ",Movement.beer,new Buzzed(null), Tactics.status,
			"You pop open a beer and chug it down, feeling buzzed and a bit sluggish."," opens up a can of cheap beer and chugs the whole thing."	),
	EnergyDrink ( "Energy Drink",20,"It'll either kill you or restore your stamina","an ",Movement.energydrink,new Caffeinated(null,3,5), Tactics.recovery,
			"You chug down the unpleasant drink. Your tiredness immediately starts to recede."," opens up an energy drink and downs the whole can."),
	SuperEnergyDrink ( "Super Energy Drink",50,"Mara's special blend","a ",Movement.energydrink,new Caffeinated(null,3,15), Tactics.recovery,
			"You chug down the bizarre drink. Energy surges through your body."," opens up an energy drink and downs the whole bottle."),
	Fox			( "Fox Potion",100,"A strange mixture that can temporarily boost your Cunning","a ",Movement.potion,new Abuff(null,Attribute.Cunning,3,10), Tactics.status,
			"You drink down the potion and feel your mind quicken."," swallows an unidentifiable potion. Her eyes take on a noticeable sharpness."),
	Bull		( "Bull Potion",100,"A strange mixture that can temporarily boost your Power","a ",Movement.potion,new Abuff(null,Attribute.Power,3,10), Tactics.status,
			"You drink down the potion and feel strength flow into your muscles."," swallows an unidentifiable potion. You can see her muscles tighten slightly."),
	Nymph		( "Nymph Potion",100,"A strange mixture that can temporarily boost your Seduction","a ",Movement.potion,new Abuff(null,Attribute.Seduction,3,10), Tactics.status,
			"You drink down the potion. You feel sexier than usual."," swallows an unidentifiable potion. She seems to take on a more sultry attitude."),
	Cat		( "Cat Potion",100,"A strange mixture that can temporarily boost your Speed","a ",Movement.potion,new Abuff(null,Attribute.Speed,2,10), Tactics.status,
			"You drink down the potion and feel an extra spring in your step."," swallows an unidentifiable potion. You see her nimbly hop in place."),
	Furlixir	( "Furry Elixir",300,"A strange, musky mixture that can temporarily give you the strength and speed of a beast","a ",Movement.potion, new Beastform(null), Tactics.status,
			"You swallow the elixir and feel an intense itch all over your body. Dog ears and a tail force their way out of your body, and fur rapidly grows "
			+ "on your limbs. You feel like a wild beast, and your physical capabilities have improved to match."," swallows a strange potion. She shudders for a moment and "
			+ "begins to transform. Animalistic feature emerge from her body like some kind of werewolf. She gives you an almost predatory smile that tells you to be wary of "
			+ "her improved physical capability."),
	;
	/**
	 * The Item's display name.
	 */
	private String desc;
	private String name;
	private String prefix;
	private int price;
	private Movement action;
	private Tactics tactic;
	private Status effect;
	private String resolution;
	private String othertext;
	/**
	 * @return the Item name
	 */
	public String getDesc()
	{
		return desc;
	}
	public String getFullDesc(Character owner) {
		return getDesc();
	}
	public int getPrice(){
		return price;
	}
	public String getName(){
		return name;
	}
	public String getFullName(Character owner){
		return getName();
	}
	public String pre(){
		return prefix;
	}
	@Override
	public void pickup(Character owner) {
		owner.gain(this);
	}
	public String getMessage(Character user){
		if(user.human()){
			return resolution;
		}
		else{
			return othertext;
		}
	}
	public Movement getAction(){
		return action;
	}
	public Status getEffect(Character target){
		return effect.copy(target);
	}
	public Tactics getTactic(){
		return tactic;
	}
	public Boolean listed() {
		return true;
	}
	@Override
	public ArrayList<Item> getRecipe() {
		return new ArrayList<Item>();
	}
	private Potion( String name, int price, String desc,String prefix,Movement action,Status effect, Tactics tactic,String resolution,String othertext )
	{
		this.name = name;
		this.price = price;
		this.desc = desc;
		this.prefix = prefix;
		this.action = action;
		this.effect = effect;
		this.tactic = tactic;
		this.resolution = resolution;
		this.othertext = othertext;
	}
}
