package items;

import java.util.ArrayList;

import characters.Character;

public enum Attachment implements Item {
	Excalibur2			("Grand Sexcalibur",1500,"An improved version of the Excalibur with better components",""),
	Excalibur3			("Masterwork Sexcalibur",5000,"",""),
	Excalibur4			("Legendary Sexcalibur",10000,"",""),
	Excalibur5			("Perfect Sexcalibur",25000,"This sex toy is a masterpiece, beyond the possibility of further upgrading",""),
	ExcaliburArcane		("Arcane Runes",3000,"The enchanted runes inscribed on the handle can steal mana from the target",""),
	ExcaliburKi			("Restoring Grip",3000,"Grasping the handle of this toy recovers a small amount of stamina","a "),
	ExcaliburDark		("Lust Curse",3000,"The cursed toy is more effective on Horny targets","a "),
	ExcaliburAnimism	("Furry Head",3000,"The vibrating head is covered with very soft fur","a "),
	ExcaliburFetish		("Tentacles",3000,"The tentacles embedded in the toy react to the user's arousal",""),
	ExcaliburScience	("Dissolver",3000,"A small sprayer uses Dissolving Solution to penetrate protective clothing","a "),
	ExcaliburNinjutsu	("Concealed Holster",3000,"The device is holstered in a hidden pocket.","a "),
	
	DildoLube			("Dildo Luber",2000,"The tips sprays an oily lubricant when pressure is applied.","a "),
	DildoSlimy			("Slime Dildo",4000,"A living sex toy made of the material from the slime girl","a "),
	OnaholeVibe			("Onahole Vibrator",2000,"A small vibrator is embedded in the onahole to give the target more pleasure","an "),
	OnaholeSlimy		("Slime Onahole",4000,"A living vagina from a slime girl","a "),
	CropKeen			("Keen Crop",2000,"The Treasure Hunter has been upgraded for better accuracy.","a "),
	CropShocker			("Shock Crop",4000,"This upgraded crop delivers a painful, but harmless electric shock on contact.","a "),
	TicklerPheromones	("Tickler Powder",2000,"The pheromone powder in the tickler can cause arousal over time",""),
	TicklerFluffy		("Fluffy Tickler",4000,"A Tickler made from the fluffy tail of a mythical fox tail. It's supernaturally soft","a "),
	;

	/**
	 * The Item's display name.
	 */
	private String desc;
	private String name;
	private String prefix;
	private int price;
	/**
	 * 
	 */
		
	public String getDesc()
	{
		return desc;
	}
	public int getPrice(){
		return price;
	}
	public String getName(){
		return name;
	}
	public String getFullName(Character owner){
		return getName();
	}
	public String pre(){
		return prefix;
	}
	@Override
	public void pickup(Character owner) {
		owner.gain(this);
	}
	@Override
	public Boolean listed() {
		return false;
	}

	private Attachment( String name, int price, String desc,String prefix )
	{
		this.name = name;
		this.price = price;
		this.desc = desc;
		this.prefix = prefix;
	}
	@Override
	public String getFullDesc(Character owner) {
		return getDesc();
	}
	@Override
	public ArrayList<Item> getRecipe() {
		ArrayList<Item> recipe = new ArrayList<Item>();
		switch(this){
		case Excalibur2:
		case OnaholeVibe:
			recipe.add(Component.HGMotor);
			break;
		case Excalibur3:
			recipe.add(Component.Titanium);
			break;
		case Excalibur4:
			recipe.add(Component.DragonBone);
			break;
		case Excalibur5:
			recipe.add(Component.PBlueprint);
			recipe.add(Component.PVibrator);
			recipe.add(Component.PHandle);
			break;
		case ExcaliburArcane:
			recipe.add(Consumable.FaeScroll);
			break;
		case ExcaliburKi:
			recipe.add(Consumable.powerband);
			break;
		case ExcaliburDark:
			recipe.add(Consumable.Talisman);
			break;
		case ExcaliburFetish:
			recipe.add(Component.Totem);
			break;
		case ExcaliburScience:
		case DildoLube:
			recipe.add(Component.MSprayer);
			break;
		case DildoSlimy:
		case OnaholeSlimy:
			recipe.add(Component.SlimeCore);
			break;
		case CropKeen:
		case ExcaliburNinjutsu:
			recipe.add(Component.Skin);
			break;
		case CropShocker:
			recipe.add(Component.Capacitor);
			break;
		case TicklerPheromones:
			recipe.add(Flask.PAphrodisiac);
			break;
		case TicklerFluffy:
		case ExcaliburAnimism:
			recipe.add(Component.Foxtail);
			break;
		}
		return recipe;
	}
}
